<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"   xmlns="http://www.liquibase.org/xml/ns/dbchangelog"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="*[local-name()='changeSet']">
		<xsl:element name="changeSet">
	      <xsl:attribute name="author"><xsl:value-of select="@author"/> </xsl:attribute>
	      <xsl:attribute name="id"><xsl:value-of select="@id"/></xsl:attribute>
	      <xsl:choose>
		      <xsl:when test="./*[local-name() = 'dropTable']">
		      	<preConditions onFail="MARK_RAN">
					<xsl:element name="tableExists">
						<xsl:attribute name="tableName"><xsl:value-of select="./*[local-name()='dropTable']/@*[local-name()='tableName']"/></xsl:attribute>
					</xsl:element>
				</preConditions>
		      	<xsl:element name="dropTable">
		      		<xsl:attribute name="tableName"><xsl:value-of select="./*[local-name()='dropTable']/@*[local-name()='tableName']"/> </xsl:attribute> 
		      	</xsl:element>
		      </xsl:when>
		      <xsl:when test="./*[local-name() = 'dropForeignKeyConstraint']">
		      	<preConditions onFail="MARK_RAN">
					<xsl:element name="foreignKeyConstraintExists">
						<xsl:attribute name="foreignKeyTableName"><xsl:value-of select="./*[local-name()='dropForeignKeyConstraint']/@*[local-name()='baseTableName']"/></xsl:attribute>
						<xsl:attribute name="foreignKeyName"><xsl:value-of select="./*[local-name()='dropForeignKeyConstraint']/@*[local-name()='constraintName']"/></xsl:attribute>
					</xsl:element>
				</preConditions>
		      	<xsl:element name="dropForeignKeyConstraint">
		      		<xsl:attribute name="baseTableName"><xsl:value-of select="./*[local-name()='dropForeignKeyConstraint']/@*[local-name()='baseTableName']"/> </xsl:attribute> 
		      		<xsl:attribute name="constraintName"><xsl:value-of select="./*[local-name()='dropForeignKeyConstraint']/@*[local-name()='constraintName']"/> </xsl:attribute>
		      	</xsl:element>
		      	
		      </xsl:when>
		      <xsl:when test="./*[local-name() = 'dropUniqueConstraint']">
		      	<preConditions onFail="MARK_RAN">
					<xsl:element name="indexExists">
						<xsl:attribute name="tableName"><xsl:value-of select="./*[local-name()='dropUniqueConstraint']/@*[local-name()='tableName']"/></xsl:attribute>
						<xsl:attribute name="indexName"><xsl:value-of select="./*[local-name()='dropUniqueConstraint']/@*[local-name()='constraintName']"/></xsl:attribute>
					</xsl:element>
				</preConditions>
		      	<xsl:element name="dropUniqueConstraint">
		      		<xsl:attribute name="tableName"><xsl:value-of select="./*[local-name()='dropUniqueConstraint']/@*[local-name()='tableName']"/> </xsl:attribute> 
		      		<xsl:attribute name="constraintName"><xsl:value-of select="./*[local-name() ='dropUniqueConstraint']/@*[local-name()='constraintName']"/> </xsl:attribute>
		      	</xsl:element>
		      	
		      </xsl:when>
		      <xsl:otherwise>
		      
		      </xsl:otherwise>
	      </xsl:choose>
	      
    </xsl:element>
	</xsl:template>
	
</xsl:stylesheet>
