/*
 * Created on Apr 29, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.poi;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.agencyport.logging.ExceptionLogger;
import com.agencyport.logging.LoggingManager;

/**
 * The ClassCodeExcelReader class
 */
public class WCClassCodeExcelReader {
    
    /**
     * The <code>logger</code> logger for this instance
     */

    final static Logger logger = LoggingManager.getLogger(WCClassCodeExcelReader.class.getName());
    

    /**
     * @param args
     */
    public static void main(String[] args) throws Exception {
        
        NCCIClassCodesNONCalifornia();
        CAClassCodes();
        
    }
        
    public static void CAClassCodes() throws Exception {
        
        String excelFilePath = "/Users/adoss/Downloads/CA_WCIRB_ClassCodes.xlsx";
        
        FileInputStream inputStream = new FileInputStream(new File(excelFilePath));
        Workbook workbook = new XSSFWorkbook(inputStream);
        Map<String, List<ClassCode>> collection = new HashMap<String, List<ClassCode>>(10);
        for(int sheetIndex=0; sheetIndex < 1; sheetIndex++) {
            Sheet firstSheet = workbook.getSheetAt(sheetIndex);
            int rowStart = Math.min(10, firstSheet.getFirstRowNum()+1);// Skip header 
            int rowEnd = Math.min(1500,firstSheet.getLastRowNum()); // Get last row #. 

            for (int rowIndex = rowStart; rowIndex < rowEnd; rowIndex++) {
                
               Row row = firstSheet.getRow(rowIndex);
               if (row == null) {
                   // Row is empty
                  continue;
               }
               ClassCode classCode = new ClassCode();
               classCode.setState("CA"); // its California.

               int lastColumn = Math.max(row.getLastCellNum(), 5); //  don't expect more that 5 cells in a row.

               for (int cellIndex = 0; cellIndex < lastColumn; cellIndex++) {
                  Cell cell = row.getCell(cellIndex, Row.RETURN_BLANK_AS_NULL);
                  if (cell == null) {
                     // The spreadsheet is empty in this cell
                  } else {
                      /**
                       * Class 
                       * SubClass    
                       * Effective Date  
                       * SubClass Desc   
                       * Class Description
                       */
                      switch (cell.getCellType()) {
                      case Cell.CELL_TYPE_STRING:
                          if(cellIndex==4) {
                              classCode.setDescription(cell.getStringCellValue());
                          }
                          if(cellIndex==3) {
                              classCode.setCode(classCode.getCode()+cell.getStringCellValue());
                          }
                          System.out.print(cell.getStringCellValue());
                          break;
                      case Cell.CELL_TYPE_BOOLEAN:
                          System.out.print(cell.getBooleanCellValue());
                          break;
                      case Cell.CELL_TYPE_NUMERIC:
                          if(cellIndex==0) {
                              classCode.setCode(String.format( "%04d",(long) cell.getNumericCellValue()));
                          }
                          System.out.print(String.format( "%04d",(long) cell.getNumericCellValue()));
                          break;
                  }
                  System.out.print(" - ");
                  }
                  
               }
               List<ClassCode> classCodes =  collection.get(classCode.getState());
               if( classCodes == null) {
                   classCodes = new ArrayList<ClassCode>();
                   collection.put(classCode.getState(), classCodes);
               }
               classCodes.add(classCode);
               logger.info("added class code "+ classCode);
            }
        }
         
        workbook.close();
        inputStream.close();
        Map<String, ClassCodes>  out = new HashMap<String, ClassCodes>(10);
        Iterator<String> keySetIter = collection.keySet().iterator();
        List<ClassCode> nationalClassCodes = collection.get(ClassCode.State.UNKNOWN.getAbbreviation());
        while(keySetIter.hasNext()) {
            String state = keySetIter.next();
            if(ClassCode.State.UNKNOWN.getAbbreviation().equals(state)) {
                continue;
            }
            if(nationalClassCodes != null) {
                nationalClassCodes.forEach(c->{
                    ClassCode cl = new ClassCode();
                    cl.setState(state);
                    cl.setClassCodeType(c.getClassCodeType());
                    cl.setCode(c.getCode());
                    cl.setDescription(c.getDescription());
                    collection.get(state).add(cl);
                });
            }
            ClassCodes cc = new ClassCodes(); 
            cc.getClassCodes().addAll(collection.get(state));
            out.put(state, cc);
        }
       
        
       out.entrySet().forEach(s->{
           try {
               JAXBContext jc1 = JAXBContext.newInstance(ClassCodes.class);
               Marshaller m1 = jc1.createMarshaller();
               FileOutputStream fileOutput = new FileOutputStream( new File("/Users/adoss/git.ap/configuration/src/main/resources/config/batchloader/WCClassCodes/"+s.getKey()+"classcode.xml") );
               m1 = jc1.createMarshaller();
               m1.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
               m1.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.FALSE);
               m1.marshal(s.getValue(), fileOutput);
               fileOutput.close();
           }catch(Exception exception) {
               ExceptionLogger.log(exception, WCClassCodeExcelReader.class, "CAClassCodes");
           }
           
       });
        
        
        

    }
    public static void NCCIClassCodesNONCalifornia() throws Exception {
        String excelFilePath = "/Users/adoss/Downloads/NCCI Class Codes_Edited.xlsx";
        FileInputStream inputStream = new FileInputStream(new File(excelFilePath));
        Workbook workbook = new XSSFWorkbook(inputStream);
        Map<String, List<ClassCode>> collection = new HashMap<String, List<ClassCode>>(10);
        for(int sheetIndex=0; sheetIndex < 2; sheetIndex++) {
            Sheet firstSheet = workbook.getSheetAt(sheetIndex);
            int rowStart = Math.min(10, firstSheet.getFirstRowNum()+1);// Skip header 
            int rowEnd = Math.min(1500,firstSheet.getLastRowNum()); // Get last row #. 

            for (int rowIndex = rowStart; rowIndex < rowEnd; rowIndex++) {
                
               Row row = firstSheet.getRow(rowIndex);
               if (row == null) {
                   // Row is empty
                  continue;
               }
               ClassCode classCode = new ClassCode();
               if( sheetIndex==1)
                   classCode.setClassCodeType("USLH");

               int lastColumn = Math.max(row.getLastCellNum(), 5); //  don't expect more that 5 cells in a row.
               
               for (int cellIndex = 0; cellIndex < lastColumn; cellIndex++) {
                   Cell cell = row.getCell(cellIndex, Row.RETURN_BLANK_AS_NULL);
                   if (cell == null) {
                      // The spreadsheet is empty in this cell
                   } else {
                       /** 
                        * description
                        * code
                        * state
                        * Status
                        */
                       
                       switch (cell.getCellType()) {
                       case Cell.CELL_TYPE_STRING:
                           if(cellIndex==0){
                               classCode.setDescription(cell.getStringCellValue());
                           }
                           if(cellIndex==2){
                               classCode.setState(cell.getStringCellValue());
                           }
                           if(cellIndex==1){
                               classCode.setCode( cell.getStringCellValue());
                           }
                           System.out.print(cell.getStringCellValue());
                           break;
                       case Cell.CELL_TYPE_BOOLEAN:
                           System.out.print(cell.getBooleanCellValue());
                           break;
                       case Cell.CELL_TYPE_NUMERIC:
                           if(cellIndex==1){
                               classCode.setCode(String.format( "%04d",(long) cell.getNumericCellValue()));
                           }
                           System.out.print(String.format( "%04d",(long) cell.getNumericCellValue()));
                           break;
                   }
                   System.out.print(" - ");
                   }
                   
                }
               List<ClassCode> classCodes =  collection.get(classCode.getState());
               if( classCodes == null) {
                   classCodes = new ArrayList<ClassCode>();
                   collection.put(classCode.getState(), classCodes);
               }
               classCodes.add(classCode);
               logger.info("added class code "+ classCode);
            }
            
            
        }
         
        workbook.close();
        inputStream.close();
        Map<String, ClassCodes>  out = new HashMap<String, ClassCodes>(10);
        Iterator<String> keySetIter = collection.keySet().iterator();
        List<ClassCode> nationalClassCodes = collection.get(ClassCode.State.UNKNOWN.getAbbreviation());
        while(keySetIter.hasNext()) {
            String state = keySetIter.next();
            if(ClassCode.State.UNKNOWN.getAbbreviation().equals(state)) {
                continue;
            }
            nationalClassCodes.forEach(c->{
                ClassCode cl = new ClassCode();
                cl.setState(state);
                cl.setClassCodeType(c.getClassCodeType());
                cl.setCode(c.getCode());
                cl.setDescription(c.getDescription());
                collection.get(state).add(cl);
            });
            ClassCodes cc = new ClassCodes(); 
            cc.getClassCodes().addAll(collection.get(state));
            out.put(state, cc);
        }
       
        
       out.entrySet().forEach(s->{
           try {
               JAXBContext jc1 = JAXBContext.newInstance(ClassCodes.class);
               Marshaller m1 = jc1.createMarshaller();
               FileOutputStream fileOutput = new FileOutputStream( new File("/Users/adoss/git.ap/configuration/src/main/resources/config/batchloader/WCClassCodes/"+s.getKey()+"classcode.xml") );
               m1 = jc1.createMarshaller();
               m1.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
               m1.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.FALSE);
               m1.marshal(s.getValue(), fileOutput);
               fileOutput.close();
           }catch(Exception exception) {
               ExceptionLogger.log(exception, WCClassCodeExcelReader.class, "NCCIClassCodesNONCalifornia");
           }
           
       });
        
        
        

    }
    

    


}
