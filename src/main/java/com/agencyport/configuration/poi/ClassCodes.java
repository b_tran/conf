package com.agencyport.configuration.poi;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by adoss on 4/30/16.
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="classCodes")
public  class ClassCodes {

    @XmlElement(name = "classCode")
    private List<ClassCode> classCodes;

    /**
     * Returns the classCodes
     * @return the classCodes
     */
    public List<ClassCode> getClassCodes() {
        if(classCodes == null) {
            classCodes = new ArrayList<ClassCode>();
        }
        return classCodes;
    }

    /**
     * Sets the classCodes to classCodes
     * @param classCodes the classCodes to set
     */
    public void setClassCodes(List<ClassCode> classCodes) {
        this.classCodes = classCodes;
    }


}
