/**
 * 
 */
package com.agencyport.configuration.spring;

import java.util.Properties;
import java.util.logging.Logger;

import javax.naming.NamingException;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jndi.JndiObjectFactoryBean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import com.agencyport.bootservice.BootServiceDriver;
import com.agencyport.configuration.loader.client.ClientConfigHolder;
import com.agencyport.logging.LoggingManager;

/**
 * 
 * The Spring AppConfig class
 */
@Configuration
@EnableJpaRepositories(basePackages = { "com.agencyport.configuration.repository" })
public class AppConfig {

    /**
     * The Constant <code>LOGGER</code>.
     */
    private static final Logger LOGGER = LoggingManager.getLogger(AppConfig.class.getName());

    /**
     * The <code>environment</code>.
     */
    @Autowired
    private Environment environment;

    @Autowired
    private ApplicationContext context;
    
    /**
     * Gets the data source.
     *
     * @return the data source
     */
    @Bean(name = "dataSource")
    public DataSource getDataSource() {
        String jndiDatasourceName = environment.getProperty("datasource");
        LOGGER.info("Configuring dataSource bean for: " + jndiDatasourceName);
        JndiObjectFactoryBean dataSource = new JndiObjectFactoryBean();
        dataSource.setJndiName(jndiDatasourceName);
        try {
            dataSource.afterPropertiesSet();
        } catch (IllegalArgumentException | NamingException e) {
            throw new RuntimeException(e);
        }
        return (DataSource) dataSource.getObject();
    }
    
    @Bean(name = "bootServiceDriver")
    public Class<BootServiceDriver> bootServiceDriver(DataSource dataSource) {
        
        LOGGER.info("Initializing BootService..");
            BootServiceDriver.bootSubSystems();
            boolean success = BootServiceDriver.getSystemBootSuccessFlag();
            if (!success) {
                String[] problematicBootstrapServices = BootServiceDriver.getProblematicBootstrapServices();
                for (int ix = 0; ix < problematicBootstrapServices.length; ix++) {
                    LOGGER.severe("Problem encountered with bootstrap service: '" + problematicBootstrapServices[ix] + "'");
                }
            } else {
                LOGGER.info("All bootstrap services started normally");
            }
            return BootServiceDriver.class;
        
        }

    /**
     * Transaction manager.
     *
     * @param entityManagerFactory the entity manager factory
     * @return the jpa transaction manager
     */
    @Bean
    public JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory);
        return transactionManager;
    }

    /**
     * Entity manager factory.
     *
     * @param dataSource the data source
     * @return the local container entity manager factory bean
     */
    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource) {

        LOGGER.info("Configuring LocalContainerEntityManagerFactoryBean");

        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setDataSource(dataSource);
        entityManagerFactoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        entityManagerFactoryBean.setPackagesToScan("com.agencyport.configuration.entity");

        Properties jpaProperties = new Properties();

        // Configures the used database dialect. This allows Hibernate to create
        // SQL
        // that is optimized for the used database.
        jpaProperties.put("hibernate.dialect", environment.getProperty("hibernate.dialect", org.hibernate.dialect.MySQL5InnoDBDialect.class.getName()));

        // Specifies the action that is invoked to the database when the
        // Hibernate
        // SessionFactory is created or closed.
        jpaProperties.put("hibernate.hbm2ddl.auto", environment.getProperty("hibernate.hbm2ddl.auto", "none"));

        // If the value of this property is true, Hibernate writes all SQL
        // statements to the console.
        jpaProperties.put("hibernate.show_sql", environment.getProperty("hibernate.show_sql", Boolean.class, true));

        // If the value of this property is true, Hibernate will format the SQL
        // that is written to the console.
        jpaProperties.put("hibernate.format_sql", environment.getProperty("hibernate.format_sql", Boolean.class, true));

        jpaProperties.put("hibernate.connection.charSet", environment.getProperty("hibernate.connection.charSet", "UTF-8"));
        jpaProperties.put("hibernate.cache.use_second_level_cache", environment.getProperty("hibernate.cache.use_second_level_cache", Boolean.class, false));
        jpaProperties.put("hibernate.cache.region.factory_class", environment.getProperty("hibernate.cache.region.factory_class", String.class, "org.hibernate.cache.ehcache.SingletonEhCacheRegionFactory"));
        jpaProperties.put("hibernate.cache.use_query_cache", environment.getProperty("hibernate.cache.use_second_level_cache", Boolean.class, false));
        jpaProperties.put("hibernate.globally_quoted_identifiers", environment.getProperty("hibernate.globally_quoted_identifiers", Boolean.class, true));
        
//        jpaProperties.put("hibernate.generate_statistics", true);
//        jpaProperties.put("hibernate.cache.use_structured_entries", true);
//        
        entityManagerFactoryBean.setJpaProperties(jpaProperties);

        return entityManagerFactoryBean;
    }

    /**
     * Gets the spring context bridge.
     *
     * @return the spring context bridge
     */
    @Bean
    SpringContextBridge getSpringContextBridge() {
        return new SpringContextBridge();
    }
    

    /**
     * Spring bean for the health monitor class
     * @return an instance of health monitor
     */
    @Bean
    HealthMonitor getHealthMonitor(DataSource dataSource) {
        return new HealthMonitor(dataSource);
    }
    
    @Bean
    InitDisposeCacheManager getInitDisposeService() {
        return new InitDisposeCacheManager();
    }
    
    @Bean
    ClientConfigHolder getClientConfig(){
        return new ClientConfigHolder(context);
    }

}
