/*
 * Created on Mar 23, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.spring;

import javax.persistence.EntityManagerFactory;

import com.agencyport.configuration.repository.ClientPropertyRepository;
import com.agencyport.configuration.repository.ClientRepository;
import com.agencyport.configuration.repository.HostRepository;
import com.agencyport.configuration.repository.ProductRepository;
import com.agencyport.configuration.repository.TransactionRepository;

/**
 * The SpringContextBridgedServices class
 */
public interface SpringContextBridgedServices {

    /**
     * Gets the client repository.
     *
     * @return the client repository
     */
    public ClientRepository getClientRepository();

    /**
     * Gets the product repository.
     *
     * @return the product repository
     */
    public ProductRepository getProductRepository();

    /**
     * Gets the transaction repository.
     *
     * @return the transaction repository
     */
    public TransactionRepository getTransactionRepository();
    
    /**
     * Gets the Host repository.
     *
     * @return the Host repository
     */
    public HostRepository getHostRepository();
    
    /**
     * Gets the entity manager factory.
     *
     * @return the entity manager factory
     */
    public EntityManagerFactory getEntityManagerFactory();
    
    /**
     * Gets the ClientProperty repository
     * @return the client property repositorys
     */
	public ClientPropertyRepository getClientPropertyRepository();
    
    /**
     * HealthMonitor
     * @return HealthMonitor
     */
    public HealthMonitor getHealthMonitor();
}
