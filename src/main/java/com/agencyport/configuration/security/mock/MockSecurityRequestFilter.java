/*
 * Created on May 4, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.security.mock;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;

import com.agencyport.security.filter.SecurityRequestFilter;
import com.agencyport.security.profile.impl.SecurityProfileManager;

/**
 * The MockSecurityRequestFilter class
 */
public class MockSecurityRequestFilter extends SecurityRequestFilter{
    
    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        
        SecurityProfileManager.get().persist( TestProfileCreator.createSecurityProfile("agent"));
        
    }

}
