package com.agencyport.configuration.security.mock;

import java.util.HashMap;
import java.util.Map;

import com.agencyport.security.admin.ISecurityAdminProvider;
import com.agencyport.security.exception.SecurityException;
import com.agencyport.security.model.ISubject;
import com.agencyport.security.profile.ISecurityProfile;
import com.agencyport.security.profile.ISubjectCreator;
import com.agencyport.security.profile.impl.SecurityProfileManager;
import com.agencyport.security.provider.ISecurityProvider;

public class MockSecurityProfileManager extends SecurityProfileManager {
    
    private static class Store {
       private static Map<String, ISecurityProfile> STORE = new HashMap<String, ISecurityProfile>();
        private Store() {}
        private static ISecurityProfile acquire() {
            return STORE.get(ISecurityProfile.class.getName());
        }
        public static void persist(ISecurityProfile securityProfile) {
            STORE.put(ISecurityProfile.class.getName(),securityProfile);
        }
    }

	@Override
	public ISecurityProvider getSecurityProvider() {
		return null;
	}

	
	@Override
	public ISecurityProfile acquire() {
	    return Store.acquire();
	}
	
	@Override
	public void setSecurityProfile(ISecurityProfile securityProfile) {
	    Store.persist(securityProfile);
	}
	
	@Override
	public void persist(ISecurityProfile securityProfile) {
	    setSecurityProfile(securityProfile);
	}

	@Override
	public ISecurityAdminProvider getSecurityAdminProvider(ISecurityProfile securityProfile) {
		// TODO Auto-generated method stub
		return null;
	}
	
	

}
