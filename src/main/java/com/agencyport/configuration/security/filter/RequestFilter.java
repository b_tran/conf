package com.agencyport.configuration.security.filter;

import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.Provider;

import com.agencyport.security.filter.SecurityRequestFilter;

@Provider
public class RequestFilter extends SecurityRequestFilter{
	
	/**
     * Determines if the given <code>uriInfo</code> should be protected. By default, all requests are protected.
     * Opens up access to ClientResource to get client based information before a security profile is created.
     * @param uriInfo is the current resource URI being accessed
     * @return true if the URI should be protected
     */
	@Override
    protected boolean isProtected(UriInfo uriInfo) {
    	if (uriInfo.getPath().startsWith("anonymousQuote") || uriInfo.getPath().startsWith("clients")) {
            return false;
        }
    	return true;
    }
}
