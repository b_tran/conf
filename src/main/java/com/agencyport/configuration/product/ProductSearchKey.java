/*
 * Created on Feb 18, 2016 by adoss AgencyPort Insurance Services, Inc.
 */

package com.agencyport.configuration.product;

import com.agencyport.key.MultiPartKey;

/**
 * The ProductSearchKey class
 */
public class ProductSearchKey extends MultiPartKey {

    /**
     * The <code>serialVersionUID</code>
     */
    private static final long serialVersionUID = -8328358311439307560L;

    /**
     * Constructs an instance.
     */
    public ProductSearchKey( String producttype, String effectivedate, String version) {
        setParts(producttype, effectivedate, version);
    }

    

    /**
     * Get Product type
     * 
     * @return
     */
    public String getProductType() {
        return getPart(0);
    }

    /**
     * Get Effective Type
     * 
     * @return
     */
    public String getEffectiveDate() {
        return getPart(1);
    }

    /**
     * Get Version
     * 
     * @return
     */
    public String getProductVersion() {
        return getPart(2);
    }

    /**
     * The Class TransactionSearchKey.
     */
    public static class TransactionSearchKey extends MultiPartKey {

        /**
         * The <code>serialVersionUID</code>
         */
        private static final long serialVersionUID = -176453801800888852L;

        /**
         * 
         * Constructs an instance.
         * 
         * @param tranactionId
         *            is the transaction id
         * @param tranactionType
         *            is the transaction type
         */
        public TransactionSearchKey(String tranactionId, String transactionType) {
            setParts(tranactionId, transactionType);
        }

        /**
         * Get Transaction Id
         * 
         * @return transactionId
         */
        public String getTransactionId() {
            return getPart(0);
        }

        /**
         * Get Transaction Type
         * 
         * @return
         */
        public String getTransactionType() {
            return getPart(1);
        }

    }

    /**
     * The Class BehaviorSearchKey.
     */
    public static class BehaviorSearchKey extends MultiPartKey {
        /**
         * The <code>serialVersionUID</code>
         */
        private static final long serialVersionUID = 777635689211040114L;

        /**
         * 
         * Constructs an instance.
         * 
         * @param tranactionId
         *            is the transaction id
         */
        public BehaviorSearchKey(String tranactionId) {
            setParts(tranactionId);
        }

        /**
         * Get Transaction Id
         * 
         * @return transactionId
         */
        public String getTransactionId() {
            return getPart(0);
        }
    }
    
    /**
     * The Class AcordToWorkItemsMapSearchKey.
     */
    public static class AcordtoworkitemmapSearchKey extends MultiPartKey {


		/**
		 * generated through IDE generation menu
		 */
		private static final long serialVersionUID = -1595582251854891477L;

		/**
         * 
         * Constructs an instance.
         * 
         * @param tranactionId
         *            is the transaction id
         * @param tranactionType
         *            is the transaction type
         */
        public AcordtoworkitemmapSearchKey(String acordtoworkitemmapId) {
            setParts(acordtoworkitemmapId);
        }

        /**
         * Get Transaction Id
         * 
         * @return transactionId
         */
        public String getAcordToWorkItemsMapId() {
            return getPart(0);
        }


    }

}
