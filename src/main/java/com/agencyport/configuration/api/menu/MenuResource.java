/*
 * Created on Jun 24, 2016 by pnamepally AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.api.menu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.agencyport.api.BaseResource;
import com.agencyport.api.URIBuilder;
import com.agencyport.configuration.entity.Client;
import com.agencyport.configuration.entity.jaxb.menudefinition.MenuDefinitionType;
import com.agencyport.configuration.entity.jaxb.menudefinition.MenuGroupType;
import com.agencyport.configuration.spring.SpringContextBridge;
import com.agencyport.logging.ExceptionLogger;
import com.agencyport.rest.AtomLink;
import com.agencyport.rest.StandardResponseEntity;
import com.agencyport.rest.Status;
import com.agencyport.security.profile.ISecurityProfile;
import com.agencyport.security.profile.impl.SecurityProfileManager;

/**
 * The WorkflowResource class
 */
@Path("/menu")
public class MenuResource extends BaseResource {
    /**
     * Get Menu
     * @param menugroupname is the name of the menu group being queried.
     * @return Menu response.
     */
    @Path("/{menuGroupName}")
    @GET
    @Produces({ MediaType.APPLICATION_XML })
    public Response getMenus(@DefaultValue("*") @PathParam("menuGroupName") String menugroupname) {
        return getMenuResponse( menugroupname);
    }

    /**
     * Gets the option lists response.
     * @param menugroupname is the name of the menu group being queried.
     * @return Menu response.
     */
    public Response getMenuResponse( String menugroupname) {
        final String functionName = "getMenuGroup";
        EntityManager em = SpringContextBridge.services().getEntityManagerFactory().createEntityManager();
        ISecurityProfile securityProfile = SecurityProfileManager.get().acquire();
        try {
            List<MenuGroupType> menuGroups;
            Client eClient = SpringContextBridge.services().getClientRepository().findById(securityProfile.getClient().getId().intValue());
            Response.ResponseBuilder builder = this.getBuilder(this.getAccepts());
            if(eClient == null) {
                menuGroups = new ArrayList<MenuGroupType>();
            }else {
                
                final String menuGroupResourceURLpattern = "/menu/{menugroupname}";
                
                CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
                CriteriaQuery<MenuGroupType> criteriaQuery = criteriaBuilder.createQuery(MenuGroupType.class);
                Root<MenuGroupType> pRoot = criteriaQuery.from(MenuGroupType.class);
                Join<MenuGroupType,MenuDefinitionType> menuDef = pRoot.join("menuDefinitionType");
                List<Predicate> predicates = new ArrayList<Predicate>();
                predicates.add((menuDef.get("client").in(eClient)));
                if(!"*".equals(menugroupname)){
                	predicates.add(criteriaBuilder.equal(pRoot.get("name"), menugroupname));
                }

                criteriaQuery.select(pRoot)
                        .where(predicates.toArray(new Predicate[] {}));

                menuGroups = getCachedResults( em.createQuery(criteriaQuery));
                        
                menuGroups.forEach(menuGroup -> {
                    Map<String, String> variables = new HashMap<String, String>();
                    variables.put("menugroupname", menuGroup.getName());
                    String href = URIBuilder.createURI(MenuResource.class, menuGroupResourceURLpattern, variables);
                    menuGroup.getLink().add(new AtomLink("self", href));
                    menuGroup.getMenuMember().forEach(member->{
                    	member.getModal().getTitle();//for lazy loading
                    	member.getModal().getButtons().forEach(button ->{
                    		button.getText();//for lazy loading
                    	});
                    });
                });
                
            }
            builder.entity(StandardResponseEntity.create().generate(this.getAccepts(), new MenuOutputter(this.getAccepts(), menuGroups)));
            return builder.build();

        } catch (Exception exception) {
            ExceptionLogger.log(exception, getClass(), functionName);
            return this.generateExceptionResponse(this.getAccepts(), new Exception("Could not find matching menu groups "), Status.MENU_ISSUE,
                    functionName);
        } finally {
            em.close();
        }
    }
    /**
     * Get Cached results
     * @param query
     * @return results
     */
    public <T extends Object> List<T> getCachedResults(TypedQuery<T> query) {
        List<T> results = query
                .setHint("org.hibernate.cacheable",true)
                .setHint("org.hibernate.cacheRegion",this.getClass().getName())
                .getResultList();
        
        return results;
    }
}
