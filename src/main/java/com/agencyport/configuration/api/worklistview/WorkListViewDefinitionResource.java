/*
 * Created on Apr 11, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.api.worklistview;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.agencyport.api.BaseResource;
import com.agencyport.api.URIBuilder;
import com.agencyport.configuration.api.product.outputter.WorkListViewOutputter;
import com.agencyport.configuration.entity.Client;
import com.agencyport.configuration.entity.jaxb.worklistview.WorkListViewDefinition;
import com.agencyport.configuration.spring.SpringContextBridge;
import com.agencyport.logging.ExceptionLogger;
import com.agencyport.rest.AtomLink;
import com.agencyport.rest.StandardResponseEntity;
import com.agencyport.rest.Status;
import com.agencyport.security.profile.ISecurityProfile;
import com.agencyport.security.profile.impl.SecurityProfileManager;

/**
 * The WorkListViewDefinitionResource class
 */
@Path("/worklistview")
public class WorkListViewDefinitionResource extends BaseResource {
    /**
     * Get Solr IndexMapping
     * @return Worklist View Mapping response.
     */
    @GET
    @Produces({ MediaType.APPLICATION_XML })
    public Response getWorkListView() {
        return getWorkListViewResponse( "*");
    }

    /**
     * Get Solr IndexMapping
     * @param viewName
     *            is the work list viewName.
     * @return Worklist View Mapping response.
     */
    @GET
    @Path("/{viewName}")
    @Produces({ MediaType.APPLICATION_XML })
    public Response getWorkListView(@DefaultValue("*") @PathParam("viewName") String viewName) {
        return getWorkListViewResponse( viewName);
    }

    /**
     * Gets the work list view response.
     * @param viewName the view name
     * @return the work list view response
     */
    private Response getWorkListViewResponse( String viewName) {
        final String functionName = "getWorkListViewResponse";
        EntityManager em = SpringContextBridge.services().getEntityManagerFactory().createEntityManager();
        ISecurityProfile securityProfile = SecurityProfileManager.get().acquire();
        try {
            Response.ResponseBuilder builder = this.getBuilder(this.getAccepts());
            final String resourceURLpattern = "/{viewName}";
            Client eClient = SpringContextBridge.services().getClientRepository().findById(securityProfile.getClient().getId().intValue());
            if (eClient == null) {
                eClient = new Client();
            }
            CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
            CriteriaQuery<WorkListViewDefinition> criteriaQuery = criteriaBuilder.createQuery(WorkListViewDefinition.class);
            Root<WorkListViewDefinition> pRoot = criteriaQuery.from(WorkListViewDefinition.class);
            List<Predicate> predicates = new ArrayList<Predicate>();

            if (!"*".equals(viewName)) {
                predicates.add(criteriaBuilder.equal(pRoot.get("viewName"), viewName));
            }
            predicates.add(criteriaBuilder.equal(pRoot.get("clientId"), eClient.getId()));
            predicates.add(criteriaBuilder.lessThanOrEqualTo(pRoot.get("effectiveDate"), new Date()));
            predicates.add(criteriaBuilder.equal(pRoot.get("isCurrentVersion"), 1));
            criteriaQuery.select(pRoot)
                    .where(predicates.toArray(new Predicate[] {}));

            List<WorkListViewDefinition> workListViewDefinitionList =getCachedResults ( em.createQuery(criteriaQuery));

            workListViewDefinitionList.forEach(eWorkListViewDefinition-> {
				
				if(eWorkListViewDefinition.getQueryInfos() != null) {
					eWorkListViewDefinition.getQueryInfos().setWorkListViewDefinition(eWorkListViewDefinition);
					eWorkListViewDefinition.getQueryInfos().getQueryInfo().forEach(qInfo->{
						qInfo.setQueryInfos(eWorkListViewDefinition.getQueryInfos());
						qInfo.getQueryField().forEach(queryField->{
							queryField.setQueryInfo(qInfo);
							
						});
						qInfo.getContent().forEach(content->{
							content.setQueryInfo(qInfo);
						});
					});
				}
				
				if(eWorkListViewDefinition.getSortInfos() != null) {
					eWorkListViewDefinition.getSortInfos().setWorkListViewDefinition(eWorkListViewDefinition);
					eWorkListViewDefinition.getSortInfos().getSortInfo().forEach(sInfo->{
						sInfo.setSortInfos(eWorkListViewDefinition.getSortInfos());
						sInfo.getContent().forEach(content->{
							content.setSortInfo(sInfo);
						});
						sInfo.getFieldRef().forEach(fRef->{
							fRef.setSortInfo(sInfo);
							fRef.getContent().forEach(content->{
								content.setFieldRef(fRef);
							});
						});
					});
				}
				eWorkListViewDefinition.getFilters().forEach(filters->{
					filters.setWorkListViewDefinition(eWorkListViewDefinition);
					filters.getFilter().forEach(filter->{
						filter.setFilters(filters);
						filter.getContent().forEach(content->{
							content.setFilter(filter);
						});
					});
				});
				
				eWorkListViewDefinition.getViews().setWorkListViewDefinition(eWorkListViewDefinition);
				eWorkListViewDefinition.getViews().getView().forEach(view->{
					view.setViews(eWorkListViewDefinition.getViews());
					
					if(view.getFilterRefs() != null) {
						view.getFilterRefs().setView(view);
						view.getFilterRefs().transferToEntity();
						view.getFilterRefs().getFilterRef().forEach(ref->{
							ref.setFilterRefs(view.getFilterRefs());
							ref.transferFromEntity();
						});
					}
					if(view.getSortInfoRef() != null) {
						view.getSortInfoRef().setView(view);
						view.getSortInfoRef().transferFromEntity();
					}
					
					if(view.getQueryInfoRef() != null) {
						view.getQueryInfoRef().setView(view);
						view.getQueryInfoRef().transferFromEntity();
					}
					
					view.getListView().forEach(lView->{
						lView.setView(view);
						lView.getFieldRef().forEach(fRef->{
							fRef.setListView(lView);
							fRef.getContent().forEach(content->{
								content.setFieldRef(fRef);
							});
						});
					});
				});
                Map<String, String> variables = new HashMap<String, String>();
                variables.put("viewName", eWorkListViewDefinition.getViewName());
                String href = URIBuilder.createURI(WorkListViewDefinitionResource.class, resourceURLpattern, variables);
                eWorkListViewDefinition.getLink().add(new AtomLink("self", href));

            });
           
            builder.entity(
                    StandardResponseEntity.create().generate(this.getAccepts(), new WorkListViewOutputter(this.getAccepts(), workListViewDefinitionList)));
            return builder.build();
        } catch (Exception exception) {
            ExceptionLogger.log(exception, getClass(), functionName);
            return this.generateExceptionResponse(this.getAccepts(), new Exception("Could not find matching WorkListViewDefinition"),
                    Status.PRODUCT_DEFINITION_ISSUE, functionName);
        }

        finally {
            em.close();
        }
    }
    
    /**
     * Get Cached results
     * @param query
     * @return results
     */
    public <T extends Object> List<T> getCachedResults(TypedQuery<T> query) {
        List<T> results = query
                .setHint("org.hibernate.cacheable",true)
                .setHint("org.hibernate.cacheRegion",this.getClass().getName())
                .getResultList();
        
        return results;
    }

}
