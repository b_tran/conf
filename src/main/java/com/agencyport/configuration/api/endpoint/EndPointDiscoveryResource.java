package com.agencyport.configuration.api.endpoint;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import com.agencyport.api.BaseResource;
import com.agencyport.api.outputter.PojoContainerOutputter;
import com.agencyport.api.outputter.pojo.PojoObjectContainer;
import com.agencyport.configuration.entity.jaxb.endpoint.EndPointType;
import com.agencyport.configuration.spring.SpringContextBridge;
import com.agencyport.env.EnvironmentContext;
import com.agencyport.security.profile.ISecurityProfile;
import com.agencyport.security.profile.ISecurityProfileManager;
import com.agencyport.security.profile.impl.SecurityProfileManager;

/**
 * Provides a Rest Api for retreiving endpoint Uri when endpoint id is provided.
 * @author pavan.hanumanth
 *
 */
@Path("/endpoint")
public class EndPointDiscoveryResource extends BaseResource {

	@GET
	@Path("{endPointId}")
	public Response getEndPointUrl(@PathParam(value="endPointId") String endPointId){
		EntityManager em = SpringContextBridge.services().getEntityManagerFactory().createEntityManager();
		ISecurityProfileManager securityManager=SecurityProfileManager.get();
		ISecurityProfile securityProfile = securityManager.acquire();
		EndPointType res=null;
		try{
			int clientId = securityProfile.getClient().getId().intValue();
			CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
			CriteriaQuery<EndPointType> criteriaQuery = criteriaBuilder.createQuery(EndPointType.class);
			Root<EndPointType> qRoot = criteriaQuery.from(EndPointType.class);
			List<Predicate> predicates = new ArrayList<Predicate>();
			predicates.add(criteriaBuilder.equal(qRoot.get("endPointId"), endPointId));
			predicates.add(criteriaBuilder.equal(qRoot.get("environment"), EnvironmentContext.getEnvironemntValue()));
			predicates.add(criteriaBuilder.equal(qRoot.get("client"), clientId));
			criteriaQuery.select(qRoot);
			criteriaQuery.where(predicates.toArray(new Predicate[] {}));
			List<EndPointType> result = em.createQuery(criteriaQuery).getResultList();

			if(result.size()!=0){
				res = result.get(0);
			}else if(result.size()==0){
				res=new EndPointType();
				res.setUri(null);
				res.setEndPointId(endPointId);
				res.setEnvironment(EnvironmentContext.getEnvironemntValue());
			}
			return generateResponse(getAccepts(),new PojoContainerOutputter(getAccepts(), new PojoObjectContainer("endpoint", res)));
		} finally{
			em.close();
		}
	}
}
