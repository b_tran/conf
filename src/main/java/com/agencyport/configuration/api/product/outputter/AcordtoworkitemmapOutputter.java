/*
 * Created on Dec 30th, 2016 by cschulz AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.api.product.outputter;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import com.agencyport.configuration.entity.jaxb.transdef.ObjectFactory;
import com.agencyport.configuration.entity.jaxb.acordtoworkitemmap.Acordtoworkitemmap;
import com.agencyport.logging.ExceptionLogger;
import com.agencyport.rest.StandardOutputter;
import com.agencyport.rest.StandardResponseEntity;
import com.agencyport.rest.forwardwriter.IForwardWriter;

/**
 * The TransactionOutputter class
 */
public class AcordtoworkitemmapOutputter extends StandardOutputter {
    /**
     * The <code>transactions</code> is the list transaction object that this
     * out putter interested in.
     */
    private List<JAXBElement<Acordtoworkitemmap>> maps = new ArrayList<JAXBElement<Acordtoworkitemmap>>();

    /**
     * 
     * Constructs an instance.
     * 
     * @param mediaType
     *            is the media type to out put
     *            {@link javax.ws.rs.core.MediaType}
     * @param transactions
     *            JAXBElement<TTransaction>>
     */
    public AcordtoworkitemmapOutputter(MediaType mediaType, List<JAXBElement<Acordtoworkitemmap>> maps) {
        super(mediaType);
        this.maps = maps;
    }

    /** 
     * {@inheritDoc}
     */ 
    
    @Override
    protected void renderResults(IForwardWriter fw, StandardResponseEntity entity) throws IOException {
        fw.writeObjectFieldStart(StandardResponseEntity.RESULTS_TAG);

        if (getMediaType().equals(MediaType.APPLICATION_JSON_TYPE)) {
            fw.writeObjectField("acordtoworkitemmaps", getOutput());
        } else {
            fw.writeObjectField(null, getOutput());
        }
        fw.writeEndObject();

    }

    /**
     * Get getOutput
     * 
     * @return JDOM document representing Acordtoworkitemmap
     * @throws IOException
     *             when io exception
     */
    protected Document getOutput() throws IOException {

        try {

            StringBuilder buffer = new StringBuilder();
            buffer.append("<acordtoworkitemmaps>");

            JAXBContext context = JAXBContext.newInstance(com.agencyport.configuration.entity.jaxb.acordtoworkitemmap.Acordtoworkitemmap.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
            maps.forEach(each -> {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                try {

                    marshaller.marshal(new ObjectFactory().createAcordtoworkitemmap(each.getValue()), baos);
                } catch (JAXBException e) {
                    ExceptionLogger.log(e, getClass(), "getOutput");
                }
                buffer.append(new String(baos.toByteArray()));
            });

            buffer.append("</acordtoworkitemmaps>");

            SAXBuilder builder = new SAXBuilder();
            return builder.build(new ByteArrayInputStream(buffer.toString().getBytes()));

        } catch (JDOMException | JAXBException exception) {
            throw new IOException(exception);

        }

    }

}
