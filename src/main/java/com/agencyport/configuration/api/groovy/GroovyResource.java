package com.agencyport.configuration.api.groovy;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Predicate;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import com.agencyport.api.BaseResource;
import com.agencyport.api.outputter.PojoContainerOutputter;
import com.agencyport.api.outputter.pojo.PojoObjectContainer;
import com.agencyport.configuration.entity.Client;
import com.agencyport.configuration.entity.groovy.GroovyModel;
import com.agencyport.configuration.spring.SpringContextBridge;
import com.agencyport.security.profile.ISecurityProfile;
import com.agencyport.security.profile.impl.SecurityProfileManager;

/**
* Expresses a Rest API function which returns 
* a groovy blob for a given groovy Id.
* 
* @author btran
*/
@Path("/groovy") 
public class GroovyResource extends BaseResource{
	
	@GET
	@Path("{id}")
	public Response getGroovy(@PathParam(value="id") String groovyFilename){
		EntityManager em = SpringContextBridge.services().getEntityManagerFactory().createEntityManager();
        ISecurityProfile securityProfile = SecurityProfileManager.get().acquire();
        Response res = null;
		try{
			int clientInt = securityProfile.getClient().getId().intValue();
			CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
			CriteriaQuery<GroovyModel> criteriaQuery = criteriaBuilder.createQuery(GroovyModel.class);
			Root<GroovyModel> qRoot = criteriaQuery.from(GroovyModel.class);
			List<Predicate> predicates = new ArrayList<Predicate>();
			predicates.add(criteriaBuilder.equal(qRoot.get("groovy_name"), groovyFilename));
			predicates.add(criteriaBuilder.equal(qRoot.get("client"), getClient(clientInt)));
			criteriaQuery.select(qRoot);
			criteriaQuery.where(predicates.toArray(new Predicate[] {}));
			List<GroovyModel> result = em.createQuery(criteriaQuery).getResultList();
			
			if(result.size()!=0){
				GroovyModel g = result.get(0);
				res = generateResponse(getAccepts(),new PojoContainerOutputter(getAccepts(), new PojoObjectContainer("groovy", g)));
			}
	
		} finally{
			em.close();
		}
		
		return res;
	}
	
    private Client getClient(int clientId){
    	EntityManager em = SpringContextBridge.services().getEntityManagerFactory().createEntityManager();
    	try{
    		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
    		CriteriaQuery<Client> criteriaQuery = criteriaBuilder.createQuery(Client.class);
    		Root<Client> qRoot = criteriaQuery.from(Client.class);
    		List<Predicate> predicates = new ArrayList<Predicate>();
    		predicates.add(criteriaBuilder.equal(qRoot.get("id"), clientId));
    		criteriaQuery.select(qRoot);
    		criteriaQuery.where(predicates.toArray(new Predicate[] {}));
    		List<Client> result = em.createQuery(criteriaQuery).getResultList();
    		return result.get(0);
    	} finally{
    		em.close();
    	}
    }
}