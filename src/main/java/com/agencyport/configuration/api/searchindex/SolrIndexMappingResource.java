/*
 * Created on Apr 11, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.api.searchindex;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.agencyport.api.BaseResource;
import com.agencyport.api.URIBuilder;
import com.agencyport.configuration.api.product.outputter.IndexMappingOutputter;
import com.agencyport.configuration.entity.Client;
import com.agencyport.configuration.entity.jaxb.searchindex.IndexMapping;
import com.agencyport.configuration.spring.SpringContextBridge;
import com.agencyport.logging.ExceptionLogger;
import com.agencyport.rest.AtomLink;
import com.agencyport.rest.StandardResponseEntity;
import com.agencyport.rest.Status;
import com.agencyport.security.profile.ISecurityProfile;
import com.agencyport.security.profile.impl.SecurityProfileManager;

/**
 * The SolrIndexMappingResource class
 */
@Path("/indexmapping")
public class SolrIndexMappingResource extends BaseResource {
    /**
     * Get Solr IndexMapping
     * @return Index Mapping response.
     */
    @GET
    @Produces({ MediaType.APPLICATION_XML })
    public Response getIndexMapping() {
        return getMappingResponse( "*");
    }

    /**
     * Get Solr IndexMapping
     * @param index
     *            is the solr index name.
     * @return Index Mapping response.
     */
    @GET
    @Path("/{index}")
    @Produces({ MediaType.APPLICATION_XML })
    public Response getIndexMapping( @DefaultValue("*") @PathParam("index") String index) {
        return getMappingResponse( index);
    }

    /**
     * Gets the mapping response.
     * @param index the index
     * @return the mapping response
     */
    private Response getMappingResponse( String index) {
        final String functionName = "getMappingResponse";
        EntityManager em = SpringContextBridge.services().getEntityManagerFactory().createEntityManager();
        ISecurityProfile securityProfile = SecurityProfileManager.get().acquire();
        try {
            Response.ResponseBuilder builder = this.getBuilder(this.getAccepts());
            final String resourceURLpattern = "/{index}";
            Client eClient = SpringContextBridge.services().getClientRepository().findById(securityProfile.getClient().getId().intValue());
            if (eClient == null) {
                eClient = new Client();
            }
            CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
            CriteriaQuery<IndexMapping> criteriaQuery = criteriaBuilder.createQuery(IndexMapping.class);
            Root<IndexMapping> pRoot = criteriaQuery.from(IndexMapping.class);
            List<Predicate> predicates = new ArrayList<Predicate>();

            if (!"*".equals(index)) {
                predicates.add(criteriaBuilder.equal(pRoot.get("index"), index));
            }
            predicates.add(criteriaBuilder.equal(pRoot.get("clientId"), eClient.getId()));
            predicates.add(criteriaBuilder.lessThanOrEqualTo(pRoot.get("effectiveDate"), new Date()));
            predicates.add(criteriaBuilder.equal(pRoot.get("isCurrentVersion"), 1));
            criteriaQuery.select(pRoot)
                    .where(predicates.toArray(new Predicate[] {}));

            List<IndexMapping> indexMappingList = getCachedResults (em.createQuery(criteriaQuery));
                    
            indexMappingList.forEach(indexMapping -> {
                Map<String, String> variables = new HashMap<String, String>();
                variables.put("index", indexMapping.getIndex());
                if (!"*".equals(index)) {
                    indexMapping.getField().forEach(i -> 
                        i.getContent().forEach(c -> 
                            c.getAutoId()
                        )
                    );
                }
                String href = URIBuilder.createURI(SolrIndexMappingResource.class, resourceURLpattern, variables);
                indexMapping.getLink().add(new AtomLink("self", href));

            });
            
            builder.entity(StandardResponseEntity.create().generate(this.getAccepts(), new IndexMappingOutputter(this.getAccepts(), indexMappingList)));
            return builder.build();
        } catch (Exception exception) {
            ExceptionLogger.log(exception, getClass(), functionName);
            return this.generateExceptionResponse(this.getAccepts(), new Exception("Could not find matching indexmapping"), Status.PRODUCT_DEFINITION_ISSUE,
                    functionName);
        }

        finally {
            em.close();
        }
    }

    /**
     * Get Cached results
     * @param query
     * @return results
     */
    public <T extends Object> List<T> getCachedResults(TypedQuery<T> query) {
        List<T> results = query
                .setHint("org.hibernate.cacheable",true)
                .setHint("org.hibernate.cacheRegion",this.getClass().getName())
                .getResultList();
        
        return results;
    }
}
