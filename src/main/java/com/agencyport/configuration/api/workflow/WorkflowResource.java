/*
 * Created on Apr 11, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.api.workflow;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.agencyport.api.BaseResource;
import com.agencyport.api.URIBuilder;
import com.agencyport.configuration.api.product.outputter.WorkFlowOutputter;
import com.agencyport.configuration.entity.Client;
import com.agencyport.configuration.entity.jaxb.workflowdefinition.TChannel;
import com.agencyport.configuration.entity.jaxb.workflowdefinition.TWorkFlow;
import com.agencyport.configuration.spring.SpringContextBridge;
import com.agencyport.logging.ExceptionLogger;
import com.agencyport.rest.AtomLink;
import com.agencyport.rest.StandardResponseEntity;
import com.agencyport.rest.Status;
import com.agencyport.security.profile.ISecurityProfile;
import com.agencyport.security.profile.impl.SecurityProfileManager;

/**
 * The WorkflowResource class
 */
@Path("/workflow")
public class WorkflowResource extends BaseResource {
    /**
     * Get WorkFlow
     * @param channel is the distribution channel.
     * @return Workflow response.
     */
    @Path("/{channel}")
    @GET
    @Produces({ MediaType.APPLICATION_XML })
    public Response getWorkFlows(@DefaultValue("*") @PathParam("channel") String channel) {
        return getWorkFlowResponse( channel, "*", "*","*", "*"  );
    }
    /**
     * Get WorkFlow
     * @param channel is the distribution channel.
     * @param productType is the product type
     * @return Workflow response.
     */
    @Path("/{channel}/{productType}")
    @GET
    @Produces({ MediaType.APPLICATION_XML })
    public Response getWorkFlows(@DefaultValue("*") @PathParam("channel") String channel,
            @DefaultValue("*") @PathParam("productType") String productType) {
        return getWorkFlowResponse( channel, productType, "*","*", "*"  );
    }
    /**
     * Get WorkFlow
     * @param channel is the distribution channel.
     * @param productType is the product type
     * @param productVersion is the product version.
     * @return Workflow response.
     */
    @Path("/{channel}/{productType}/{productVersion}")
    @GET
    @Produces({ MediaType.APPLICATION_XML })
    public Response getWorkFlows(@DefaultValue("*") @PathParam("channel") String channel,
            @DefaultValue("*") @PathParam("productType") String productType,
            @DefaultValue("*") @PathParam("productVersion") String productVersion) {
        return getWorkFlowResponse( channel, productType, productVersion,"*", "*"  );
    }
    /**
     * Get WorkFlow
     * @param channel is the distribution channel.
     * @param productType is the product type
     * @param productVersion is the product version
     * @param lob is the lob
     * @return Workflow response.
     */
    @Path("/{channel}/{productType}/{productVersion}/{lob}")
    @GET
    @Produces({ MediaType.APPLICATION_XML })
    public Response getWorkFlows(@DefaultValue("*") @PathParam("channel") String channel,
            @DefaultValue("*") @PathParam("productType") String productType,
            @DefaultValue("*") @PathParam("productVersion") String productVersion,
            @DefaultValue("*") @PathParam("lob") String lob) {
        return getWorkFlowResponse( channel, productType, productVersion,lob, "*"  );
    }
    /**
     * Get WorkFlow
     * @param channel is the distribution channel.
     * @param productType is the product type.
     * @param productVersion is the product version.
     * @param lob is the lob.
     * @param transactionId is the transaction.
     * @return Workflow response.
     */
    @Path("/{channel}/{productType}/{productVersion}/{lob}/{transactionId}")
    @GET
    @Produces({ MediaType.APPLICATION_XML })
    public Response getWorkFlows(@DefaultValue("*") @PathParam("channel") String channel,
            @DefaultValue("*") @PathParam("productType") String productType ,
            @DefaultValue("*") @PathParam("productVersion") String productVersion, 
            @DefaultValue("*") @PathParam("lob") String lob, 
            @DefaultValue("*") @PathParam("transactionId") String transactionId) {
        return getWorkFlowResponse( channel, productType, productVersion,lob, transactionId  );
    }

    /**
     * Gets the work flow response.
     * @param channel the channel
     * @param productType the product type
     * @param productVersion the product version
     * @param lob the lob
     * @param transactionId the transaction id
     * @return the work flow response
     */
    private Response getWorkFlowResponse( String channel,String productType , String productVersion, String lob, String transactionId) {
        final String functionName = "getWorkFlowResponse";
        EntityManager em = SpringContextBridge.services().getEntityManagerFactory().createEntityManager();
        ISecurityProfile securityProfile = SecurityProfileManager.get().acquire();
        try {
            Response.ResponseBuilder builder = this.getBuilder(this.getAccepts());
            final String resourceURLpattern = "/{channel}/{productType}/{productVersion}/{lob}/{transactionId}";
            Client eClient = SpringContextBridge.services().getClientRepository().findById(securityProfile.getClient().getId().intValue());
            if (eClient == null) {
                eClient = new Client();
            }
            CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
            CriteriaQuery<TWorkFlow> criteriaQuery = criteriaBuilder.createQuery(TWorkFlow.class);
            Root<TWorkFlow> pRoot = criteriaQuery.from(TWorkFlow.class);
            List<Predicate> predicates = new ArrayList<Predicate>();
            predicates.add(criteriaBuilder.equal(pRoot.get("client"), eClient));
            if(!"*".equals(channel)) {
                predicates.add(criteriaBuilder.equal(pRoot.get("lookupCriteria").get("channel"), TChannel.valueOf(channel)));
            }
            if(!"*".equals(productType)) {
                predicates.add(criteriaBuilder.equal(pRoot.get("lookupCriteria").get("productType"), productType));
            }
            if(!"*".equals(productVersion)) {
                predicates.add(criteriaBuilder.equal(pRoot.get("lookupCriteria").get("productVersion"), productVersion));
            }
            if(!"*".equals(lob)) {
                predicates.add(criteriaBuilder.equal(pRoot.get("lookupCriteria").get("lob"), lob));
            }
            if(!"*".equals(transactionId)) {
                predicates.add(criteriaBuilder.equal(pRoot.get("lookupCriteria").get("transactionId"), transactionId));
            }

            predicates.add(criteriaBuilder.lessThanOrEqualTo(pRoot.get("effectiveDate"), java.sql.Date.valueOf(LocalDate.now())));
            predicates.add(criteriaBuilder.equal(pRoot.get("isCurrentVersion"), 1));
            criteriaQuery.select(pRoot)
                    .where(predicates.toArray(new Predicate[] {}));

            List<TWorkFlow> workflowList = getCachedResults( em.createQuery(criteriaQuery));
                    

            workflowList.forEach(workFlow -> {
                Map<String, String> variables = new HashMap<String, String>();
                variables.put("channel", workFlow.getLookupCriteria().getChannel().value());
                variables.put("productType", workFlow.getLookupCriteria().getProductType());
                variables.put("productVersion", workFlow.getLookupCriteria().getProductVersion());
                variables.put("lob", workFlow.getLookupCriteria().getLob());
                variables.put("transactionId", workFlow.getLookupCriteria().getTransactionId());
                String href = URIBuilder.createURI(WorkflowResource.class, resourceURLpattern, variables);
                workFlow.getLink().add(new AtomLink("self", href));

            });
            
            builder.entity(StandardResponseEntity.create().generate(this.getAccepts(), new WorkFlowOutputter(this.getAccepts(), workflowList)));
            return builder.build();
        } catch (Exception exception) {
            ExceptionLogger.log(exception, getClass(), functionName);
            return this.generateExceptionResponse(this.getAccepts(), new Exception("Could not find matching workflow"), Status.PRODUCT_DEFINITION_ISSUE,
                    functionName);
        }

        finally {
            em.close();
        }
    }

    /**
     * Get Cached results
     * @param query
     * @return results
     */
    public <T extends Object> List<T> getCachedResults(TypedQuery<T> query) {
        List<T> results = query
                .setHint("org.hibernate.cacheable",true)
                .setHint("org.hibernate.cacheRegion",this.getClass().getName())
                .getResultList();
        
        return results;
    }
}
