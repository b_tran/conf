/*
 * Created on Feb 18, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.api.productdefinition;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBElement;

import com.agencyport.api.BaseResource;
import com.agencyport.api.URIBuilder;
import com.agencyport.api.outputter.PojoContainerOutputter;
import com.agencyport.api.outputter.pojo.PojoCollectionContainer;
import com.agencyport.api.outputter.pojo.PojoContainer;
import com.agencyport.api.outputter.pojo.PojoObjectContainer;
import com.agencyport.configuration.api.pojo.OptionList;
import com.agencyport.configuration.api.product.outputter.AcordtoworkitemmapOutputter;
import com.agencyport.configuration.api.product.outputter.BehaviorOutputter;
import com.agencyport.configuration.api.product.outputter.DynamicTemplateListOutputter;
import com.agencyport.configuration.api.product.outputter.OptionListOutputter;
import com.agencyport.configuration.api.product.outputter.OptionListPojoOutputter;
import com.agencyport.configuration.api.product.outputter.PDFFormsOutputter;
import com.agencyport.configuration.api.product.outputter.RuleOutputter;
import com.agencyport.configuration.api.product.outputter.TransactionOutputter;
import com.agencyport.configuration.api.product.outputter.ViewOutputter;
import com.agencyport.configuration.entity.Client;
import com.agencyport.configuration.entity.Product;
import com.agencyport.configuration.entity.classcode.WorkCompClassCode;
import com.agencyport.configuration.entity.groovy.GroovyModel;
import com.agencyport.configuration.entity.industrycode.IndustryCode;
import com.agencyport.configuration.entity.jaxb.acordtoworkitemmap.Acordtoworkitemmap;
import com.agencyport.configuration.entity.jaxb.behavior.TransactionDefinitionBehavior;
import com.agencyport.configuration.entity.jaxb.codelist.TOption;
import com.agencyport.configuration.entity.jaxb.codelist.TOptionList;
import com.agencyport.configuration.entity.jaxb.dynamiclisttemplate.TDynamicListTemplate;
import com.agencyport.configuration.entity.jaxb.pdfdefinitions.TForm;
import com.agencyport.configuration.entity.jaxb.pdfdefinitions.TForms;
import com.agencyport.configuration.entity.jaxb.pdfdefinitions.TGroup;
import com.agencyport.configuration.entity.jaxb.transdef.ObjectFactory;
import com.agencyport.configuration.entity.jaxb.transdef.TPage;
import com.agencyport.configuration.entity.jaxb.transdef.TTransaction;
import com.agencyport.configuration.entity.jaxb.view.TView;
import com.agencyport.configuration.entity.jaxb.xarcrules.Rule;
import com.agencyport.configuration.entity.jaxb.xarcrules.RuleFile;
import com.agencyport.configuration.entity.markdown.MarkdownPojo;
import com.agencyport.configuration.product.ProductSearchKey;
import com.agencyport.configuration.spring.SpringContextBridge;
import com.agencyport.logging.ExceptionLogger;
import com.agencyport.rest.AtomLink;
import com.agencyport.rest.StandardResponseEntity;
import com.agencyport.rest.Status;
import com.agencyport.security.profile.ISecurityProfile;
import com.agencyport.security.profile.impl.SecurityProfileManager;

/**
 * The ProductDefinitionServiceResource class
 */
@Path("/productdefinition")
public class ProductDefinitionServiceResource extends BaseResource {
    /**
     * Get Products API.
     * @return Matching transaction.
     */
    @GET
    @Path("/products")
    @Produces({ MediaType.APPLICATION_XML })
    public Response getProducts() {
        ProductSearchKey searchKey = new ProductSearchKey( "*", "*", "*");
        return searchProduct(searchKey);
    }

    /**
     * Get Products API.
     * 
     * @param producttype
     *            is the product type. WORK, CA etc. its optional.
     * @return Matching transaction.
     */
    @GET
    @Path("/products/{type}")
    @Produces({ MediaType.APPLICATION_XML })
    public Response getProducts(@DefaultValue("*") @PathParam("type") String productType) {

        ProductSearchKey searchKey = new ProductSearchKey( productType, "*", "*");
        return searchProduct(searchKey);
    }

    /**
     * Get Products API.
     * 
     * @param producttype
     *            is the product type. WORK, CA etc. its optional.
     * @param productVersion
     *            product version, its optional.
     * @return Matching transaction.
     */
    @GET
    @Path("/products/{type}/{version}")
    @Produces({ MediaType.APPLICATION_XML })
    public Response getProducts(@DefaultValue("*") @PathParam("type") String productType,
            @DefaultValue("*") @PathParam("version") String version) {

        ProductSearchKey searchKey = new ProductSearchKey( productType, "*", version);
        return searchProduct(searchKey);
    }

    /**
     * Search for products
     * 
     * @param productSearchKey
     *            is the key to search products
     * @return Response that matches the search criteria
     */
    private Response searchProduct(ProductSearchKey productSearchKey) {
        final String functionName = "searchProduct";
        try {
            List<com.agencyport.configuration.entity.Product> poductList = getProducts(productSearchKey);
            PojoContainer pojoContainer = new PojoCollectionContainer("products", com.agencyport.configuration.entity.Product.class, poductList);
            return this.generateResponse(this.getAccepts(), new PojoContainerOutputter(this.getAccepts(), pojoContainer));
        } catch (Exception exception) {
            ExceptionLogger.log(exception, getClass(), functionName);
            return this.generateExceptionResponse(this.getAccepts(), new Exception("Could not find matching product"), Status.PRODUCT_DEFINITION_ISSUE,
                    functionName);
        } 
    }

    /**
     * Get Transactions API.
     * 
     * @param producttype
     *            is the product type. WORK, CA etc.
     * @param productVersion
     *            product version.
     * @param transactionType
     *            transactionType.
     * @return ALL Matching transactions.
     */
    @GET
    @Path("/products/{type}/{version}/transactions")
    @Produces({ MediaType.APPLICATION_XML })
    public Response getTransactions(@PathParam("type") String producttype,
            @PathParam("version") String productversion,
            @DefaultValue("*") @QueryParam("transactionType") String transactionType) {

        ProductSearchKey searchKey = new ProductSearchKey( producttype, "*", productversion);
        return getTransactions(searchKey, new ProductSearchKey.TransactionSearchKey("*", transactionType));
    }
    
    @GET
    @Path("/products/{type}/{version}/groovy/{filename}")
    public Response getGroovy(@PathParam("type") String type, @PathParam("version") String version, @PathParam("filename") String filename){
    	
    	Response res = null;
    	EntityManager em = SpringContextBridge.services().getEntityManagerFactory().createEntityManager();
    	try{
	    	ProductSearchKey searchKey = new ProductSearchKey(type, "*", version);
	    	List<Product> productsfiltered = getProducts(searchKey);
	        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
	        CriteriaQuery<GroovyModel> criteriaQuery = criteriaBuilder.createQuery(GroovyModel.class);
	        Root<GroovyModel> pRoot = criteriaQuery.from(GroovyModel.class);
	        List<Predicate> predicates = new ArrayList<Predicate>();
	        List<Predicate> productPredicates = new ArrayList<Predicate>();
	        productsfiltered.forEach(p -> 
	            productPredicates.add(criteriaBuilder.equal(pRoot.get("product"), p))
	        );
	        predicates.add(criteriaBuilder.or(productPredicates.stream().toArray(Predicate[]::new)));
	        predicates.add(criteriaBuilder.equal(pRoot.get("groovy_name"), filename));
	        criteriaQuery.select(pRoot)
	                .where(predicates.toArray(new Predicate[] {}));
	        
	        List<GroovyModel> groovyList = getCachedResults(em.createQuery(criteriaQuery));
	        if(groovyList.size()!=0){
	        	GroovyModel g = groovyList.get(0);
	        	res = generateResponse(getAccepts(),new PojoContainerOutputter(getAccepts(), new PojoObjectContainer("groovy", g)));
	        }
    	} finally{
    		em.close();
    	}
        
    	return res;
    }
    
    @GET
    @Path("/products/{type}/{version}/markdown/{filename}")
    public Response getMarkdown(@PathParam("type") String type, @PathParam("version") String version, @PathParam("filename") String filename){
    	
    	Response res = null;
    	EntityManager em = SpringContextBridge.services().getEntityManagerFactory().createEntityManager();
    	try{
	    	ProductSearchKey searchKey = new ProductSearchKey(type, "*", version);
	    	List<Product> productsfiltered = getProducts(searchKey);
	        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
	        CriteriaQuery<MarkdownPojo> criteriaQuery = criteriaBuilder.createQuery(MarkdownPojo.class);
	        Root<MarkdownPojo> pRoot = criteriaQuery.from(MarkdownPojo.class);
	        List<Predicate> predicates = new ArrayList<Predicate>();
	        List<Predicate> productPredicates = new ArrayList<Predicate>();
	        productsfiltered.forEach(p -> 
	            productPredicates.add(criteriaBuilder.equal(pRoot.get("product"), p))
	        );
	        predicates.add(criteriaBuilder.or(productPredicates.stream().toArray(Predicate[]::new)));
	        predicates.add(criteriaBuilder.equal(pRoot.get("markdown_name"), filename));
	        criteriaQuery.select(pRoot)
	                .where(predicates.toArray(new Predicate[] {}));
	        
	        List<MarkdownPojo> markdownList = getCachedResults(em.createQuery(criteriaQuery));
	        if(markdownList.size()!=0){
	        	MarkdownPojo m = markdownList.get(0);
	        	res = generateResponse(getAccepts(),new PojoContainerOutputter(getAccepts(), new PojoObjectContainer("markdown", m)));
	        }
    	} finally{
    		em.close();
    	}
        
    	return res;
    }

    /**
     * Get all Acordtoworkitemmaps matching search.
     * 
     * @param productSearchKey
     *            is the product search key.
     * @param transactionSearchKey
     *            transaction search key
     * @return all transaction matching search
     */
    private Response getAcordtoworkitemmaps(ProductSearchKey productSearchKey, ProductSearchKey.AcordtoworkitemmapSearchKey acordtoworkitemmapSearchKey) {
        final String functionName = "getAcordtoworkitemmaps";
        try {

            Response.ResponseBuilder builder = this.getBuilder(this.getAccepts());
            List<com.agencyport.configuration.entity.Product> productsfiltered = getProducts(productSearchKey);
            List<Acordtoworkitemmap> entities = getAcordtoworkitemmaps(productsfiltered, acordtoworkitemmapSearchKey);
            List<JAXBElement<Acordtoworkitemmap>> listOfEntityPojos = new ArrayList<>();
            entities.forEach(tran -> listOfEntityPojos.add(
            		new ObjectFactory().createAcordtoworkitemmap(tran)));
            
            builder.entity(StandardResponseEntity.create().generate(this.getAccepts(), new AcordtoworkitemmapOutputter(this.getAccepts(), listOfEntityPojos)));
            return builder.build();

        } catch (Exception exception) {
            ExceptionLogger.log(exception, getClass(), functionName);
            return this.generateExceptionResponse(this.getAccepts(), new Exception("Could not find matching transaction"), Status.PRODUCT_DEFINITION_ISSUE,
                    functionName);
        } 
    }

    /**
     * Get a document listing all available Acordtotorkitemmaps.
     * 
     * @param producttype
     *            is the product type. WORK, CA etc.
     * @param productVersion
     *            product version.
     * @param mapType
     *            type of map (usually workflow).
     * @return ALL Matching transactions.
     */
    @GET
    @Path("/products/{type}/{version}/acordtoworkitemmaps")
    @Produces({ MediaType.APPLICATION_XML })
    public Response getAcordtoworkitemmaps(@PathParam("type") String producttype,
            @PathParam("version") String productversion)
    {

        ProductSearchKey searchKey = new ProductSearchKey( producttype, "*", productversion);
        return getAcordtoworkitemmaps(searchKey, new ProductSearchKey.AcordtoworkitemmapSearchKey("*"));
    }

    /**
     * Get all transaction matching search.
     * 
     * @param productSearchKey
     *            is the product search key.
     * @param transactionSearchKey
     *            transaction search key
     * @return all transaction matching search
     */
    private Response getTransactions(ProductSearchKey productSearchKey, ProductSearchKey.TransactionSearchKey transactionSearchKey) {
        final String functionName = "getTransactions";
        try {

            Response.ResponseBuilder builder = this.getBuilder(this.getAccepts());
            List<com.agencyport.configuration.entity.Product> productsfiltered = getProducts(productSearchKey);
            List<TTransaction> transactions = getTransactions(productsfiltered, transactionSearchKey);
            List<JAXBElement<TTransaction>> listOfTransactionPojos = new ArrayList<>();
            transactions.forEach(tran -> 
                listOfTransactionPojos.add(new ObjectFactory().createTransaction(tran))
            );
            
            builder.entity(StandardResponseEntity.create().generate(this.getAccepts(), new TransactionOutputter(this.getAccepts(), listOfTransactionPojos)));
            return builder.build();

        } catch (Exception exception) {
            ExceptionLogger.log(exception, getClass(), functionName);
            return this.generateExceptionResponse(this.getAccepts(), new Exception("Could not find matching transaction"), Status.PRODUCT_DEFINITION_ISSUE,
                    functionName);
        } 
    }
    
    

    /**
     * Get all acord to work item maps matching search.
     * 
     * @param productSearchKey
     *            is the product search key.
     * @param transactionSearchKey
     *            transaction search key
     * @return all transaction matching search
     *
    private Response getAcordToWorkItemMaps(ProductSearchKey productSearchKey, ProductSearchKey.AcordtoworkitemmapSearchKey mapSearchKey) {
        final String functionName = "getAcordToWorkItemMaps";
        try {

            Response.ResponseBuilder builder = this.getBuilder(this.getAccepts());
            List<com.agencyport.configuration.entity.Product> productsfiltered = getProducts(productSearchKey);
            List<Acordtoworkitemmap> maps = getAcordtoworkitemmaps(productsfiltered, mapSearchKey);
            List<JAXBElement<Acordtoworkitemmap>> listOfAcordToWorkitemMapPojos = new ArrayList<>();
            maps.forEach(each -> 
            	listOfAcordToWorkitemMapPojos.add(new ObjectFactory().createAcordtoworkitemmap(each))
            );
            
            //builder.entity(StandardResponseEntity.create().generate(this.getAccepts(), new TransactionOutputter(this.getAccepts(), listOfAcordToWorkitemMapPojos)));
            builder.entity(StandardResponseEntity.create().generate(this.getAccepts(), new AcordtoworkitemmapOutputter(this.getAccepts(), listOfAcordToWorkitemMapPojos)));
            return builder.build();

        } catch (Exception exception) {
            ExceptionLogger.log(exception, getClass(), functionName);
            return this.generateExceptionResponse(this.getAccepts(), new Exception("Could not find matching acord-to work-item map"), Status.PRODUCT_DEFINITION_ISSUE,
                    functionName);
        } 
    }
 */
    
    /**
     * Get Transaction API.
     * 
     * @param producttype
     *            is the product type. WORK, CA etc.
     * @param productVersion
     *            product version.
     * @param transactionId
     *            transaction id.
     * @return Matching transaction.
     */

    @GET
    @Path("/products/{type}/{version}/transactions/{transactionId}")
    @Produces({ MediaType.APPLICATION_XML })
    public Response getTransaction(@PathParam("type") String producttype,
            @PathParam("version") String productVersion,
            @PathParam("transactionId") String transactionId) {

        ProductSearchKey searchKey = new ProductSearchKey( producttype, "*", productVersion);
        return getTransaction(searchKey, new ProductSearchKey.TransactionSearchKey(transactionId, "*"));
    }

    /**
     * Get Transaction
     * 
     * @param productSearchKey
     *            is the product search key.
     * @param transactionSearchKey
     *            transaction search key
     * @return transaction response.
     */
    private Response getTransaction(ProductSearchKey productSearchKey, ProductSearchKey.TransactionSearchKey transactionSearchKey) {
        EntityManager em = SpringContextBridge.services().getEntityManagerFactory().createEntityManager();
        final String functionName = "getTransaction";
        try {
            Response.ResponseBuilder builder = this.getBuilder(this.getAccepts());
            List<com.agencyport.configuration.entity.Product> productsfiltered = getProducts(productSearchKey);
            TTransaction tTransaction = getTransactions(productsfiltered, transactionSearchKey).stream().findFirst().orElse(new TTransaction());
            TTransaction tran = em.find(TTransaction.class, tTransaction.getAutoId());
            List<JAXBElement<TTransaction>> listOfTransaction = new ArrayList<>();

            if (tran != null) {
                tran.transferFromEntity();

                for (TPage page : tran.getTPage()) {
                    page.getPageElement().forEach(pageElem -> 
                        pageElem.getFieldElement().forEach(fieldElem -> 
                            fieldElem.transferFromEntity()
                        )

                    );
                    page.getConnectors().forEach(connectors -> {
                        connectors.getConnector().forEach(connector -> 
                            connector.getCustomParameters().forEach(param -> 
                                param.getAutoId()
                            )
                        );
                        connectors.getInstruction().forEach(instruction -> 
                            instruction.transferFromEntity()
                        );

                    });
                }
                listOfTransaction.add(new ObjectFactory().createTransaction(tran));
            } 
            
            builder.entity(StandardResponseEntity.create().generate(this.getAccepts(), new TransactionOutputter(this.getAccepts(), listOfTransaction)));
            return builder.build();

        } catch (Exception exception) {
            ExceptionLogger.log(exception, getClass(), functionName);
            return this.generateExceptionResponse(this.getAccepts(), new Exception("Could not find matching transaction"), Status.PRODUCT_DEFINITION_ISSUE,
                    functionName);
        } finally {
            em.close();
        }
    }
    
/**
 * RESTful interface for getting a single Acordtoworkitemmap. The selector is the
 * AcordtoworkitemmapId and it is supplied by path parameter. 
 * 
 * @param producttype
 * @param productVersion
 * @param acordtoworkitemmapId
 * @return
 */
    @GET
    @Path("/products/{type}/{version}/acordtoworkitemmaps/{acordtoworkitemmapId}")
    @Produces({ MediaType.APPLICATION_XML })
    public Response getAcordtoworkitemmap(@PathParam("type") String producttype,
            @PathParam("version") String productVersion,
            @PathParam("acordtoworkitemmapId") String acordtoworkitemmapId) {

        ProductSearchKey searchKey = new ProductSearchKey( producttype, "*", productVersion);
        return getAcordtoworkitemmap(searchKey, new ProductSearchKey.AcordtoworkitemmapSearchKey(acordtoworkitemmapId));
    }


    /**
     * Get Acordtoworkitemmap
     * 
     * @param productSearchKey
     *            is the product search key.
     * @param acordtoworkitemmapSearchKey
     *            acordtoworkitemmap search key
     * @return acordtoworkitemmap response.
     */
    private Response getAcordtoworkitemmap(ProductSearchKey productSearchKey, ProductSearchKey.AcordtoworkitemmapSearchKey acordtoworkitemmapSearchKey) {
        EntityManager em = SpringContextBridge.services().getEntityManagerFactory().createEntityManager();
        final String functionName = "getAcordtoworkitemmap";
        try {
            Response.ResponseBuilder builder = this.getBuilder(this.getAccepts());
            List<com.agencyport.configuration.entity.Product> productsfiltered = getProducts(productSearchKey);
            Acordtoworkitemmap existingOrNewMap = getAcordtoworkitemmaps(productsfiltered, acordtoworkitemmapSearchKey).stream().findFirst().orElse(new Acordtoworkitemmap());
            Acordtoworkitemmap map = em.find(Acordtoworkitemmap.class, existingOrNewMap.getAutoId());
            List<JAXBElement<Acordtoworkitemmap>> listOfAcordtoworkitemmaps = new ArrayList<>();

            if (map != null) {
            	map.transferFromEntity();
                listOfAcordtoworkitemmaps.add(new ObjectFactory().createAcordtoworkitemmap(map));
            }
            
            builder.entity(StandardResponseEntity.create().generate(this.getAccepts(), new AcordtoworkitemmapOutputter(this.getAccepts(), listOfAcordtoworkitemmaps)));
            return builder.build();

        } catch (Exception exception) {
            ExceptionLogger.log(exception, getClass(), functionName);
            return this.generateExceptionResponse(this.getAccepts(), new Exception("Could not find matching transaction"), Status.PRODUCT_DEFINITION_ISSUE,
                    functionName);
        } finally {
            em.close();
        }
    }


    /**
     * Get getBehaviors API.
     * 
     * @param producttype
     *            is the product type. WORK, CA etc.
     * @param productVersion
     *            product version.
     * @param transactionId
     *            transaction id.
     * @return Matching behavior.
     */
    @GET
    @Path("/products/{type}/{version}/transactions/{transactionId}/behaviors")
    @Produces({ MediaType.APPLICATION_XML })
    public Response getBehaviors(@PathParam("type") String producttype,
            @PathParam("version") String productVersion,
            @PathParam("transactionId") String transactionId) {

        ProductSearchKey searchKey = new ProductSearchKey( producttype, "*", productVersion);
        ProductSearchKey.TransactionSearchKey transactionSearchKey = new ProductSearchKey.TransactionSearchKey(transactionId, "*");
        return getBehaviors(searchKey, transactionSearchKey);
    }

    /**
     * Build Behaviors response.
     * 
     * @param productSearchKey
     *            product search criteria.
     * @param transactionSearchKey
     *            transaction search key
     * @return response.
     */
    private Response getBehaviors(ProductSearchKey productSearchKey, ProductSearchKey.TransactionSearchKey transactionSearchKey) {
        EntityManager em = SpringContextBridge.services().getEntityManagerFactory().createEntityManager();
        final String functionName = "getBehaviors";
        try {
            Response.ResponseBuilder builder = this.getBuilder(this.getAccepts());
            List<com.agencyport.configuration.entity.Product> productsfiltered = getProducts(productSearchKey);
            TTransaction tTransaction = getTransactions(productsfiltered, transactionSearchKey).stream().findFirst().orElse(new TTransaction());
            TTransaction tran = em.find(TTransaction.class, tTransaction.getAutoId());
            List<TransactionDefinitionBehavior> listOfTransactionDefinitionBehavior = new ArrayList<TransactionDefinitionBehavior>();
            if (tran != null) {
                tran.getTransactionDefinitionBehavior()
                        .stream()
                        .forEach(transactionDefinitionBehavior -> {
                            if (transactionDefinitionBehavior.getWhere() != null) 
                                transactionDefinitionBehavior.getWhere().getPreCondition().forEach(p -> 
                                    p.getAutoId()
                                );
                            transactionDefinitionBehavior.getAdditionalFieldToShred().forEach(x -> 
                                x.getAutoId()
                            );
                            transactionDefinitionBehavior.getHotField().forEach(h -> 
                                h.getRefreshListFieldEvent()
                            );
                            transactionDefinitionBehavior.getBehavior().forEach(b -> 
                                b.transferFromEntity()
                            );
                        });

                listOfTransactionDefinitionBehavior = tran.getTransactionDefinitionBehavior()
                        .stream().collect(Collectors.toList());
            } 
            
            builder.entity(
                    StandardResponseEntity.create().generate(this.getAccepts(), new BehaviorOutputter(this.getAccepts(), listOfTransactionDefinitionBehavior)));
            return builder.build();

        } catch (Exception exception) {
            ExceptionLogger.log(exception, getClass(), functionName);
            return this.generateExceptionResponse(this.getAccepts(), new Exception("Could not find matching behaviors"), Status.PRODUCT_DEFINITION_ISSUE,
                    functionName);
        } finally {
            if (em != null)
                em.close();
        }
    }

    /**
     * Get Products
     * 
     * @param productSearchKey
     *            is the product search criteria
     * @return List<Product> of {@link Product}
     */
    private List<Product> getProducts(ProductSearchKey productSearchKey) {
        ISecurityProfile securityProfile = SecurityProfileManager.get().acquire();
        EntityManager em = SpringContextBridge.services().getEntityManagerFactory().createEntityManager();
        try {
            final String resourceURLpattern = "/products/{type}/{version}";
            final String transactionsURLpattern = "/products/{type}/{version}/transactions";
            final String version = "version";
            Client client = SpringContextBridge.services().getClientRepository().findById(securityProfile.getClient().getId().intValue());
            if (client == null) {
                return new ArrayList<Product>();
            }
            CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
            CriteriaQuery<Product> criteriaQuery = criteriaBuilder.createQuery(Product.class);
            Root<Product> pRoot = criteriaQuery.from(Product.class);
            List<Predicate> predicates = new ArrayList<Predicate>();

            if (!"*".equals(productSearchKey.getProductType()))
                predicates.add(criteriaBuilder.equal(pRoot.get("type"), productSearchKey.getProductType()));

            if (!"*".equals(productSearchKey.getProductVersion()))
                predicates.add(criteriaBuilder.equal(pRoot.get(version), productSearchKey.getProductVersion()));

            predicates.add(criteriaBuilder.equal(pRoot.get("clientId"), client.getId()));

            predicates.add(criteriaBuilder.lessThanOrEqualTo(pRoot.get("effectiveDate"), java.sql.Date.valueOf(LocalDate.now())));
            predicates.add(criteriaBuilder.equal(pRoot.get("isCurrentVersion"), 1));
            criteriaQuery.select(pRoot)
                    .where(predicates.toArray(new Predicate[] {}));

            List<com.agencyport.configuration.entity.Product> poductList =  getCachedResults (em.createQuery(criteriaQuery));

            poductList.forEach(product -> {
                Map<String, String> variables = new HashMap<String, String>();
                variables.put("type", product.getType());
                variables.put(version, product.getVersion());
                String href = URIBuilder.createURI(ProductDefinitionServiceResource.class, resourceURLpattern, variables);
                product.getLink().add(new AtomLink("self", href));
                href = URIBuilder.createURI(ProductDefinitionServiceResource.class, transactionsURLpattern, variables);
                product.getLink().add(new AtomLink("transactions", href));
            });

            return poductList;

        } finally {
            em.close();
        }
    }

    /**
     * Get Transactions
     * 
     * @param transactionSearchKey
     *            is the transaction search criteria
     * @return List<TTransaction> of {@link TTransaction}
     */
    private List<TTransaction> getTransactions(List<Product> products, ProductSearchKey.TransactionSearchKey transactionSearchKey) {

        EntityManager em = SpringContextBridge.services().getEntityManagerFactory().createEntityManager();
        try {
            final String tranResourceURLpattern = "/products/{type}/{version}/transactions/{transactionId}";
            final String behaviorsResourceURLpattern = "/products/{type}/{version}/transactions/{transactionId}/behaviors";
            CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
            CriteriaQuery<TTransaction> criteriaQuery = criteriaBuilder.createQuery(TTransaction.class);
            Root<TTransaction> pRoot = criteriaQuery.from(TTransaction.class);
            List<Predicate> predicates = new ArrayList<Predicate>();

            if (!"*".equals(transactionSearchKey.getTransactionType()))
                predicates.add(criteriaBuilder.equal(pRoot.get("type"), transactionSearchKey.getTransactionType()));

            if (!"*".equals(transactionSearchKey.getTransactionId()))
                predicates.add(criteriaBuilder.equal(pRoot.get("id"), transactionSearchKey.getTransactionId()));

            List<Predicate> productPredicates = new ArrayList<Predicate>();

            products.forEach(p -> 
                productPredicates.add(criteriaBuilder.equal(pRoot.get("product"), p))
            );

            predicates.add(criteriaBuilder.or(productPredicates.stream().toArray(Predicate[]::new)));

            criteriaQuery.select(pRoot)
                    .where(predicates.toArray(new Predicate[] {}));

            List<TTransaction> transactionList = getCachedResults (em.createQuery(criteriaQuery));
            transactionList.forEach(tran -> {
                Map<String, String> variables = new HashMap<String, String>();
                variables.put("type", tran.getProduct().getType());
                variables.put("version", tran.getProduct().getVersion());
                variables.put("transactionId", tran.getId());
                String href = URIBuilder.createURI(ProductDefinitionServiceResource.class, tranResourceURLpattern, variables);
                tran.getLink().add(new AtomLink("self", href));
                href = URIBuilder.createURI(ProductDefinitionServiceResource.class, behaviorsResourceURLpattern, variables);
                tran.getLink().add(new AtomLink("behaviors", href));
            });

            return transactionList;

        } finally {
            em.close();
        }
    }
    

    /**
     * Get AcordToWorkItemMaps
     * 
     * @param acordMapSearchKey
     *            is the Acordtoworkitemmap search criteria
     * @return List<Acordtoworkitemmap> of {@link Acordtoworkitemmap}
     */
    private List<Acordtoworkitemmap> getAcordtoworkitemmaps(List<Product> products, ProductSearchKey.AcordtoworkitemmapSearchKey acordtoworkitemmapSearchKey) {

        EntityManager em = SpringContextBridge.services().getEntityManagerFactory().createEntityManager();
        try {
            final String acordtoworkitemmapResourceURLpattern = "/products/{type}/{version}/accordtoworkitemmap/{mapId}";
            CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
            CriteriaQuery<Acordtoworkitemmap> criteriaQuery = criteriaBuilder.createQuery(Acordtoworkitemmap.class);
            Root<Acordtoworkitemmap> pRoot = criteriaQuery.from(Acordtoworkitemmap.class);
            List<Predicate> predicates = new ArrayList<Predicate>();

//            if (!"*".equals(acordtoworkitemmapSearchKey.getTransactionType()))
//                predicates.add(criteriaBuilder.equal(pRoot.get("type"), acordtoworkitemmapSearchKey.getTransactionType()));

            if (!"*".equals(acordtoworkitemmapSearchKey.getAcordToWorkItemsMapId()))
                predicates.add(criteriaBuilder.equal(pRoot.get("id"), acordtoworkitemmapSearchKey.getAcordToWorkItemsMapId()));

            List<Predicate> productPredicates = new ArrayList<Predicate>();

            products.forEach(p -> 
                productPredicates.add(criteriaBuilder.equal(pRoot.get("product"), p))
            );

            predicates.add(criteriaBuilder.or(productPredicates.stream().toArray(Predicate[]::new)));

            criteriaQuery.select(pRoot)
                    .where(predicates.toArray(new Predicate[] {}));

            List<Acordtoworkitemmap> acordtoWorkItemMapList = getCachedResults (em.createQuery(criteriaQuery));
            acordtoWorkItemMapList.forEach(map -> {
                Map<String, String> variables = new HashMap<String, String>();
                variables.put("type", map.getProduct().getType());
                variables.put("version", map.getProduct().getVersion());
                variables.put("acordtoworkitemId", map.getId());
                String href = URIBuilder.createURI(ProductDefinitionServiceResource.class, acordtoworkitemmapResourceURLpattern, variables);
                map.getLink().add(new AtomLink("self", href));
            });

            return acordtoWorkItemMapList;

        } finally {
            em.close();
        }
    }
    
    /**
     * Get Views API.
     * @return Matching views.
     */
    @GET
    @Path("/views")
    @Produces({ MediaType.APPLICATION_XML })
    public Response getViews() {
        return getViewsResponse("*");
    }
    /**
     * Get Views API.
     * @param client is the client name.
     * @param id is the view id
     * @return Matching views.
     */
    @GET
    @Path("/views/{id}")
    @Produces({ MediaType.APPLICATION_XML })
    public Response getViews(  @PathParam("id") String id) {
        return getViewsResponse(id);
    }
    
    /**
     * Gets the views response.
     * @param id the id
     * @return the views response
     */
    public Response getViewsResponse( String id) {
        final String functionName = "getTDFViews";
        EntityManager em = SpringContextBridge.services().getEntityManagerFactory().createEntityManager();
        ISecurityProfile securityProfile = SecurityProfileManager.get().acquire();
        try {
            Response.ResponseBuilder builder = this.getBuilder(this.getAccepts());
            List<TView> viewList;
            Client eClient = SpringContextBridge.services().getClientRepository().findById(securityProfile.getClient().getId().intValue());
            if (eClient == null) {
                viewList =  new ArrayList<TView>();
            }else {
                final String viewResourceURLpattern = "/views/{id}";
                CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
                CriteriaQuery<TView> criteriaQuery = criteriaBuilder.createQuery(TView.class);
                Root<TView> pRoot = criteriaQuery.from(TView.class);
                List<Predicate> predicates = new ArrayList<Predicate>();
                predicates.add(criteriaBuilder.equal(pRoot.get("client"), eClient));
                if (!"*".equals(id))
                    predicates.add(criteriaBuilder.equal(pRoot.get("id"), id));
    
                criteriaQuery.select(pRoot)
                        .where(predicates.toArray(new Predicate[] {}));
    
                viewList = getCachedResults (em.createQuery(criteriaQuery));
                viewList.forEach(view -> {
                    Map<String, String> variables = new HashMap<String, String>();
                    variables.put("id", view.getId());
                    String href = URIBuilder.createURI(ProductDefinitionServiceResource.class, viewResourceURLpattern, variables);
                    view.getLink().add(new AtomLink("self", href));
                });
            }

            builder.entity(StandardResponseEntity.create().generate(this.getAccepts(), new ViewOutputter(this.getAccepts(), viewList)));
            return builder.build();

        } catch (Exception exception) {
            ExceptionLogger.log(exception, getClass(), functionName);
            return this.generateExceptionResponse(this.getAccepts(), new Exception("Could not find matching view"), Status.PRODUCT_DEFINITION_ISSUE,
                    functionName);
        } finally {
            em.close();
        }
    
    }
    
    /**
     * Get Dynamic List Template API.
     * @return Matching Dynamic List Template.
     */
    @GET
    @Path("/dynamiclisttemplates")
    @Produces({ MediaType.APPLICATION_XML })
    public Response getDynamiclisttemplates() {
        return getTDFDynamiclisttemplateResponse("*");
    }
    /**
     * Get Dynamic List Template API.
     * @param id is the dynamic list template id
     * @return Matching Dynamic List Template.
     */
    @GET
    @Path("/dynamiclisttemplates/{id}")
    @Produces({ MediaType.APPLICATION_XML })
    public Response getDynamiclisttemplates( @PathParam("id") String id) {
        return getTDFDynamiclisttemplateResponse(id);
    }
    
    /**
     * Gets the TDF dynamiclisttemplate response.
     * @param id the id
     * @return the TDF dynamiclisttemplate response
     */
    public Response getTDFDynamiclisttemplateResponse( String id) {
        final String functionName = "getTDFDynamiclisttemplates";
        EntityManager em = SpringContextBridge.services().getEntityManagerFactory().createEntityManager();
        ISecurityProfile securityProfile = SecurityProfileManager.get().acquire();
        try {
            Response.ResponseBuilder builder = this.getBuilder(this.getAccepts());
            Client eClient = SpringContextBridge.services().getClientRepository().findById(securityProfile.getClient().getId().intValue());
            List<TDynamicListTemplate> dynamicListTemplates;
            if (eClient == null) {
                dynamicListTemplates =  new ArrayList<TDynamicListTemplate>();
            }else {
                final String dynamicListTemplateResourceURLpattern = "/dynamiclisttemplates/{id}";
                CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
                CriteriaQuery<TDynamicListTemplate> criteriaQuery = criteriaBuilder.createQuery(TDynamicListTemplate.class);
                Root<TDynamicListTemplate> pRoot = criteriaQuery.from(TDynamicListTemplate.class);
                List<Predicate> predicates = new ArrayList<Predicate>();
                predicates.add(criteriaBuilder.equal(pRoot.get("client"), eClient));
                if (!"*".equals(id))
                    predicates.add(criteriaBuilder.equal(pRoot.get("id"), id));

                criteriaQuery.select(pRoot)
                        .where(predicates.toArray(new Predicate[] {}));

                dynamicListTemplates = getCachedResults( em.createQuery(criteriaQuery));
                dynamicListTemplates.forEach(dynamicListTemplate -> {
                    Map<String, String> variables = new HashMap<String, String>();
                    variables.put("id", dynamicListTemplate.getId());
                    String href = URIBuilder.createURI(ProductDefinitionServiceResource.class, dynamicListTemplateResourceURLpattern, variables);
                    dynamicListTemplate.getLink().add(new AtomLink("self", href));
                });
            }
            

            builder.entity(StandardResponseEntity.create().generate(this.getAccepts(), new DynamicTemplateListOutputter(this.getAccepts(), dynamicListTemplates)));
            return builder.build();

        } catch (Exception exception) {
            ExceptionLogger.log(exception, getClass(), functionName);
            return this.generateExceptionResponse(this.getAccepts(), new Exception("Could not find matching dynamiclisttemplate"), Status.PRODUCT_DEFINITION_ISSUE,
                    functionName);
        } finally {
            em.close();
        }
    
    }
    
    /**
     * Get Option List  API.
     * @return Matching Option List .
     */
    @GET
    @Path("/optionlists")
    @Produces({ MediaType.APPLICATION_XML })
    public Response getOptionLists() {
        return getOptionListsResponse("*","*","*");
    }
    /**
     * Get Option List  API.
     * @param client is the client name.
     * @param target is the code list target.
     * @return Matching Option List .
     */
    @GET
    @Path("/optionlists/{target}")
    @Produces({ MediaType.APPLICATION_XML })
    public Response getOptionLists(@PathParam("target") String target,
            @DefaultValue("*") @QueryParam("type") String productType,
            @DefaultValue("*") @QueryParam("version") String productVersion) {
        return getOptionListsResponse(target,productType, productVersion);
    }
    
    /**
     * Gets the option lists response.
     * @param target the target
     * @param productType the product type
     * @param productVersion the product version
     * @return the option lists response
     */
    public Response getOptionListsResponse( String target,String productType, String productVersion) {
        final String functionName = "getOptionListsResponse";
        EntityManager em = SpringContextBridge.services().getEntityManagerFactory().createEntityManager();
        ISecurityProfile securityProfile = SecurityProfileManager.get().acquire();
        try {
            List<TOptionList> optionLists;
            Client eClient = SpringContextBridge.services().getClientRepository().findById(securityProfile.getClient().getId().intValue());
            Response.ResponseBuilder builder = this.getBuilder(this.getAccepts());
            if(eClient == null) {
                optionLists = new ArrayList<TOptionList>();
            }else {
                
                final String optionListResourceURLpattern = "/optionlists/{target}?&type={type}&version={version}";
                CriteriaBuilder productCriteriaBuilder = em.getCriteriaBuilder();
               
                /**
                 * Get Products
                 */
                CriteriaQuery<Product> productCriteriaQuery = productCriteriaBuilder.createQuery(Product.class);
                Root<Product> productRoot = productCriteriaQuery.from(Product.class);
                List<Predicate> productPredicate = new ArrayList<Predicate>();
                if (!"*".equals(productType))
                    productPredicate.add(productCriteriaBuilder.equal(productRoot.get("type"), productType));
                if (!"*".equals(productVersion))
                    productPredicate.add(productCriteriaBuilder.equal(productRoot.get("version"), productVersion));
                
                productPredicate.add(productCriteriaBuilder.equal(productRoot.get("clientId"), eClient.getId()));
                productPredicate.add(productCriteriaBuilder.lessThanOrEqualTo(productRoot.get("effectiveDate"), java.sql.Date.valueOf(LocalDate.now())));
                productPredicate.add(productCriteriaBuilder.equal(productRoot.get("isCurrentVersion"), 1));
                productCriteriaQuery.select(productRoot)
                        .where(productPredicate.toArray(new Predicate[] {}));

                List<com.agencyport.configuration.entity.Product> poductList = getCachedResults( em.createQuery(productCriteriaQuery));
                        
                
                /**
                 * Get The options
                 */
                CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
                CriteriaQuery<TOption> criteriaQuery = criteriaBuilder.createQuery(TOption.class);
                Root<TOption> pRoot = criteriaQuery.from(TOption.class);
                Join<TOption,TOptionList> optionList = pRoot.join("optionList");
                List<Predicate> predicates = new ArrayList<Predicate>();
                predicates.add((optionList.get("client").in(eClient)));
                
                /**
                 * If there are no products with the given productType & productVersion set product id to -1.
                 */
                if( (!"*".equals(productType) || !"*".equals(productVersion) ) && poductList.size()==0) {
                    predicates.add((pRoot.get("product").get("id").in(Arrays.asList(-1))));
                }
                
                if (!"*".equals(target))
                    predicates.add(criteriaBuilder.equal(optionList.get("id"), target));

                criteriaQuery.select(pRoot)
                        .where(predicates.toArray(new Predicate[] {}));

                List<TOption> options = getCachedResults( em.createQuery(criteriaQuery));
                        
                optionLists = new ArrayList<TOptionList>();
                options.forEach(option -> {
                    TOptionList oList=  optionLists.stream().filter(ol ->{
                       return  ol.getId() == option.getOptionList().getId();
                        
                    }).findFirst().orElse(null);
                    if(oList==null) {
                        oList =new TOptionList();
                        oList.setId(option.getOptionList().getId());
                        oList.setTitle(option.getOptionList().getTitle());
                        Map<String, String> variables = new HashMap<String, String>();
                        variables.put("target", oList.getId());
                        variables.put("type", productType);
                        variables.put("version", productVersion);
                        String href = URIBuilder.createURI(ProductDefinitionServiceResource.class, optionListResourceURLpattern, variables);
                        oList.getLink().add(new AtomLink("self", href));
                        optionLists.add(oList);
                    }
                    oList.getOption().add(option);
                });
            }
            builder.entity(StandardResponseEntity.create().generate(this.getAccepts(), new OptionListOutputter(this.getAccepts(), optionLists)));
            return builder.build();

        } catch (Exception exception) {
            ExceptionLogger.log(exception, getClass(), functionName);
            return this.generateExceptionResponse(this.getAccepts(), new Exception("Could not find matching optionList "), Status.PRODUCT_DEFINITION_ISSUE,
                    functionName);
        } finally {
            em.close();
        }
    
    }
    
    /**
     * Get WC class code List  API.
     * @return Matching class code List .
     */
    @GET
    @Path("/wcclasscode")
    @Produces({ MediaType.APPLICATION_XML })
    public Response getWorkCompOptionLists( @QueryParam("stateprovcd") String stateProvCd,
            @DefaultValue("*") @QueryParam("classcodetype") String classCodeType) {
        return getWorkCompOptionListsResponse(stateProvCd,classCodeType);
    }
    
    /**
     * Gets the work comp option lists response.
     * @param stateProvCd the state prov cd
     * @param classCodeType they type of class code requested.
     * @return the work comp option lists response
     */
    public Response getWorkCompOptionListsResponse( String stateProvCd, String classCodeType) {
        final String functionName = "getWorkCompOptionListsResponse";
        EntityManager em = SpringContextBridge.services().getEntityManagerFactory().createEntityManager();
        ISecurityProfile securityProfile = SecurityProfileManager.get().acquire();
        try {
            Response.ResponseBuilder builder = this.getBuilder(this.getAccepts());
            final String optionListResourceURLpattern = "/wcclasscode?stateprovcd={stateProvCd}&classcodetype={classCodeType}";

            OptionList<WorkCompClassCode> pojos = new OptionList<WorkCompClassCode>();
            List<OptionList<WorkCompClassCode>> pojoList = new ArrayList<OptionList<WorkCompClassCode>>();
            
            
            TypedQuery<WorkCompClassCode> query = em.createQuery("SELECT w FROM ref_data_work_comp_class_code w "
                    + "JOIN w.client c where c.id = :id and w.stateProvCd = :stateProvCd", 
                       WorkCompClassCode.class)
                    .setParameter("id", securityProfile.getClient().getId().intValue())
                    .setParameter("stateProvCd", stateProvCd);
            
            if (!"*".equals(classCodeType)) {
                query = em.createQuery("SELECT w FROM ref_data_work_comp_class_code w "
                        + "JOIN w.client c where c.id = :id and w.stateProvCd = :stateProvCd and w.classCodeType = :classCodeType", 
                           WorkCompClassCode.class)
                        .setParameter("id", securityProfile.getClient().getId().intValue())
                        .setParameter("stateProvCd", stateProvCd)
                        .setParameter("classCodeType", classCodeType);
            }
                
            
            List<WorkCompClassCode> workCompClassCodeLists = getCachedResults( query);
            
            Map<String, String> variables = new HashMap<String, String>();
            workCompClassCodeLists.forEach(workCompClass -> {
                workCompClass.setValue(workCompClass.getAttrValue() +" - "+workCompClass.getValue());
                pojos.getOption().add(workCompClass);
                variables.put("stateProvCd", workCompClass.getStateProvCd());
                variables.put("classCodeType", classCodeType);
            });
            String href = URIBuilder.createURI(ProductDefinitionServiceResource.class, optionListResourceURLpattern, variables);
            pojos.getLink().add(new AtomLink("self", href));
            pojoList.add(pojos);
            
            
            builder.entity(StandardResponseEntity.create().generate(this.getAccepts(), new OptionListPojoOutputter<WorkCompClassCode>(this.getAccepts(),  pojoList,WorkCompClassCode.class)));
            return builder.build();

        } catch (Exception exception) {
            ExceptionLogger.log(exception, getClass(), functionName);
            return this.generateExceptionResponse(this.getAccepts(), new Exception("Could not find matching wc class code "), Status.PRODUCT_DEFINITION_ISSUE,
                    functionName);
        } finally {
            em.close();
        }
    
    }
    
    /**
     * Get Industry Code List  API.
     * @return Matching Industry Code List .
     */
    @GET
    @Path("/industrycode")
    @Produces({ MediaType.APPLICATION_XML })
    public Response getIndustryCodeLists(@QueryParam("industrycdtype") String industryCdType) {
        return getIndustryCodeListsResponse(industryCdType);
    }
    
    /**
     * Gets the industry code lists response.
     * @param industryCdType the industry cd type
     * @return the industry code lists response
     */
    public Response getIndustryCodeListsResponse( String industryCdType) {
        final String functionName = "getIndustryCodeListsResponse";
        EntityManager em = SpringContextBridge.services().getEntityManagerFactory().createEntityManager();
        ISecurityProfile securityProfile = SecurityProfileManager.get().acquire();
        try {
            Response.ResponseBuilder builder = this.getBuilder(this.getAccepts());
            final String optionListResourceURLpattern = "/industrycode?industrycdtype={industryCdType}";

            OptionList<IndustryCode> pojos = new OptionList<IndustryCode>();
            List<OptionList<IndustryCode>> pojoList = new ArrayList<OptionList<IndustryCode>>();
            
            List<IndustryCode> industryCodeLists = getCachedResults(em.createQuery("SELECT i FROM ref_data_industry_code i "
                    + "JOIN i.client c where c.id = :id and i.industryCdType = :industryCdType", 
                    IndustryCode.class)
                    .setParameter("id", securityProfile.getClient().getId().intValue())
                    .setParameter("industryCdType", industryCdType));
            
            Map<String, String> variables = new HashMap<String, String>();
            industryCodeLists.forEach(industryCode -> {
                pojos.getOption().add(industryCode);
                variables.put("industryCdType", industryCode.getIndustryCdType());
            });
            String href = URIBuilder.createURI(ProductDefinitionServiceResource.class, optionListResourceURLpattern, variables);
            pojos.getLink().add(new AtomLink("self", href));
            pojoList.add(pojos);
            
            
            builder.entity(StandardResponseEntity.create().generate(this.getAccepts(), new OptionListPojoOutputter<IndustryCode>(this.getAccepts(),  pojoList,IndustryCode.class)));
            return builder.build();

        } catch (Exception exception) {
            ExceptionLogger.log(exception, getClass(), functionName);
            return this.generateExceptionResponse(this.getAccepts(), new Exception("Could not find matching industry code "), Status.PRODUCT_DEFINITION_ISSUE,
                    functionName);
        } finally {
            em.close();
        }
    
    }
    /**
     * Gets the xarcrules.
     * @param productType the product type
     * @param productVersion the product version
     * @param ruleFileName the rule file name
     * @param ruleName the rule name
     * @return the xarcrules
     */
    @GET
    @Path("/xarcrules")
    @Produces({ MediaType.APPLICATION_XML })
    public Response getXarcrules() {
        return getXarcrulesResponse( "*",  "*","*",  "*");
    }
    
    /**
     * Gets the xarcrules.
     * @param productType the product type
     * @param productVersion the product version
     * @param ruleFileName the rule file name
     * @param ruleName the rule name
     * @return the xarcrules
     */
    @GET
    @Path("/xarcrules/{ruleFileName}/{ruleName}")
    @Produces({ MediaType.APPLICATION_XML })
    public Response getXarcrules( @DefaultValue("*") @QueryParam("productType") String productType,
            @DefaultValue("*") @QueryParam("productVersion") String productVersion,
            @DefaultValue("*") @PathParam("ruleFileName") String ruleFileName,
            @DefaultValue("*") @PathParam("ruleName") String ruleName
            ) {
        return getXarcrulesResponse( productType,  productVersion,ruleFileName,  ruleName);
    }
    
    /**
     * Gets the xarcrules response.
     * @param productType the product type
     * @param productVersion the product version
     * @param ruleFileName the rule file name
     * @param ruleName the rule name
     * @return the xarcrules response
     */
    public Response getXarcrulesResponse( String productType, String productVersion,String ruleFileName, String ruleName) {
        
        final String functionName = "getXarcrulesResponse";
        EntityManager em = SpringContextBridge.services().getEntityManagerFactory().createEntityManager();
        ISecurityProfile securityProfile = SecurityProfileManager.get().acquire();
        try {
            Response.ResponseBuilder builder = this.getBuilder(this.getAccepts());
            final String resourceURLpattern = "/xarcrules/{ruleFileName}/{ruleName}?productType={productType}&productVersion={productVersion}";
            Client eClient = SpringContextBridge.services().getClientRepository().findById(securityProfile.getClient().getId().intValue());
            List<Rule> ruleList = new ArrayList<Rule>();
            if (eClient == null) {
                eClient = new Client();
            } else {
            
                Product eProduct = getProducts(new ProductSearchKey( productType, "*", productVersion)).stream().findFirst().orElse(new Product());
                
                CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
                CriteriaQuery<RuleFile> criteriaQuery = criteriaBuilder.createQuery(RuleFile.class);
                Root<RuleFile> ruleFileRoot = criteriaQuery.from(RuleFile.class);
                List<Predicate> predicates = new ArrayList<Predicate>();
                predicates.add(criteriaBuilder.equal(ruleFileRoot.get("product"), eProduct));
                if(!"*".equals(ruleFileName)) {
                    predicates.add(criteriaBuilder.equal(ruleFileRoot.get("name"), ruleFileName));
                }
                criteriaQuery.select(ruleFileRoot)
                        .where(predicates.toArray(new Predicate[] {}));
    
                List<RuleFile> ruleFileList = getCachedResults (em.createQuery(criteriaQuery));
                        
    
                ruleFileList.forEach(ruleFile -> {
                    ruleFile.getRule().stream()
                    .filter(r->{ 
                        
                        if("*".equals(ruleName)){
                            return true;
                        }
                        
                        return ruleName.equals(r.getName());
                        
                        })
                    .forEach(rule->{
                        Map<String, String> variables = new HashMap<String, String>();
                        variables.put("productType", ruleFile.getProduct().getType());
                        variables.put("productVersion", ruleFile.getProduct().getVersion());
                        variables.put("ruleFileName", ruleFile.getName());
                        variables.put("ruleName", rule.getName());
                        String href = URIBuilder.createURI(ProductDefinitionServiceResource.class, resourceURLpattern, variables);
                        rule.getLink().add(new AtomLink("self", href));
                        ruleList.add(rule);
                    });
                });
            }
            
            builder.entity(StandardResponseEntity.create().generate(this.getAccepts(), new RuleOutputter(this.getAccepts(), ruleList)));
            return builder.build();
        } catch (Exception exception) {
            ExceptionLogger.log(exception, getClass(), functionName);
            return this.generateExceptionResponse(this.getAccepts(), new Exception("Could not find matching xarc rule file"), Status.PRODUCT_DEFINITION_ISSUE,
                    functionName);
        }

        finally {
            em.close();
        }
    
    }
    /**
     * Get Cached results
     * @param query
     * @return results
     */
    public <T extends Object> List<T> getCachedResults(TypedQuery<T> query) {
        List<T> results = query
                .setHint("org.hibernate.cacheable",true)
                .setHint("org.hibernate.cacheRegion",this.getClass().getName())
                .getResultList();
        
        return results;
    }
    
    @GET
    @Path("/products/{type}/{version}/transactions/{transactionId}/pdfdefinitions")
    @Produces({ MediaType.APPLICATION_XML })
    public Response getPdfdefinitions(@PathParam("type") String producttype,
            @PathParam("version") String productVersion,
            @PathParam("transactionId") String transactionId,
            @DefaultValue("*") @QueryParam("packagename") String packageName ) {

        ProductSearchKey searchKey = new ProductSearchKey( producttype, "*", productVersion);
        ProductSearchKey.TransactionSearchKey transactionSearchKey = new ProductSearchKey.TransactionSearchKey(transactionId, "*");
        return getPdfdefinitions(searchKey, transactionSearchKey,packageName);
    }

    /**
     * Build Pdfdefinitions response.
     * 
     * @param productSearchKey
     *            product search criteria.
     * @param transactionSearchKey
     *            transaction search key
     * @param packageName is name that is assigned to a group of PDF definitions.
     * @return response.
     */
    private Response getPdfdefinitions(ProductSearchKey productSearchKey, ProductSearchKey.TransactionSearchKey transactionSearchKey,String packageName) {
        EntityManager em = SpringContextBridge.services().getEntityManagerFactory().createEntityManager();
        final String functionName = "getPdfdefinitions";
        try {
            Response.ResponseBuilder builder = this.getBuilder(this.getAccepts());
            List<com.agencyport.configuration.entity.Product> productsfiltered = getProducts(productSearchKey);
            TTransaction tTransaction = getTransactions(productsfiltered, transactionSearchKey).stream().findFirst().orElse(new TTransaction());
            TTransaction tran = em.find(TTransaction.class, tTransaction.getAutoId());
            List<JAXBElement<TForms>> listOfTForms = new ArrayList<>();
            if (tran != null) {
                if( tran.gettForms() != null) {
                    if(!"*".equals(packageName)) {
                        if(tran.gettForms().getFormGroups() != null){
                           List<TGroup> groupsToRemove  =tran.gettForms().getFormGroups().getGroup().stream()
                                   .filter(g -> !g.getName().equals(packageName))
                                   .collect(Collectors.toList());
                           for(TGroup group: groupsToRemove){
                               tran.gettForms().getFormGroups().getGroup().remove(group);
                            }
                        }
                        List<TForm> formsToRemove = tran.gettForms().getForm().stream()
                                .filter(f -> !f.getName().equals(packageName))
                                .collect(Collectors.toList());
                        
                        for(TForm form: formsToRemove){
                            tran.gettForms().getForm().remove(form);
                         }
                    }
                    
                    listOfTForms.add(new com.agencyport.configuration.entity.jaxb.pdfdefinitions.ObjectFactory().createForms( tran.gettForms()));
                }
            } 
            
            builder.entity(
                    StandardResponseEntity.create().generate(this.getAccepts(), new PDFFormsOutputter(this.getAccepts(), listOfTForms)));
            return builder.build();

        } catch (Exception exception) {
            ExceptionLogger.log(exception, getClass(), functionName);
            return this.generateExceptionResponse(this.getAccepts(), new Exception("Could not find matching pdf definitions"), Status.PRODUCT_DEFINITION_ISSUE,
                    functionName);
        } finally {
            if (em != null)
                em.close();
        }
    }

}
