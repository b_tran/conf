/*
 * Created on Mar 23, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.agencyport.configuration.entity.Product;

/**
 * The ProductRepository class
 */
public interface ProductRepository extends JpaRepository<Product, Long> {

    /**
     * Find by client id.
     *
     * @param clientId the client id
     * @param date the date
     * @return the list
     */
    @Query(value = "SELECT * FROM product p where p.client_id = ?1 and effective_date <= ?2 and is_current_version = 1", nativeQuery = true)
    List<Product> findByClientId(int clientId, Date date);

    /**
     * Find by client id and type.
     *
     * @param clientId the client id
     * @param productType the product type
     * @param date the date
     * @return the list
     */
    @Query(value = "SELECT * FROM product p where p.client_id = ?1 and type = ?2  and effective_date <= ?3 and is_current_version = 1", nativeQuery = true)
    List<Product> findByClientIdAndType(int clientId, String productType, Date date);

    /**
     * Find by client id and type and version.
     *
     * @param clientId the client id
     * @param productType the product type
     * @param version the version
     * @param date the date
     * @return the list
     */
    @Query(value = "SELECT * FROM product p where p.client_id = ?1 and type = ?2 and version =3? and effective_date <= ?4  and is_current_version = 1", nativeQuery = true)
    List<Product> findByClientIdAndTypeAndVersion(int clientId, String productType, String version, Date date);

}
