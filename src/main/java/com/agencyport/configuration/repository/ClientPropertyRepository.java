package com.agencyport.configuration.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.agencyport.configuration.entity.jaxb.clientproperty.Property;

public interface ClientPropertyRepository extends JpaRepository<Property, Long>{
	
	@Query(value="SELECT * FROM client_property cp WHERE cp.client_id = ?1", nativeQuery=true)
	List<Property> findByClientId(int clientId);
}
