/*
 * Created on Mar 23, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.agencyport.configuration.entity.Client;

/**
 * The ProductRepository class
 */
public interface ClientRepository extends JpaRepository<Client, Long> {

    /**
     * Find by name.
     *
     * @param clientName the client name
     * @return the client
     */
    Client findByName(String clientName);

    /**
     * Find by id.
     *
     * @param id the id
     * @return the client
     */
    Client findById(int id);

}
