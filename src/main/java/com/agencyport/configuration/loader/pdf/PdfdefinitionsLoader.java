/*
 * Created on Mar 25, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.loader.pdf;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Service;

import com.agencyport.configuration.entity.Client;
import com.agencyport.configuration.entity.Product;
import com.agencyport.configuration.entity.jaxb.pdfdefinitions.ObjectFactory;
import com.agencyport.configuration.entity.jaxb.pdfdefinitions.TForms;
import com.agencyport.logging.ExceptionLogger;
import com.agencyport.logging.LoggingManager;

/**
 * The PDFDefinitionsLoader class
 */
@Service
public class PdfdefinitionsLoader {
	
    /**
     * The <code>logger</code> logger for this instance
     */
    final  Logger logger = LoggingManager.getLogger(PdfdefinitionsLoader.class.getName());
    
	/**
	 * The <code>transaction manager</code>.
	 */
	@Autowired
	JpaTransactionManager transactionManager;

	/**
	 * Load.
	 *
	 * @param client the client
	 * @param product the product
	 * @param configPDFDefintions the config behavior
	 * @param input the input
	 * @throws Exception the exception
	 */
	@SuppressWarnings("unchecked")
	public void load(Client client, Product product, Pdfdefinitions configPDFDefintions , InputStream input) throws Exception {
			logger.info("Loading :"+configPDFDefintions);
			EntityManagerFactory entityManagerFactory = transactionManager.getEntityManagerFactory();
			EntityManager em = entityManagerFactory.createEntityManager();
			try {
    			JAXBContext jc1 = JAXBContext.newInstance(com.agencyport.configuration.entity.jaxb.pdfdefinitions.TForms.class);
    			Unmarshaller u1 = jc1.createUnmarshaller();
    			JAXBElement<TForms> jaxbFroms = (JAXBElement<TForms>) u1.unmarshal(input);
    			TForms forms = jaxbFroms.getValue();
    			em.getTransaction().begin();
    			
    			Object c = em.createQuery("select c from client c where c.name = :name")
    			.setParameter("name", client.getName())
    			.getResultList().stream().findFirst().orElse(null);
    			
    			if(c != null) {
    				client = (Client)c;
    			}
    			
    			Object  p =  em.createQuery("select p from product p where p.clientId = :clientId "
    					+ "and p.type = :type and p.version = :version and p.effectiveDate = :effectiveDate")
    					.setParameter("clientId", client.getId())
    					.setParameter("type", product.getType())
    					.setParameter("version", product.getVersion())
    					.setParameter("effectiveDate", product.getEffectiveDate())
    					.getResultList().stream().findFirst().orElse(null);
    			
    			if(p!=null) {
    				product = (Product)(p);
    			}
    			
    			product.setClient(client);
    			product.gettTransactions().stream().filter(tran -> {
    				return tran.getId().equals(configPDFDefintions.getTransaction());
    				
    			}).forEach(tran -> {
    				tran.settForms(forms);
    				forms.setTransaction(tran);
    			});
    			
    			em.persist(client);
    			em.persist(product);
    			em.persist(forms);
    			em.flush();
    			em.getTransaction().commit();
			}catch(Exception exception) {
			    try {
                    if(em.getTransaction().isActive()) {
                        em.getTransaction().rollback();
                    }
                }catch (Exception exception2) {
                    ExceptionLogger.log(exception2, getClass(), "Transaction#rollback()");
                }
			    throw new Exception(configPDFDefintions.toString(),exception);
			}finally{
			em.close();
			}

	}

	/**
	 * Read.
	 *
	 * @param formsId the forms Id
	 * @return the string
	 * @throws Exception the exception
	 */
	public String read(int formsId) throws Exception {

		JAXBContext jc1 = JAXBContext.newInstance(com.agencyport.configuration.entity.jaxb.pdfdefinitions.TForms.class);
		Marshaller m1 = jc1.createMarshaller();

		EntityManagerFactory entityManagerFactory = transactionManager.getEntityManagerFactory();
		EntityManager em = entityManagerFactory.createEntityManager();
		
		try {
			
		    TForms tForms = em.find(TForms.class, formsId);
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			m1 = jc1.createMarshaller();
			m1.marshal(new ObjectFactory().createForms(tForms), baos);
			return new String(baos.toByteArray());

	
	}finally {
		em.close();
	}

}


}
