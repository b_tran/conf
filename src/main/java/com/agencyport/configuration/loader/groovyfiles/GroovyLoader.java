/*
 * Created on Jan 20, 2017 by btran AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.loader.groovyfiles;

import java.io.InputStream;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Service;

import com.agencyport.configuration.entity.Client;
import com.agencyport.configuration.entity.Product;
import com.agencyport.configuration.entity.groovy.GroovyModel;
import com.agencyport.logging.ExceptionLogger;
import com.agencyport.logging.LoggingManager;

/**
 * The GroovyLoader class, use to load groovy files into database
 */
@Service
public class GroovyLoader {
    /**
     * The <code>logger</code> logger for this instance
     */
	
    final  Logger logger = LoggingManager.getLogger(GroovyLoader.class.getName());


	/**
	 * The <code>transaction manager</code>.
	 */
	@Autowired
	JpaTransactionManager transactionManager;

	/**
	 * Load.
	 *
	 * @param client the client
	 * @param groovy the groovy
	 * @param input the input
	 * @throws Exception the exception
	 */
	@SuppressWarnings("unchecked")
	public void load(Product product, Client client, Groovy groovy, InputStream input) throws Exception {
			logger.info("Loading :"+ groovy);
			EntityManagerFactory entityManagerFactory = transactionManager.getEntityManagerFactory();
			EntityManager em = entityManagerFactory.createEntityManager();
			try {
				//constructs the blob from inputstream
                StringBuilder builder = new StringBuilder();
                int ch;
                while((ch = input.read()) != -1){
                    builder.append((char)ch);
                }
                String blob = builder.toString();
                
	            GroovyModel groovyfile = new GroovyModel();
	            groovyfile.setProduct(product);
    			groovyfile.setGroovyName(groovy.getName());
    			groovyfile.setGroovyFile(blob);
    			groovyfile.setClient(client);
    			
                //persists the blob into database
    			em.getTransaction().begin();
    			em.persist(groovyfile);
    			em.flush();
    			em.getTransaction().commit();
			}catch (Exception exception){
			    try {
                    if(em.getTransaction().isActive()) {
                        em.getTransaction().rollback();
                    }
                }catch (Exception exception2) {
                    ExceptionLogger.log(exception2, getClass(), "Transaction#rollback()");
                }
			    throw new Exception("Error loading groovy file: " + groovy.toString(), exception);
			}finally {
			    em.close();
			}
	}
}
