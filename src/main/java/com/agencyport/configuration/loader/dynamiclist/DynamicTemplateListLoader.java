/*
 * Created on Mar 25, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.loader.dynamiclist;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Service;

import com.agencyport.configuration.entity.Client;
import com.agencyport.configuration.entity.jaxb.dynamiclisttemplate.TDynamicListTemplate;
import com.agencyport.configuration.entity.jaxb.dynamiclisttemplate.TDynamicListTemplates;
import com.agencyport.logging.ExceptionLogger;
import com.agencyport.logging.LoggingManager;

/**
 * The DynamicTemplateListLoader class
 */
@Service
public class DynamicTemplateListLoader {
    /**
     * The <code>logger</code> logger for this instance
     */
    final  Logger logger = LoggingManager.getLogger(DynamicTemplateListLoader.class.getName());

	/**
	 * The <code>transaction manager</code>.
	 */
	@Autowired
	JpaTransactionManager transactionManager;

	/**
	 * Load.
	 *
	 * @param client the client
	 * @param dynamicTemplateList the dynamic template list
	 * @param input the input
	 * @throws Exception the exception
	 */
	@SuppressWarnings("unchecked")
	public void load(Client client,com.agencyport.configuration.loader.dynamiclist.DynamicListTemplate dynamicTemplateList, InputStream input) throws Exception {
        logger.info("Loading :"+dynamicTemplateList);
        EntityManagerFactory entityManagerFactory = transactionManager.getEntityManagerFactory();
        EntityManager em = entityManagerFactory.createEntityManager();
        JAXBContext jc1 = JAXBContext.newInstance(com.agencyport.configuration.entity.jaxb.dynamiclisttemplate.ObjectFactory.class);
        Unmarshaller u1 = jc1.createUnmarshaller();
        JAXBElement<TDynamicListTemplates> jaxbTDynamicListTemplates = (JAXBElement<TDynamicListTemplates>) u1.unmarshal(input);
        TDynamicListTemplates eTDynamicListTemplates = jaxbTDynamicListTemplates.getValue();
        try {
            for(TDynamicListTemplate eTDynamicListTemplate: eTDynamicListTemplates.getDynamicListTemplate()) {
                em.getTransaction().begin();
                
                Object c = em.createQuery("select c from client c where c.name = :name")
                .setParameter("name", client.getName())
                .getResultList().stream().findFirst().orElse(null);
                
                if(c != null) {
                    client = (Client)c;
                }
                
                eTDynamicListTemplate.setClient(client);
                client.getDynamicListTemplate().add(eTDynamicListTemplate);
                eTDynamicListTemplate.setEffectiveDate(dynamicTemplateList.getEffective_date());
                eTDynamicListTemplate.setCurrentVersion(dynamicTemplateList.isIs_current_version());
                eTDynamicListTemplate.getField().forEach(f->{
                    f.setDynamicListTemplate(eTDynamicListTemplate);
                    
                });
                
                em.persist(client);
                em.flush();
                em.getTransaction().commit();
                
            }
        }catch (Exception exception) {
            try {
                if(em.getTransaction().isActive()) {
                    em.getTransaction().rollback();
                }
            }catch (Exception exception2) {
                ExceptionLogger.log(exception2, getClass(), "Transaction#rollback()");
            }
            throw new Exception(dynamicTemplateList.toString(),exception);
        }finally {
            em.close();
        }

}

	/**
	 * Read.
	 *
	 * @param dynamicTemplateListId the dynamic template list id
	 * @return the string
	 * @throws Exception the exception
	 */
	public String read(int dynamicTemplateListId) throws Exception {

		JAXBContext jc1 = JAXBContext.newInstance(com.agencyport.configuration.entity.jaxb.dynamiclisttemplate.ObjectFactory.class);
		Marshaller m1 = jc1.createMarshaller();

		EntityManagerFactory entityManagerFactory = transactionManager.getEntityManagerFactory();
		EntityManager em = entityManagerFactory.createEntityManager();
		
		try {
			
		    TDynamicListTemplate eDynamicListTemplate = em.find(TDynamicListTemplate.class, dynamicTemplateListId);
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			m1 = jc1.createMarshaller();
			m1.marshal(eDynamicListTemplate, baos);
			return new String(baos.toByteArray());

	
	}finally {
		em.close();
	}

}


}
