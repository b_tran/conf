/*
 * Created on Mar 25, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.loader.workflow;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Service;

import com.agencyport.configuration.entity.Client;
import com.agencyport.configuration.entity.jaxb.workflowdefinition.TReasonOptions;
import com.agencyport.configuration.entity.jaxb.workflowdefinition.TReasons;
import com.agencyport.configuration.entity.jaxb.workflowdefinition.TWorkFlow;
import com.agencyport.logging.ExceptionLogger;
import com.agencyport.logging.LoggingManager;

/**
 * The WorkFlowLoader class
 */
@Service
public class WorkFlowLoader {
    
    /**
     * The <code>logger</code> logger for this instance
     */
    final  Logger logger = LoggingManager.getLogger(WorkFlowLoader.class.getName());
    
	

	/**
	 * The <code>transaction manager</code>.
	 */
	@Autowired
	JpaTransactionManager transactionManager;

	/**
	 * Load.
	 *
	 * @param client the client
	 * @param workflow the workflow
	 * @param input the input
	 * @throws Exception the exception
	 */
	@SuppressWarnings("unchecked")
	public void load(Client client,com.agencyport.configuration.loader.workflow.WorkFlow workflow, InputStream input) throws Exception {
			logger.info("Loading :"+workflow);
			EntityManagerFactory entityManagerFactory = transactionManager.getEntityManagerFactory();
			EntityManager em = entityManagerFactory.createEntityManager();
			try {
    			JAXBContext jc1 = JAXBContext.newInstance(com.agencyport.configuration.entity.jaxb.workflowdefinition.ObjectFactory.class);
    			Unmarshaller u1 = jc1.createUnmarshaller();
    			JAXBElement<TWorkFlow> jaxbWorkFlow = (JAXBElement<TWorkFlow>) u1.unmarshal(input);
    			TWorkFlow eTWorkFlow = jaxbWorkFlow.getValue();
    			eTWorkFlow.setEffectiveDate(workflow.getEffective_date());
    			eTWorkFlow.setCurrentVersion(workflow.isIs_current_version());
    			em.getTransaction().begin();
    			
    			Object c = em.createQuery("select c from client c where c.name = :name")
    			.setParameter("name", client.getName())
    			.getResultList().stream().findFirst().orElse(null);
    			
    			if(c != null) {
    				client = (Client)c;
    			}
    			
    			eTWorkFlow.setClient(client);
    			client.getWorkFlow().add(eTWorkFlow);
    			
    			eTWorkFlow.getWorkFlowDescriptor().forEach(workflowDescriptor -> {
    			    workflowDescriptor.setWorkFlow(eTWorkFlow);
    			    workflowDescriptor.getTask().forEach(task->{
    			        task.setWorkFlowDescriptor(workflowDescriptor);
    			        task.getWhen().forEach(when->{
	                        when.setTask(task);
	                        if(when.getScript() != null){
	                            when.getScript().forEach(script->{
	                            	script.setWhen(when);
	                            });
	                        }
	                        if(when.getUri() != null){
	                        	when.getUri().setWhen(when);
	                        }
	                    });
    			        
    			    });
    			});
    			if(eTWorkFlow.getWorkItemActionDefinition()!=null) {
    			    
    			    eTWorkFlow.getWorkItemActionDefinition().setWorkFlow(eTWorkFlow);
    	            
    	            eTWorkFlow.getWorkItemActionDefinition().getQueue().forEach(q->{
    	                q.setWorkItemActionDefinition(eTWorkFlow.getWorkItemActionDefinition());
    	                q.getWorkItemAction().forEach(action->{
    	                    action.setQueue(q);
    	                    if(action.getPageConstraints() != null) {
    	                        action.getPageConstraints().setWorkItemAction(action);
    	                    }
    	                    if (action.getStatusConstraints() != null) {
    	                    	action.getStatusConstraints().setWorkItemAction(action);
    	                    }
    	                    if(action.getToStatus() != null) {
    	                        action.getToStatus().setWorkItemAction(action);
    	                    }
	                    TReasons reasons = action.getReasons();
	                    if (reasons != null) {
	                    	TReasonOptions reasonOptions = reasons.getReasonOptions();
	                    	if (reasonOptions != null) {
	                    		reasonOptions.setReasons(reasons);
	                    		reasonOptions.getReason().forEach(reason -> {
	                    			reason.setReasonOptions(reasonOptions);
	                    		});
	                    	}
	                    	if (reasons.getAllowReasonComment() != null) {
	                    		reasons.getAllowReasonComment().setReasons(reasons);
	                    	}
	                        action.getReasons().setWorkItemAction(action);
	                    }
    	                    if(action.getUri() !=null){
    	                        action.getUri().setWorkItemAction(action);
    	                    }
    	                    if(action.getAssign() !=null){
    	                        action.getAssign().setWorkItemAction(action);
    	                    }
    	                    if (action.getModal() != null) {
    	                    	action.getModal().setWorkItemAction(action);
    	                    }
    	                    if(action.getBehaviorConstraints() != null) {
    	                    	action.getBehaviorConstraints().setWorkItemAction(action);
    	                    	action.getBehaviorConstraints().getWhere().forEach(where->{
    	                    		where.setBehaviorConstraint(action.getBehaviorConstraints());
    	                    		where.getPreCondition().forEach(preCondition->{
    	                    			preCondition.setWhere(where);
    	                    		});
    	                    	});
    	                    }
    	                    action.getWhen().forEach(when->{
    	                        when.setWorkItemAction(action);
    	                        if(when.getScript() != null){
    	                            when.getScript().forEach(script->{
    	                            	script.setWhen(when);
    	                            });
    	                            
    	                        }
    	                    });
    	                });
    	            });
    			}
    			
    			if(eTWorkFlow.getWorkItemStatusDefinition() !=null){
    			    eTWorkFlow.getWorkItemStatusDefinition().setWorkFlow(eTWorkFlow);
    			    eTWorkFlow.getWorkItemStatusDefinition().getStatus().forEach(status->{
    	                status.setWorkItemStatusDefinition(eTWorkFlow.getWorkItemStatusDefinition());
    	                
    	            });
    			    eTWorkFlow.getWorkItemStatusDefinition().getTransition().forEach(transition->{
    	                transition.setWorkItemStatusDefinition(eTWorkFlow.getWorkItemStatusDefinition());
    	            });
    			}
    			
    			em.persist(client);
    			em.persist(eTWorkFlow);
    			em.flush();
    			em.getTransaction().commit();
			}catch(Exception exception) {
			    try {
                    if(em.getTransaction().isActive()) {
                        em.getTransaction().rollback();
                    }
                }catch (Exception exception2) {
                    ExceptionLogger.log(exception2, getClass(), "Transaction#rollback()");
                }
			    throw new Exception(workflow.toString(),exception);
			}finally {
			    em.close();
			}
	}

	/**
	 * Read.
	 *
	 * @param workFlowId the work flow id
	 * @return the string
	 * @throws Exception the exception
	 */
	public String read(int workFlowId) throws Exception {

		JAXBContext jc1 = JAXBContext.newInstance(com.agencyport.configuration.entity.jaxb.workflowdefinition.TWorkFlow.class);
		Marshaller m1 = jc1.createMarshaller();

		EntityManagerFactory entityManagerFactory = transactionManager.getEntityManagerFactory();
		EntityManager em = entityManagerFactory.createEntityManager();
		
		try {
			
		    TWorkFlow wTWorkFlow = em.find(TWorkFlow.class, workFlowId);
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			m1 = jc1.createMarshaller();
			m1.marshal(wTWorkFlow, baos);
			return new String(baos.toByteArray());

	
	}finally {
		em.close();
	}

}


}
