/*
 * Created on Mar 25, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.loader.workflow;

import java.util.Date;

/**
 * The WorkFlow class
 */
public class WorkFlow {
	
	/**
	 * The <code>effective_date</code>.
	 */
	private Date effective_date;
	
	/**
	 * The <code>is_current_version</code>.
	 */
	private boolean is_current_version;
	
	/**
	 * The <code>location</code>.
	 */
	private String location;

	/**
	 * Gets the location.
	 *
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * Sets the location.
	 *
	 * @param location the new location
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * Gets the effective_date.
	 *
	 * @return the effective_date
	 */
	public Date getEffective_date() {
		return effective_date;
	}

	/**
	 * Sets the effective_date.
	 *
	 * @param effective_date the new effective_date
	 */
	public void setEffective_date(Date effective_date) {
		this.effective_date = effective_date;
	}

	/**
	 * Checks if is is_current_version.
	 *
	 * @return true, if is is_current_version
	 */
	public boolean isIs_current_version() {
		return is_current_version;
	}

	/**
	 * Sets the is_current_version.
	 *
	 * @param is_current_version the new is_current_version
	 */
	public void setIs_current_version(boolean is_current_version) {
		this.is_current_version = is_current_version;
	}
	
	/** 
	 * {@inheritDoc}
	 */ 
	
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("effective_date: "+effective_date);
		buffer.append(" is_current_version: "+is_current_version);
		buffer.append(" location: "+location);
		return buffer.toString();
	}

	
	
	
	


}
