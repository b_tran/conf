/*
 * Created on Mar 25, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.loader.xarcrules;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Service;

import com.agencyport.configuration.entity.Client;
import com.agencyport.configuration.entity.Product;
import com.agencyport.configuration.entity.jaxb.xarcrules.RuleFile;
import com.agencyport.logging.ExceptionLogger;
import com.agencyport.logging.LoggingManager;

/**
 * The XarcRulesLoader class
 */
@Service
public class XarcRulesLoader {
    /**
     * The <code>logger</code> logger for this instance
     */
    final  Logger logger = LoggingManager.getLogger(XarcRulesLoader.class.getName());
    

	/**
	 * The <code>transaction manager</code>.
	 */
	@Autowired
	JpaTransactionManager transactionManager;

	/**
	 * Load.
	 *
	 * @param client the client
	 * @param product the product
	 * @param xarcrulesConfig the xarcrules config
	 * @param input the input
	 * @throws Exception the exception
	 */
	@SuppressWarnings("unchecked")
	public void load(Client client, Product product, Xarcrules xarcrulesConfig, InputStream input) throws Exception {
			logger.info("Loading :"+xarcrulesConfig);
			EntityManagerFactory entityManagerFactory = transactionManager.getEntityManagerFactory();
			EntityManager em = entityManagerFactory.createEntityManager();
			try {
    			JAXBContext jc1 = JAXBContext.newInstance(com.agencyport.configuration.entity.jaxb.xarcrules.RuleFile.class);
    			Unmarshaller u1 = jc1.createUnmarshaller();
    			RuleFile ruleFile =( RuleFile )u1.unmarshal(input);
    			em.getTransaction().begin();
    			
    			Object c = em.createQuery("select c from client c where c.name = :name")
    			.setParameter("name", client.getName())
    			.getResultList().stream().findFirst().orElse(null);
    			
    			if(c != null) {
    				client = (Client)c;
    			}
    			
    			Object  p =  em.createQuery("select p from product p where p.clientId = :clientId "
    					+ "and p.type = :type and p.version = :version and p.effectiveDate = :effectiveDate")
    					.setParameter("clientId", client.getId())
    					.setParameter("type", product.getType())
    					.setParameter("version", product.getVersion())
    					.setParameter("effectiveDate", product.getEffectiveDate())
    					.getResultList().stream().findFirst().orElse(null);
    			
    			if(p!=null) {
    				product = (Product)(p);
    			}
    			
    			ruleFile.getRule().forEach(rule->{
    			    
    			    rule.setRuleFile(ruleFile);
    			    rule.getAction().setRule(rule);
    			    
    			    rule.getAction().getMessage().forEach(message-> {
    			        message.setAction(rule.getAction());
    			    });
    			    
    			    rule.getStep().setRule(rule);
    			    rule.getStep().getSelectNodes().setStep(rule.getStep());
    			    if(rule.getStep().getSelectNodes().getVariables() != null) {
    			        rule.getStep().getSelectNodes().getVariables().setSelectNodes(rule.getStep().getSelectNodes());
        			    rule.getStep().getSelectNodes().getVariables().getVariable().forEach(variable->{
        			        variable.setVariables(rule.getStep().getSelectNodes().getVariables());
        			        if(variable.getField() !=null)
        			            variable.getField().setVariable(variable);
        			        
        			        if(variable.getVariableReference() !=null)
        			            variable.getVariableReference().setVariable(variable);
        			        
        			        if(variable.getConstant() !=null)
        			            variable.getConstant().setVariable(variable);
        			        
        			        if(variable.getExpression() !=null) {
        			            variable.getExpression().setVariable(variable);
        			            if( variable.getExpression().getExpressionOperator() != null) {
        			            	variable.getExpression().getExpressionOperator().setExpression(variable.getExpression());
                                    
                                    if(variable.getExpression().getExpressionOperator().getExpressionOperand() != null) {
                                        
                                    	variable.getExpression().getExpressionOperator().getExpressionOperand().forEach(expOperand->{
                                        	
                                        	//Had to set this as xarc_expression_operand should contain the foreign key value which refers to xarc_expression_operator table primary key
                                        	expOperand.setExpressionOperator(variable.getExpression().getExpressionOperator());
       
                                        	if(expOperand.getDate() !=null){
                                                expOperand.getDate().setExpressionOperand(expOperand);
                                            }
                                            if(expOperand.getField() !=null){
                                                expOperand.getField().setExpressionOperand(expOperand);
                                            }
                                            if(expOperand.getSql() !=null){
                                                expOperand.getSql().setExpressionOperand(expOperand);
                                            }
                                            if(expOperand.getVariableReference() !=null){
                                                expOperand.getVariableReference().setExpressionOperand(expOperand);
                                            }
                                            if(expOperand.getXpath() !=null){
                                                expOperand.getXpath().setExpressionOperand(expOperand);
                                            }
                                            if( expOperand.getConstant() != null){
                                                expOperand.getConstant().setExpressionOperand(expOperand);
                                            }
                                        });
                                        
                                        
                                    }
                                    
                                }
        			        }
        			        
        			        if(variable.getXpath() !=null)
        			            variable.getXpath().setVariable(variable);
        			        
        			        if(variable.getDate() !=null)
        			            variable.getDate().setVariable(variable);
        			        
        			        if(variable.getSql() !=null)
        			            variable.getSql().setVariable(variable);
        			        
        			    });
    			    }
    			    rule.getStep().getSelectNodes().getWhere().forEach(where->{
    			        where.setSelectNodes(rule.getStep().getSelectNodes());
    			        where.getOperator().setWhere(where);
    			        
    			        where.getOperator().getOperand().forEach(operand->{
    			            operand.setOperator(where.getOperator());
    			            
    			            if(operand.getConstant() != null) {
        			            operand.getConstant().setOperand(operand);
        			            
    			            }
    			            if(operand.getDate() != null) {
    			                operand.getDate().setOperand(operand);
    			            }
    			            
    			            if( operand.getExpression() != null) {
                                operand.getExpression().setOperand(operand);
                                
                                if( operand.getExpression().getExpressionOperator() != null) {
                                    operand.getExpression().getExpressionOperator().setExpression(operand.getExpression());
                                    
                                    if(operand.getExpression().getExpressionOperator().getExpressionOperand() != null) {
                                        
                                        operand.getExpression().getExpressionOperator().getExpressionOperand().forEach(expOperand->{
                                        	
                                        	//Had to set this as xarc_expression_operand should contain the foreign key value which refers to xarc_expression_operator table primary key
                                        	expOperand.setExpressionOperator(operand.getExpression().getExpressionOperator());
       
                                        	if(expOperand.getDate() !=null){
                                                expOperand.getDate().setExpressionOperand(expOperand);
                                            }
                                            if(expOperand.getField() !=null){
                                                expOperand.getField().setExpressionOperand(expOperand);
                                            }
                                            if(expOperand.getSql() !=null){
                                                expOperand.getSql().setExpressionOperand(expOperand);
                                            }
                                            if(expOperand.getVariableReference() !=null){
                                                expOperand.getVariableReference().setExpressionOperand(expOperand);
                                            }
                                            if(expOperand.getXpath() !=null){
                                                expOperand.getXpath().setExpressionOperand(expOperand);
                                            }
                                            if( expOperand.getConstant() != null){
                                                expOperand.getConstant().setExpressionOperand(expOperand);
                                            }
                                        });
                                        
                                        
                                    }
                                    
                                }
                                
                            }
    			            if(operand.getField() !=null)
    			                operand.getField().setOperand(operand);
    			            
    			            if(operand.getSql() !=null)
    			                operand.getSql().setOperand(operand);
    			            
    			            if(operand.getVariableReference() !=null)
    			                operand.getVariableReference().setOperand(operand);
    			            
    			            if(operand.getXpath() !=null)
    			                operand.getXpath().setOperand(operand);
    			            
    			        });
    			        if(where.getJavaclass() != null)
    			            where.getJavaclass().setWhere(where);
    		            
    			        if(where.getXpath() != null)
    			            where.getXpath().setWhere(where);
    			    });
    			    
    			});
    			product.setClient(client);
    			ruleFile.setProduct(product);
    			product.getRuleFile().add(ruleFile);
    			
    			em.persist(product);
    			em.flush();
    			em.getTransaction().commit();
			}catch(Exception exception) {
			    try {
                    if(em.getTransaction().isActive()) {
                        em.getTransaction().rollback();
                    }
                }catch (Exception exception2) {
                    ExceptionLogger.log(exception2, getClass(), "Transaction#rollback()");
                }
                throw new Exception(xarcrulesConfig.toString(),exception);
			}finally{
			    em.close();
			}

	}
	

	/**
	 * Read.
	 *
	 * @param ruleFileId the rule file id
	 * @return the string
	 * @throws Exception the exception
	 */
	public String read(int ruleFileId) throws Exception {

		JAXBContext jc1 = JAXBContext.newInstance(com.agencyport.configuration.entity.jaxb.xarcrules.RuleFile.class);
		Marshaller m1 = jc1.createMarshaller();

		EntityManagerFactory entityManagerFactory = transactionManager.getEntityManagerFactory();
		EntityManager em = entityManagerFactory.createEntityManager();
		
		try {
		    RuleFile ruleFile = em.find(RuleFile.class, ruleFileId);


			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			m1 = jc1.createMarshaller();
			m1.marshal(ruleFile, baos);
			return new String(baos.toByteArray());

	
	}finally {
		em.close();
	}

}


}
