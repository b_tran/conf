/*
 * Created on Mar 25, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.loader.behavior;

/**
 * The Behavior class
 */
public class Behavior {
	

	
	/**
	 * The <code>transaction</code>.
	 */
	private String transaction;
	
	/**
	 * The <code>location</code>.
	 */
	private String location;

	/**
	 * Gets the transaction.
	 *
	 * @return the transaction
	 */
	public String getTransaction() {
		return transaction;
	}

	/**
	 * Sets the transaction.
	 *
	 * @param transaction the new transaction
	 */
	public void setTransaction(String transaction) {
		this.transaction = transaction;
	}

	/**
	 * Gets the location.
	 *
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * Sets the location.
	 *
	 * @param location the new location
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/** 
	 * {@inheritDoc}
	 */ 
	
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(" transaction: "+transaction);
		buffer.append(" location: "+location);
		return buffer.toString();
	}
	
	
	


}
