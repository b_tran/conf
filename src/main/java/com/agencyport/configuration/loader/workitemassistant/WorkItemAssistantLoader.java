/*
 * Created on Mar 25, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.loader.workitemassistant;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Service;

import com.agencyport.configuration.entity.Client;
import com.agencyport.configuration.entity.jaxb.workitemassistant.Workitemassistant;
import com.agencyport.logging.ExceptionLogger;
import com.agencyport.logging.LoggingManager;

/**
 * The WorkItemAssistantLoader class
 */
@Service
public class WorkItemAssistantLoader {

    /**
     * The <code>logger</code> logger for this instance
     */
    final  Logger logger = LoggingManager.getLogger(WorkItemAssistantLoader.class.getName());
    

	/**
	 * The <code>transaction manager</code>.
	 */
	@Autowired
	JpaTransactionManager transactionManager;

	
	/**
	 * Load.
	 *
	 * @param client the client
	 * @param workItemAssistant the work item assistant
	 * @param input the input
	 * @throws Exception the exception
	 */
	@SuppressWarnings("unchecked")
	public void load(Client client,com.agencyport.configuration.loader.workitemassistant.WorkItemAssistant workItemAssistant, InputStream input) throws Exception {
			logger.info("Loading :"+workItemAssistant);
			EntityManagerFactory entityManagerFactory = transactionManager.getEntityManagerFactory();
			EntityManager em = entityManagerFactory.createEntityManager();
			try {
    			JAXBContext jc1 = JAXBContext.newInstance(com.agencyport.configuration.entity.jaxb.workitemassistant.ObjectFactory.class);
    			Unmarshaller u1 = jc1.createUnmarshaller();
    			Workitemassistant eWorkitemassistant = (Workitemassistant) u1.unmarshal(input);
    			eWorkitemassistant.setEffectiveDate(workItemAssistant.getEffective_date());
    			eWorkitemassistant.setCurrentVersion(workItemAssistant.isIs_current_version());
    			em.getTransaction().begin();
    			
    			Object c = em.createQuery("select c from client c where c.name = :name")
    			.setParameter("name", client.getName())
    			.getResultList().stream().findFirst().orElse(null);
    			
    			if(c != null) {
    				client = (Client)c;
    			}
    			
    			eWorkitemassistant.setClient(client);
    			client.getWorkitemassistant().add(eWorkitemassistant);
    			
    			eWorkitemassistant.getSection().forEach(section->{
    			    section.setWorkitemassistant(eWorkitemassistant);
    			    section.getCodeListRef().forEach(codeListRef->{
    			        codeListRef.setSection(section);
    			    });
    			});
    			
    			
    			em.persist(client);
    			em.persist(eWorkitemassistant);
    			em.flush();
    			em.getTransaction().commit();
			}catch(Exception exception) {
			    try {
                    if(em.getTransaction().isActive()) {
                        em.getTransaction().rollback();
                    }
                }catch (Exception exception2) {
                    ExceptionLogger.log(exception2, getClass(), "Transaction#rollback()");
                }
			    throw new Exception(workItemAssistant.toString(),exception);
			}finally {
			    em.close();
			}

	}

	
	/**
	 * Read.
	 *
	 * @param menuDefId the menu def id
	 * @return the string
	 * @throws Exception the exception
	 */
	public String read(int menuDefId) throws Exception {

		JAXBContext jc1 = JAXBContext.newInstance(com.agencyport.configuration.entity.jaxb.workitemassistant.ObjectFactory.class);
		Marshaller m1 = jc1.createMarshaller();

		EntityManagerFactory entityManagerFactory = transactionManager.getEntityManagerFactory();
		EntityManager em = entityManagerFactory.createEntityManager();
		
		try {
			
		    WorkItemAssistant eWorkItemAssistant = em.find(WorkItemAssistant.class, menuDefId);
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			m1 = jc1.createMarshaller();
			m1.marshal(eWorkItemAssistant, baos);
			return new String(baos.toByteArray());

	
	}finally {
		em.close();
	}

}


}
