/*
 * Created on Mar 25, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.loader.transdef;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Service;

import com.agencyport.configuration.entity.Client;
import com.agencyport.configuration.entity.Product;
import com.agencyport.configuration.entity.jaxb.transdef.ObjectFactory;
import com.agencyport.configuration.entity.jaxb.transdef.TPage;
import com.agencyport.configuration.entity.jaxb.transdef.TTransaction;
import com.agencyport.logging.ExceptionLogger;
import com.agencyport.logging.LoggingManager;

/**
 * The TDFLoader class
 */
@Service
public class TDLoader {

    /**
     * The <code>logger</code> logger for this instance
     */
    final  Logger logger = LoggingManager.getLogger(TDLoader.class.getName());
    

	/**
	 * The <code>transaction manager</code>.
	 */
	@Autowired
	JpaTransactionManager transactionManager;

	/**
	 * Load.
	 *
	 * @param client the client
	 * @param product the product
	 * @param configTransaction the config transaction
	 * @param input the input
	 * @throws Exception the exception
	 */
	@SuppressWarnings("unchecked")
	public void load(Client client, Product product, Transaction configTransaction, InputStream input) throws Exception {
			logger.info("Loading :"+configTransaction);
			EntityManagerFactory entityManagerFactory = transactionManager.getEntityManagerFactory();
			EntityManager em = entityManagerFactory.createEntityManager();
			try {
    			JAXBContext jc1 = JAXBContext.newInstance(com.agencyport.configuration.entity.jaxb.transdef.TTransaction.class);
    			Unmarshaller u1 = jc1.createUnmarshaller();
    			JAXBElement<TTransaction> jaxbTransaction = (JAXBElement<TTransaction>) u1.unmarshal(input);
    			em.getTransaction().begin();
    			
    			Object c = em.createQuery("select c from client c where c.name = :name")
    			.setParameter("name", client.getName())
    			.getResultList().stream().findFirst().orElse(null);
    			
    			if(c != null) {
    				client = (Client)c;
    			}
    			
    			Object  p =  em.createQuery("select p from product p where p.clientId = :clientId "
    					+ "and p.type = :type and p.version = :version and p.effectiveDate = :effectiveDate")
    					.setParameter("clientId", client.getId())
    					.setParameter("type", product.getType())
    					.setParameter("version", product.getVersion())
    					.setParameter("effectiveDate", product.getEffectiveDate())
    					.getResultList().stream().findFirst().orElse(null);
    			
    			if(p!=null) {
    				product = (Product)(p);
    			}
    			
    			TTransaction transaction = jaxbTransaction.getValue();
    			product.setClient(client);
    			transaction.setProduct(product);
    			transaction.transferToEntity();
    
    			for (TPage page : transaction.getTPage()) {
    				page.getPageElement().forEach(pageElem -> {
    					pageElem.setPage(page);
    					pageElem.getFieldElement().forEach(fieldElem -> {
    						fieldElem.setPageelement(pageElem);
    						fieldElem.transferToEntity();
    					});
    					pageElem.getAction().forEach(action->{
    						action.setPageelement(pageElem);
    					});
    					pageElem.getJoinInfo().forEach(join->{
    						join.setPageelement(pageElem);
    					});
    					
    					
    				});
    				page.getConnectors().forEach(connectors -> {
    					connectors.setPage(page);
    					connectors.getConnector().forEach(connector -> {
    						connector.setConnectors(connectors);
    						connector.getExecuteWhen().forEach(executewhen -> {
    							executewhen.setConnector(connector);
    						});
    						connector.getXarcRules().forEach(xarchrule -> {
    							xarchrule.setConnector(connector);
    						});
    						connector.getCustomParameters().forEach(customeparam -> {
    							customeparam.transferToEntity();
    							customeparam.setConnector(connector);
    
    						});
    					});
    					connectors.getInstruction().forEach(instruction -> {
    						instruction.setConnectors(connectors);
    						instruction.transferToEntity();
    					});
    
    				});
    			}
    			em.persist(client);
    			em.persist(product);
    			em.persist(transaction);
    			em.flush();
    			em.getTransaction().commit();
			}catch(Exception exception) {
			    try {
                    if(em.getTransaction().isActive()) {
                        em.getTransaction().rollback();
                    }
                }catch (Exception exception2) {
                    ExceptionLogger.log(exception2, getClass(), "Transaction#rollback()");
                }
			    throw new Exception(configTransaction.toString(),exception);
			}finally {
			    em.close();
			}

	}

	/**
	 * Read.
	 *
	 * @param transactionId the transaction id
	 * @return the string
	 * @throws Exception the exception
	 */
	public String read(int transactionId) throws Exception {

		JAXBContext jc1 = JAXBContext.newInstance(com.agencyport.configuration.entity.jaxb.transdef.TTransaction.class);
		Marshaller m1 = jc1.createMarshaller();

		EntityManagerFactory entityManagerFactory = transactionManager.getEntityManagerFactory();
		EntityManager em = entityManagerFactory.createEntityManager();
		
		try {
			TTransaction tran = em.find(TTransaction.class, transactionId);

			tran.transferFromEntity();

			for (TPage page : tran.getTPage()) {
				page.getPageElement().forEach(pageElem -> {
					pageElem.getFieldElement().forEach(fieldElem -> {
						fieldElem.transferFromEntity();
					});
				});
				page.getConnectors().forEach(connectors -> {
					connectors.getConnector().forEach(connector -> {
						connector.getCustomParameters().forEach(param -> {

						});
					});
					connectors.getInstruction().forEach(instruction -> {
						instruction.transferFromEntity();
					});

				});
			}
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			m1 = jc1.createMarshaller();
			m1.marshal(new ObjectFactory().createTransaction(tran), baos);
			return new String(baos.toByteArray());

	
	}finally {
		em.close();
	}

}


}
