/*
 * Created on Mar 25, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.loader;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.core.io.ClassPathResource;

import com.agencyport.configuration.loader.behavior.TDBehaviorLoader;
import com.agencyport.configuration.loader.transdef.TDLoader;
import com.agencyport.logging.LoggingManager;

/**
 * The Reader class
 */
public class Reader {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		 final  Logger logger = LoggingManager.getLogger(BatchLoader.class.getName());
		   

		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
		context.register(BatchLoaderAppConfig.class, TDLoader.class);
		context.register(BatchLoaderAppConfig.class, TDBehaviorLoader.class);
		
		YamlPropertiesFactoryBean yaml = new YamlPropertiesFactoryBean();
	    yaml.setResources(new ClassPathResource("config/batchloader/batchloader.yaml"));

	    ConfigurableEnvironment environment = context.getEnvironment();
	    environment.getPropertySources()
	        .addFirst(new PropertiesPropertySource("batchloader", yaml.getObject()));

	    context.refresh();
		try {
			
			TDLoader loader = context.getBean(TDLoader.class);
			String out = loader.read(3);
			logger.info(out);
			TDBehaviorLoader bLoader = context.getBean(TDBehaviorLoader.class);
			out = bLoader.read(2238);
			logger.info(out);
			
		}catch (Exception exception) {
			
			logger.log(Level.SEVERE	, "Error loading", exception);;
			
		}finally {
			
			context.close();
		}

	

	}

}
