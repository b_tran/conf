/*
 * Created on Mar 25, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.loader.transformer;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Service;

import com.agencyport.configuration.entity.Client;
import com.agencyport.configuration.entity.Product;
import com.agencyport.configuration.entity.jaxb.transformer.TTransformer;
import com.agencyport.configuration.entity.jaxb.transformer.TTransformers;
import com.agencyport.logging.ExceptionLogger;
import com.agencyport.logging.LoggingManager;

/**
 * The TransformerLoader class
 */
@Service
public class TransformerLoader {
    /**
     * The <code>logger</code> logger for this instance
     */

    final  Logger logger = LoggingManager.getLogger(TransformerLoader.class.getName());
    
	/**
	 * The <code>transaction manager</code>.
	 */
	@Autowired
	JpaTransactionManager transactionManager;

	
	/**
	 * Load.
	 *
	 * @param client the client
	 * @param transformer the transformer
	 * @param input the input
	 * @throws Exception the exception
	 */
	@SuppressWarnings("unchecked")
	public void load(Client client,com.agencyport.configuration.loader.transformer.Transformer transformer, InputStream input) throws Exception {
        logger.info("Loading :"+transformer);
        EntityManagerFactory entityManagerFactory = transactionManager.getEntityManagerFactory();
        EntityManager em = entityManagerFactory.createEntityManager();
        JAXBContext jc1 = JAXBContext.newInstance(com.agencyport.configuration.entity.jaxb.transformer.ObjectFactory.class);
        Unmarshaller u1 = jc1.createUnmarshaller();
        JAXBElement<TTransformers> jaxbTTransformers = (JAXBElement<TTransformers>) u1.unmarshal(input);
        TTransformers eTTransformers = jaxbTTransformers.getValue();
        try{
            for(TTransformer eTTransformer: eTTransformers.getTransformer()) {
                em.getTransaction().begin();
                
                Object c = em.createQuery("select c from client c where c.name = :name")
                .setParameter("name", client.getName())
                .getResultList().stream().findFirst().orElse(null);
                
                if(c != null) {
                    client = (Client)c;
                }
                
                eTTransformer.setClient(client);
                client.getTransformer().add(eTTransformer);
                eTTransformer.setEffectiveDate(transformer.getEffective_date());
                eTTransformer.setCurrentVersion(transformer.isIs_current_version());
    
                eTTransformer.getStep().forEach(step->{
                    step.setTransformer(eTTransformer);
                });
                
                em.persist(eTTransformer);
                em.persist(client);
                em.flush();
                em.getTransaction().commit();
                
            }
        }catch(Exception exception) {
            try {
                if(em.getTransaction().isActive()) {
                    em.getTransaction().rollback();
                }
            }catch (Exception exception2) {
                ExceptionLogger.log(exception2, getClass(), "Transaction#rollback()");
            }
            throw new Exception(transformer.toString(),exception);
        }finally{
            em.close();
        }

}
	
	
	/**
	 * Load.
	 *
	 * @param client the client
	 * @param product the product
	 * @param transformer the transformer
	 * @param input the input
	 * @throws Exception the exception
	 */
	@SuppressWarnings("unchecked")
    public void load( Client client,Product product,com.agencyport.configuration.loader.transformer.Transformer transformer, InputStream input) throws Exception {
        logger.info("Loading :"+transformer);
        EntityManagerFactory entityManagerFactory = transactionManager.getEntityManagerFactory();
        EntityManager em = entityManagerFactory.createEntityManager();
        JAXBContext jc1 = JAXBContext.newInstance(com.agencyport.configuration.entity.jaxb.transformer.ObjectFactory.class);
        Unmarshaller u1 = jc1.createUnmarshaller();
        JAXBElement<TTransformers> jaxbTTransformers = (JAXBElement<TTransformers>) u1.unmarshal(input);
        TTransformers eTTransformers = jaxbTTransformers.getValue();
        for(TTransformer eTTransformer: eTTransformers.getTransformer()) {
            em.getTransaction().begin();
            
            Object c = em.createQuery("select c from client c where c.name = :name")
            .setParameter("name", client.getName())
            .getResultList().stream().findFirst().orElse(null);
            
            if(c != null) {
                client = (Client)c;
            }
            
            eTTransformer.setClient(client);
            eTTransformer.setProduct(product);
            product.getTransformer().add(eTTransformer);
            
            eTTransformer.getStep().forEach(step->{
                step.setTransformer(eTTransformer);
            });
            
            eTTransformer.setEffectiveDate(product.getEffectiveDate());
            eTTransformer.setCurrentVersion(product.isCurrrentVersion());
            em.persist(eTTransformer);
            em.flush();
            em.getTransaction().commit();
        }
        em.close();

}

	
	/**
	 * Read.
	 *
	 * @param transformerId the transformer id
	 * @return the string
	 * @throws Exception the exception
	 */
	public String read(int transformerId) throws Exception {

		JAXBContext jc1 = JAXBContext.newInstance(com.agencyport.configuration.entity.jaxb.transformer.ObjectFactory.class);
		Marshaller m1 = jc1.createMarshaller();

		EntityManagerFactory entityManagerFactory = transactionManager.getEntityManagerFactory();
		EntityManager em = entityManagerFactory.createEntityManager();
		
		try {
			
		    TTransformer eTTransformer = em.find(TTransformer.class, transformerId);
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			m1 = jc1.createMarshaller();
			m1.marshal(new com.agencyport.configuration.entity.jaxb.transformer.ObjectFactory().createTransformer(eTTransformer), baos);
			return new String(baos.toByteArray());

	
	}finally {
		em.close();
	}

}


}
