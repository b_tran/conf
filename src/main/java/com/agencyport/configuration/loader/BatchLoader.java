/*
 * Created on Mar 22, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.loader;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.yaml.snakeyaml.Yaml;

import com.agencyport.configuration.entity.Client;
import com.agencyport.configuration.entity.Product;
import com.agencyport.configuration.loader.acordtoworkitem.AcordToWorkItemLoader;
import com.agencyport.configuration.loader.behavior.TDBehaviorLoader;
import com.agencyport.configuration.loader.client.ClientConfig;
import com.agencyport.configuration.loader.clientproperty.ClientPropertyLoader;
import com.agencyport.configuration.loader.dynamiclist.DynamicTemplateListLoader;
import com.agencyport.configuration.loader.endpoint.EndPointsLoader;
import com.agencyport.configuration.loader.groovyfiles.GroovyLoader;
import com.agencyport.configuration.loader.hosts.HostsLoader;
import com.agencyport.configuration.loader.indexmapping.IndexMappingLoader;
import com.agencyport.configuration.loader.industrycode.IndustryCodeLoader;
import com.agencyport.configuration.loader.markdown.MarkdownLoader;
import com.agencyport.configuration.loader.menu.MenuLoader;
import com.agencyport.configuration.loader.optionlist.OptionListLoader;
import com.agencyport.configuration.loader.pdf.PdfdefinitionsLoader;
import com.agencyport.configuration.loader.transdef.TDLoader;
import com.agencyport.configuration.loader.transformer.TransformerLoader;
import com.agencyport.configuration.loader.view.ViewLoader;
import com.agencyport.configuration.loader.wcclasscode.WorkCompClassCodeLoader;
import com.agencyport.configuration.loader.workflow.WorkFlowLoader;
import com.agencyport.configuration.loader.workitemassistant.WorkItemAssistantLoader;
import com.agencyport.configuration.loader.worklistview.WorkListViewLoader;
import com.agencyport.configuration.loader.xarcrules.XarcRulesLoader;
import com.agencyport.logging.ExceptionLogger;
import com.agencyport.logging.LoggingManager;


/**
 * The BatchLoader class
 */
@Service
public class BatchLoader {
    
    /**
     * The <code>context</code>.
     */
    @Autowired
    ApplicationContext context;
    
    @Autowired
    EntityManagerFactory entityManagerFactory;
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		
	    init(null,args);

	}
	/**
	 * Init 
	 * @param paramContext
	 * @param args
	 */
	public static void init(AnnotationConfigWebApplicationContext paramContext,String[] args) {
	    
	    final  Logger logger = LoggingManager.getLogger(BatchLoader.class.getName());
	    
	    AnnotationConfigWebApplicationContext batchLoaderContext = new AnnotationConfigWebApplicationContext();
        
        YamlPropertiesFactoryBean batchLoaderYaml = new YamlPropertiesFactoryBean();
        Properties prop = new Properties();
        
        if(paramContext==null) {
            batchLoaderYaml.setResources(batchLoaderContext.getResource("classpath:config/batchloader/batchloader.yaml"));
            prop.setProperty("client.config","classpath:config/batchloader/clientconfig.yaml");
        }else {
            Environment env = paramContext.getEnvironment();
            String jndiDatasourceName = env.getProperty("datasource");
            batchLoaderContext.setServletContext(paramContext.getServletContext());
            batchLoaderYaml.setResources(paramContext.getResource(env.getProperty("batchload.config")));
            prop.setProperty("datasource",jndiDatasourceName);
            prop.setProperty("client.config",env.getProperty("client.config"));
            
        }
        batchLoaderContext.register(BatchLoaderAppConfig.class);
        
        ConfigurableEnvironment environment = batchLoaderContext.getEnvironment();
        environment.getPropertySources().addFirst(new PropertiesPropertySource("batchloader", batchLoaderYaml.getObject()));
        environment.getPropertySources().addLast(new PropertiesPropertySource("batchloader.1", prop));

        try {
            Boolean bootupload = (Boolean) environment.getProperty("db.webcontainer_boot_up_load", Boolean.class,false);
            String datasource = environment.getProperty("datasource", String.class,"");
            if(datasource.length() > 0 && !bootupload)
                return;
            
            batchLoaderContext.refresh();
            BatchLoader loader  = batchLoaderContext.getBean(BatchLoader.class);
            loader.deletAll();
            loader.loadAll();
            
        }catch (Exception exception) {
            
            logger.log(Level.SEVERE , "Error loading", exception);;
            
        }finally {
            batchLoaderContext.close();
        }
	}
	/**
     * Load all.
     *
     * @param context the context
     */
    public void loadAll() throws InterruptedException{
        String configFileProp = context.getEnvironment().getProperty("client.config");
        String[] files = StringUtils.tokenizeToStringArray(configFileProp, ";");
        /**
         * Allocate thread.
         */
        List<Thread> threads = new ArrayList<Thread>();
        for(String file: files) {
            Thread loaderThread = new Thread(new Loader(file, context), file);
            threads.add(loaderThread);
        }
        /**
         * Start them
         */
        for(Thread thread: threads) {
            thread.start();
        }
        /** 
         * Join
         */
        for(Thread thread: threads) {
            thread.join();
        }
       
        
    }
    
    /**
     * Worker to load config data
     * The Loader class
     */
    private static class Loader implements Runnable {
        
        /**
         * The <code>file</code> file path for the yaml config file.
         */
        private String file;
        
        /**
         * The <code>context</code> application context.
         */
        private ApplicationContext context;
        /**
         * 
         * Constructs an instance.
         * @param file file path for the yaml config file.
         * @param context {@link ApplicationContext}
         */
        public Loader(String file, ApplicationContext context) {
            this.file = file;
            this.context =context;
        }
        
        @Override
        public void run() {
            loadAll();
            
        }
        
        public String getArtifactLocation (Resource parentResource, String childPartialPath) throws IOException {
            if(parentResource instanceof ClassPathResource) {
                String parentPath = ((ClassPathResource)parentResource).getPath();
                return "classpath:"+parentPath.replace("clientconfig.yaml", childPartialPath);
            }
            return parentResource.getURL().toString().replace("clientconfig.yaml", childPartialPath);
        }
        
        /**
         * Load all.
         */
        private void loadAll(){
            
            final  Logger logger = LoggingManager.getLogger(BatchLoader.Loader.class.getName());
            
            InputStream stream = null;
            try {
                logger.info("Loadding File :"+file);
                Resource parentResource =  context.getResource(file);
                stream =  parentResource.getInputStream();
                ClientConfig config = new Yaml().loadAs( stream, ClientConfig.class );
                config.getClient().forEach(client -> {
                    logger.info("Loading Client: "+client);
                    Client eClient = new Client();
                    eClient.setId(client.getId());
                    eClient.setName(client.getName());
                    eClient.setDescription(client.getDescription());
                    client.getProduct().forEach(product-> {
                        logger.info("Loading Product: "+product);
                        Product eProduct = new Product();
                        eProduct.setCurrrentVersion(product.isIs_current_version());
                        eProduct.setDescription(product.getDescription());
                        eProduct.setTitle(product.getDescription());
                        eProduct.setType(product.getType());
                        eProduct.setVersion(product.getVersion());
                        eProduct.setEffectiveDate((product.getEffective_date()));
                        
                        product.getTransaction().forEach(transaction->{
                            InputStream inputStream = null;
                            try {
                                inputStream = context.getResource(getArtifactLocation(parentResource, transaction.getLocation())).getInputStream();
                                TDLoader loader = context.getBean(TDLoader.class);
                                loader.load( eClient, eProduct, transaction,inputStream);
                            }catch (Exception exception) {
                                ExceptionLogger.log(exception, this.getClass(), "loadAll");
                                throw new RuntimeException(exception);
                            }
                            finally {
                                try {
                                    if(inputStream != null)
                                        inputStream.close();
                                    }catch (Exception exception) {
                                        logger.log(Level.SEVERE    , "Error loading", exception);;
                                    }
                            }
                            
                        });
                        
                        product.getBehavior().forEach(behavior->{
                            InputStream inputStream = null;
                            try {
                                inputStream = context.getResource(getArtifactLocation(parentResource, behavior.getLocation())).getInputStream();
                                TDBehaviorLoader loader = context.getBean(TDBehaviorLoader.class);
                                loader.load( eClient, eProduct, behavior, inputStream);
                            }catch (Exception exception) {
                                ExceptionLogger.log(exception, getClass(), "loadAll");
                                throw new RuntimeException(exception);
                            }
                            finally {
                                try {
                                    if(inputStream != null)
                                        inputStream.close();
                                    }catch (Exception exception) {
                                        logger.log(Level.SEVERE    , "Error loading", exception);;
                                    }
                            }
                            
                        });
                        product.getView().forEach(view->{
                            InputStream inputStream = null;
                            try {
                                inputStream = context.getResource(getArtifactLocation(parentResource, view.getLocation())).getInputStream();
                                ViewLoader loader = context.getBean(ViewLoader.class);
                                loader.load( eClient, eProduct, view, inputStream);
                            }catch (Exception exception) {
                                ExceptionLogger.log(exception, getClass(), "loadAll");
                                throw new RuntimeException(exception);
                            }
                            finally {
                                try {
                                    if(inputStream != null)
                                        inputStream.close();
                                    }catch (Exception exception) {
                                        logger.log(Level.SEVERE    , "Error loading", exception);;
                                    }
                            }
                            
                        });
                        
                        product.getOptionlist().forEach(optionList->{
                            InputStream inputStream = null;
                            try {
                                inputStream = context.getResource(getArtifactLocation(parentResource, optionList.getLocation())).getInputStream();
                                OptionListLoader loader = context.getBean(OptionListLoader.class);
                                loader.load( eClient, eProduct, optionList, inputStream);
                            }catch (Exception exception) {
                                ExceptionLogger.log(exception, getClass(), "loadAll");
                                throw new RuntimeException(exception);
                            }
                            finally {
                                try {
                                    if(inputStream != null)
                                        inputStream.close();
                                    }catch (Exception exception) {
                                        logger.log(Level.SEVERE    , "Error loading", exception);;
                                    }
                            }
                            
                        });
                        product.getXarcrules().forEach(xarcRules->{
                            InputStream inputStream = null;
                            try {
                                inputStream = context.getResource(getArtifactLocation(parentResource, xarcRules.getLocation())).getInputStream();
                                XarcRulesLoader loader = context.getBean(XarcRulesLoader.class);
                                loader.load( eClient, eProduct, xarcRules, inputStream);
                            }catch (Exception exception) {
                                ExceptionLogger.log(exception, getClass(), "loadAll");
                                throw new RuntimeException(exception);
                            }
                            finally {
                                try {
                                    if(inputStream != null)
                                        inputStream.close();
                                    }catch (Exception exception) {
                                        logger.log(Level.SEVERE    , "Error loading", exception);;
                                    }
                            }
                            
                        });
                        
                        product.getAcordtoworkitemmap().forEach(acordtoworkitemmap->{
                            InputStream inputStream = null;
                            try {
                                inputStream = context.getResource(getArtifactLocation(parentResource, acordtoworkitemmap.getLocation())).getInputStream();
                                AcordToWorkItemLoader loader = context.getBean(AcordToWorkItemLoader.class);
                                loader.load( eClient, eProduct, acordtoworkitemmap, inputStream);
                            }catch (Exception exception) {
                                ExceptionLogger.log(exception, getClass(), "loadAll");
                                throw new RuntimeException(exception);
                            }
                            finally {
                                try {
                                    if(inputStream != null)
                                        inputStream.close();
                                    }catch (Exception exception) {
                                        logger.log(Level.SEVERE    , "Error loading", exception);;
                                    }
                            }
                            
                        });
                        
                        product.getTransformer().forEach(transformer->{
                            InputStream inputStream = null;
                            try {
                                inputStream = context.getResource(getArtifactLocation(parentResource, transformer.getLocation())).getInputStream();
                                TransformerLoader loader = context.getBean(TransformerLoader.class);
                                loader.load( eClient, eProduct, transformer, inputStream);
                            }catch (Exception exception) {
                                ExceptionLogger.log(exception, getClass(), "loadAll");
                                throw new RuntimeException(exception);
                            }
                            finally {
                                try {
                                    if(inputStream != null)
                                        inputStream.close();
                                    }catch (Exception exception) {
                                        logger.log(Level.SEVERE    , "Error loading", exception);;
                                    }
                            }
                            
                        });
                        
                        product.getPdfdefinitions().forEach(pdfdefinitions->{
                            InputStream inputStream = null;
                            try {
                                inputStream = context.getResource(getArtifactLocation(parentResource, pdfdefinitions.getLocation())).getInputStream();
                                PdfdefinitionsLoader loader = context.getBean(PdfdefinitionsLoader.class);
                                loader.load( eClient, eProduct, pdfdefinitions, inputStream);
                            }catch (Exception exception) {
                                ExceptionLogger.log(exception, getClass(), "loadAll");
                                throw new RuntimeException(exception);
                            }
                            finally {
                                try {
                                    if(inputStream != null)
                                        inputStream.close();
                                    }catch (Exception exception) {
                                        logger.log(Level.SEVERE    , "Error loading", exception);;
                                    }
                            }
                            
                        });
                        
                        product.getGroovy().forEach(groovy->{
                            InputStream inputStream = null;
                            try {
                                inputStream = context.getResource(getArtifactLocation(parentResource, groovy.getLocation())).getInputStream();
                                GroovyLoader loader = context.getBean(GroovyLoader.class);
                                loader.load(eProduct, eClient, groovy, inputStream);
                            }catch (Exception exception) {
                                ExceptionLogger.log(exception, getClass(), "loadAll");
                                throw new RuntimeException(exception);
                            }
                            finally {
                                try {
                                    if(inputStream != null)
                                        inputStream.close();
                                    }catch (Exception exception) {
                                        logger.log(Level.SEVERE    , "Error loading groovy file for product during batchloading process", exception);;
                                    }
                            }
                            
                        });
                        product.getMarkdown().forEach(markdown->{
                            InputStream inputStream = null;
                            try {
                                inputStream = context.getResource(getArtifactLocation(parentResource, markdown.getLocation())).getInputStream();
                                MarkdownLoader loader = context.getBean(MarkdownLoader.class);
                                loader.load(eProduct, eClient, markdown, inputStream);
                            }catch (Exception exception) {
                                ExceptionLogger.log(exception, getClass(), "loadAll markdown files.");
                                throw new RuntimeException(exception);
                            }
                            finally {
                                try {
                                    if(inputStream != null) 
                                    	inputStream.close();
                                } catch(Exception exception) {
                                        logger.log(Level.SEVERE, "Error loading markdown file for product during batchloading process", exception);;
                            	}
                            }
                        });
                        
                    });
                    
                    client.getSearchindex().forEach(indexMapping->{
        
                        InputStream inputStream = null;
                        try {
                            inputStream = context.getResource(getArtifactLocation(parentResource, indexMapping.getLocation())).getInputStream();
                            IndexMappingLoader loader = context.getBean(IndexMappingLoader.class);
                            loader.load( eClient, indexMapping, inputStream);
                        }catch (Exception exception) {
                            ExceptionLogger.log(exception, getClass(), "loadAll");
                            throw new RuntimeException(exception);
                        }
                        finally {
                            try {
                                if(inputStream != null)
                                    inputStream.close();
                                }catch (Exception exception) {
                                    logger.log(Level.SEVERE    , "Error loading", exception);;
                                }
                        }
                        
                    
                        
                    });
                    
                    client.getWorklistview().forEach(workListView->{
                        
        
        
                        InputStream inputStream = null;
                        try {
                            inputStream = context.getResource(getArtifactLocation(parentResource, workListView.getLocation())).getInputStream();
                            WorkListViewLoader loader = context.getBean(WorkListViewLoader.class);
                            loader.load( eClient, workListView, inputStream);
                        }catch (Exception exception) {
                            ExceptionLogger.log(exception, getClass(), "loadAll");
                            throw new RuntimeException(exception);
                        }
                        finally {
                            try {
                                if(inputStream != null)
                                    inputStream.close();
                                }catch (Exception exception) {
                                    logger.log(Level.SEVERE    , "Error loading", exception);;
                                }
                        }
                        
                    
                        
                    
                        
                    });
                    
                    client.getWorkflow().forEach(workflow->{
                        
        
        
                        InputStream inputStream = null;
                        try {
                            inputStream = context.getResource(getArtifactLocation(parentResource, workflow.getLocation())).getInputStream();
                            WorkFlowLoader loader = context.getBean(WorkFlowLoader.class);
                            loader.load( eClient, workflow, inputStream);
                        }catch (Exception exception) {
                            ExceptionLogger.log(exception, getClass(), "loadAll");
                            throw new RuntimeException(exception);
                        }
                        finally {
                            try {
                                if(inputStream != null)
                                    inputStream.close();
                                }catch (Exception exception) {
                                    logger.log(Level.SEVERE    , "Error loading", exception);;
                                }
                        }
                        
                    
                        
                    
                        
                    });
                    
                    client.getOptionlist().forEach(optionList->{
        
                        InputStream inputStream = null;
                        try {
                            inputStream = context.getResource(getArtifactLocation(parentResource, optionList.getLocation())).getInputStream();
                            OptionListLoader loader = context.getBean(OptionListLoader.class);
                            loader.load( eClient, optionList, inputStream);
                        }catch (Exception exception) {
                            ExceptionLogger.log(exception, getClass(), "loadAll");
                            throw new RuntimeException(exception);
                        }
                        finally {
                            try {
                                if(inputStream != null)
                                    inputStream.close();
                                }catch (Exception exception) {
                                    logger.log(Level.SEVERE    , "Error loading", exception);;
                                }
                        }
                        
                    });
                    
                    client.getDynamiclisttemplate().forEach(dynamicList->{
        
                        InputStream inputStream = null;
                        try {
                            inputStream = context.getResource(getArtifactLocation(parentResource, dynamicList.getLocation())).getInputStream();
                            DynamicTemplateListLoader loader = context.getBean(DynamicTemplateListLoader.class);
                            loader.load( eClient, dynamicList, inputStream);
                        }catch (Exception exception) {
                            ExceptionLogger.log(exception, getClass(), "loadAll");
                            throw new RuntimeException(exception);
                        }
                        finally {
                            try {
                                if(inputStream != null)
                                    inputStream.close();
                                }catch (Exception exception) {
                                    logger.log(Level.SEVERE    , "Error loading", exception);;
                                }
                        }
                        
                    });
                    
                    client.getView().forEach(view->{
        
                        InputStream inputStream = null;
                        try {
                            inputStream = context.getResource(getArtifactLocation(parentResource, view.getLocation())).getInputStream();
                            ViewLoader loader = context.getBean(ViewLoader.class);
                            loader.load( eClient, view, inputStream);
                        }catch (Exception exception) {
                            ExceptionLogger.log(exception, getClass(), "loadAll");
                            throw new RuntimeException(exception);
                        }
                        finally {
                            try {
                                if(inputStream != null)
                                    inputStream.close();
                                }catch (Exception exception) {
                                    logger.log(Level.SEVERE    , "Error loading", exception);;
                                }
                        }
                        
                    });
                    
                    client.getWorkcompclasscodelist().forEach(ccList->{
        
                        InputStream inputStream = null;
                        try {
                            inputStream = context.getResource(getArtifactLocation(parentResource, ccList.getLocation())).getInputStream();
                            WorkCompClassCodeLoader loader = context.getBean(WorkCompClassCodeLoader.class);
                            loader.load( eClient, ccList, inputStream);
                        }catch (Exception exception) {
                            ExceptionLogger.log(exception, getClass(), "loadAll");
                            throw new RuntimeException(exception);
                        }
                        finally {
                            try {
                                if(inputStream != null)
                                    inputStream.close();
                                }catch (Exception exception) {
                                    logger.log(Level.SEVERE    , "Error loading", exception);;
                                }
                        }
                        
                    });
                    
                    client.getIndustrycodelist().forEach(industryList->{
        
                        InputStream inputStream = null;
                        try {
                            IndustryCodeLoader loader = context.getBean(IndustryCodeLoader.class);
                            loader.load( eClient, industryList, inputStream);
                        }catch (Exception exception) {
                            ExceptionLogger.log(exception, getClass(), "loadAll");
                            throw new RuntimeException(exception);
                        }
                        finally {
                            try {
                                if(inputStream != null)
                                    inputStream.close();
                                }catch (Exception exception) {
                                    logger.log(Level.SEVERE    , "Error loading", exception);;
                                }
                        }
                        
                    });
                    
                    client.getMenu().forEach(menu->{
        
                        InputStream inputStream = null;
                        try {
                            inputStream = context.getResource(getArtifactLocation(parentResource, menu.getLocation())).getInputStream();
                            MenuLoader loader = context.getBean(MenuLoader.class);
                            loader.load( eClient, menu, inputStream);
                        }catch (Exception exception) {
                            ExceptionLogger.log(exception, getClass(), "loadAll");
                            throw new RuntimeException(exception);
                        }
                        finally {
                            try {
                                if(inputStream != null)
                                    inputStream.close();
                                }catch (Exception exception) {
                                    logger.log(Level.SEVERE    , "Error loading", exception);;
                                }
                        }
                        
                    });
                    
                    client.getTransformer().forEach(transfomer->{
        
                        InputStream inputStream = null;
                        try {
                            inputStream = context.getResource(getArtifactLocation(parentResource, transfomer.getLocation())).getInputStream();
                            TransformerLoader loader = context.getBean(TransformerLoader.class);
                            loader.load( eClient, transfomer, inputStream);
                        }catch (Exception exception) {
                            ExceptionLogger.log(exception, getClass(), "loadAll");
                            throw new RuntimeException(exception);
                        }
                        finally {
                            try {
                                if(inputStream != null)
                                    inputStream.close();
                                }catch (Exception exception) {
                                    logger.log(Level.SEVERE    , "Error loading", exception);;
                                }
                        }
                        
                    });
                    
                    client.getWorkitemassistant().forEach(workItemAssitant->{
        
                        InputStream inputStream = null;
                        try {
                            inputStream = context.getResource(getArtifactLocation(parentResource, workItemAssitant.getLocation())).getInputStream();
                            WorkItemAssistantLoader loader = context.getBean(WorkItemAssistantLoader.class);
                            loader.load( eClient, workItemAssitant, inputStream);
                        }catch (Exception exception) {
                            ExceptionLogger.log(exception, getClass(), "loadAll");
                            throw new RuntimeException(exception);
                        }
                        finally {
                            try {
                                if(inputStream != null)
                                    inputStream.close();
                                }catch (Exception exception) {
                                    logger.log(Level.SEVERE    , "Error loading", exception);;
                                }
                        }
                        
                    });
                    
                    ArrayList<String> groovyfileNames = new ArrayList<String>();
                    client.getGroovy().forEach(groovyFile->{
                        
                        InputStream inputStream = null;
                        try {
                            inputStream = context.getResource(getArtifactLocation(parentResource, groovyFile.getLocation())).getInputStream();
                            GroovyLoader loader = context.getBean(GroovyLoader.class);
                            
                            //check that all groovy files have a unique name
                            if(groovyfileNames.contains(groovyFile.getName()+":"+eClient.getId())){
                            	throw new Exception("Groovy filenames must be unique.");
                            }
                            groovyfileNames.add(groovyFile.getName()+":"+eClient.getId());
                            
                            loader.load(null, eClient, groovyFile, inputStream);
                        }catch (Exception exception) {
                            ExceptionLogger.log(exception, getClass(), "loadAll Groovy files.");
                            throw new RuntimeException(exception);
                        }
                        finally {
                            try {
                                if(inputStream != null) 
                                	inputStream.close();
                            } catch(Exception exception) {
                                    logger.log(Level.SEVERE, "Error loading", exception);;
                        	}
                        }
                        
                    });
                    
                    client.getMarkdown().forEach(markdown->{
                        InputStream inputStream = null;
                        try {
                            inputStream = context.getResource(getArtifactLocation(parentResource, markdown.getLocation())).getInputStream();
                            MarkdownLoader loader = context.getBean(MarkdownLoader.class);
                            loader.load(null, eClient, markdown, inputStream);
                        }catch (Exception exception) {
                            ExceptionLogger.log(exception, getClass(), "loadAll markdown files.");
                            throw new RuntimeException(exception);
                        }
                        finally {
                            try {
                                if(inputStream != null) 
                                	inputStream.close();
                            } catch(Exception exception) {
                                    logger.log(Level.SEVERE, "Error loading", exception);;
                        	}
                        }
                    });
                    
                    client.getEndpoint().forEach(endPoint->{
                        InputStream inputStream = null;
                        try {
                            inputStream = context.getResource(getArtifactLocation(parentResource, endPoint.getLocation())).getInputStream();
                            EndPointsLoader loader = context.getBean(EndPointsLoader.class);
                            loader.load(eClient, endPoint, inputStream);
                        }catch (Exception exception) {
                            ExceptionLogger.log(exception, getClass(), "loadAll");
                            throw new RuntimeException(exception);
                        }
                        finally {
                            try {
                                if(inputStream != null)
                                    inputStream.close();
                                }catch (Exception exception) {
                                    logger.log(Level.SEVERE    , "Error loading endpoints file for client during batchloading process", exception);
                                }
                        }
                    });
                    
                    client.getClientProperty().forEach(endPoint->{
                        InputStream inputStream = null;
                        try {
                            inputStream = context.getResource(getArtifactLocation(parentResource, endPoint.getLocation())).getInputStream();
                            ClientPropertyLoader loader = context.getBean(ClientPropertyLoader.class);
                            loader.load(eClient, endPoint, inputStream);
                        }catch (Exception exception) {
                            ExceptionLogger.log(exception, getClass(), "loadAll");
                            throw new RuntimeException(exception);
                        }
                        finally {
                            try {
                                if(inputStream != null)
                                    inputStream.close();
                            }catch (Exception exception) {
                                logger.log(Level.SEVERE , "Error loading client property file for client during batchloading process", exception);
                            }
                    }
                    });
                    
                    client.getClientHosts().forEach(endPoint->{
                        InputStream inputStream = null;
                        try {
                            inputStream = context.getResource(getArtifactLocation(parentResource, endPoint.getLocation())).getInputStream();
                            HostsLoader loader = context.getBean(HostsLoader.class);
                            loader.load(eClient, endPoint, inputStream);
                        }catch (Exception exception) {
                            ExceptionLogger.log(exception, getClass(), "loadAll");
                            throw new RuntimeException(exception);
                        }
                        finally {
                            try {
                                if(inputStream != null)
                                    inputStream.close();
                            }catch (Exception exception) {
                                logger.log(Level.SEVERE , "Error loading client host file for client during batchloading process", exception);
                            }
                    }
                    });
                    
                });
            
            }catch(Exception exception) {
                logger.log(Level.SEVERE    , "Error loading", exception);;
                try {
                    
                    if(stream != null)
                        stream.close();
                    
                    }catch (Exception e) {
                        logger.log(Level.SEVERE    , "Error loading", exception);;
                    }
            }
            
        }
        
    }
	
	
    public void deletAll(){
            
            final  Logger logger = LoggingManager.getLogger(BatchLoader.class.getName());
            EntityManager em=  entityManagerFactory.createEntityManager();
            InputStream stream = null;
            try {     
                String configFileProp = context.getEnvironment().getProperty("client.config");
                String[] files = StringUtils.tokenizeToStringArray(configFileProp, ";");
                for(String file: files) {
                    Resource res =  context.getResource(file);
                    stream =  res.getInputStream();
                    ClientConfig config = new Yaml().loadAs( stream, ClientConfig.class );
                    config.getClient().forEach(client->{
                        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
                        CriteriaQuery<Client> criteriaQuery = criteriaBuilder.createQuery(Client.class);
                        Root<Client> pRoot = criteriaQuery.from(Client.class);
                        List<Predicate> predicates = new ArrayList<Predicate>();
                        predicates.add(criteriaBuilder.equal(pRoot.get("name"), client.getName() ));
                        criteriaQuery.select(pRoot)
                        .where(predicates.toArray(new Predicate[] {}));
                        Client eClient = em.createQuery(criteriaQuery).getResultList().stream().findFirst().orElse(null);
                        
                        if(eClient !=  null) {
                            try {
                                em.getTransaction().begin();
                                em.remove(eClient);
                                em.getTransaction().commit();
                            }catch(Exception exception){
                                logger.log(Level.SEVERE    , "Error deleting", exception);;
                            }
                        }
                        
                        
                    });
                }
                
            }catch(Exception exception){
                logger.log(Level.SEVERE    , "Error deleting", exception);;
            }finally {
                
                em.close();
            }
    }
	

}
