/*
 * Created on Apr 19, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.loader.wcclasscode;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;

import com.agencyport.configuration.entity.classcode.WorkCompClassCode;
import com.agencyport.configuration.poi.ClassCode;
import com.agencyport.configuration.poi.ClassCodes;
import com.agencyport.logging.LoggingManager;

/**
 * The WorkCompClassCodeOptionList class
 */
public class WorkCompClassCodeList {
    final  Logger logger = LoggingManager.getLogger(WorkCompClassCodeList.class.getName());
    
    @Autowired
    Environment env;
    
    @Autowired
    ApplicationContext context;
    
    /**
     * The <code>location</code>.
     */
    private String location;

    /**
     * Gets the location.
     *
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * Sets the location.
     *
     * @param location the new location
     */
    public void setLocation(String location) {
        this.location = location;
    }
    
    /** 
     * {@inheritDoc}
     */ 
    
    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" location: "+location);
        return buffer.toString();
    }
    
    /**
     * Gets the.
     *
     * @return the list
     */
    public Map<String, List<WorkCompClassCode>>  get(InputStream inputStream) throws Exception{
        
        Map<String, List<WorkCompClassCode>> results = new HashMap<String, List<WorkCompClassCode>>();

             try {
                 JAXBContext jc1 = JAXBContext.newInstance(ClassCodes.class);
                 Unmarshaller u1 = jc1.createUnmarshaller();
                 ClassCodes classCodes = (ClassCodes)u1.unmarshal(inputStream);
                 for(ClassCode classCode : classCodes.getClassCodes()){
                     
                     List<WorkCompClassCode> workCompClassCodes = results.get(classCode.getState());
                     if(workCompClassCodes==null) {
                         workCompClassCodes = new ArrayList<WorkCompClassCode>();
                         results.put(classCode.getState(), workCompClassCodes);
                     }
                     
                     WorkCompClassCode workCompClassCode = new WorkCompClassCode();
                     workCompClassCode.setAttrValue(classCode.getCode());
                     workCompClassCode.setValue(classCode.getDescription());
                     workCompClassCode.setStateProvCd(classCode.getState());
                     workCompClassCode.setClassCodeType(classCode.getClassCodeType());
                     workCompClassCodes.add(workCompClassCode);
                 }
             }catch (JAXBException exception) {
                 logger.info(exception.getMessage());
             }



        return results;
        
    }

}
