/*
 * Created on Mar 25, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.loader.wcclasscode;

import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.core.env.Environment;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Service;

import com.agencyport.configuration.entity.Client;
import com.agencyport.configuration.entity.classcode.WorkCompClassCode;
import com.agencyport.logging.ExceptionLogger;
import com.agencyport.logging.LoggingManager;

/**
 * The WorkCompClassCodeLoader class
 */
@Service
public class WorkCompClassCodeLoader {
    
    /**
     * The <code>logger</code> logger for this instance
     */
    final  Logger logger = LoggingManager.getLogger(WorkCompClassCodeLoader.class.getName());
    
	
    @Autowired
    Environment env;
    
	/**
	 * The <code>transaction manager</code>.
	 */
	@Autowired
	JpaTransactionManager transactionManager;
	
	@Autowired
    AutowireCapableBeanFactory beanFactory;

	/**
	 * Load.
	 *
	 * @param client the client
	 * @param wcClassCodeList the option list
	 * @param input the input
	 * @throws Exception the exception
	 */
	@SuppressWarnings("unchecked")
	public void load(Client client,com.agencyport.configuration.loader.wcclasscode.WorkCompClassCodeList wcClassCodeList, InputStream input) throws Exception {
			logger.info("Loading :"+wcClassCodeList);
			EntityManagerFactory entityManagerFactory = transactionManager.getEntityManagerFactory();
			EntityManager em = entityManagerFactory.createEntityManager();
			try {
			    Client c = em.createQuery("select c from client c where c.name = :name",Client.class)
                        .setParameter("name", client.getName())
                        .getResultList().stream().findFirst().orElse(null);
			    
			    if(c == null) {
                    return;
                }
			    
			    beanFactory.autowireBean(wcClassCodeList);
    			Map<String, List<WorkCompClassCode>> l = wcClassCodeList.get(input);
    			for(Map.Entry<String, List<WorkCompClassCode>> entry:  l.entrySet()) {
    			    int max = env.getProperty("wc.classcode.load_max_per_state", Integer.class, Integer.MAX_VALUE);
    			    int count =0;
    			    classCodeLoad:
    			    for(WorkCompClassCode classCode: entry.getValue()) {
        			    try {
        			        if(++count > max)
        			            break classCodeLoad;
    
        			        boolean empty = em.createQuery("select c from ref_data_work_comp_class_code c where state_prov_cd=:state_prov_cd and client_id=:client_id and type_code=:type_code and type_description=:type_description",WorkCompClassCode.class)
                                    .setParameter("state_prov_cd", classCode.getStateProvCd())
                                    .setParameter("client_id", client.getId())
                                    .setParameter("type_code", classCode.getAttrValue())
                                    .setParameter("type_description", classCode.getValue())
                                    .getResultList().isEmpty();
        			        
        			        if(!empty ){
        			            continue;
        			        }
        			        
        			        em.getTransaction().begin();
        			            WorkCompClassCode code = new WorkCompClassCode();
                    			code.setClient(client);
                    			code.setAttrValue(classCode.getAttrValue());
                    			code.setStateProvCd(classCode.getStateProvCd());
                    			code.setClassCodeType(classCode.getClassCodeType());
                    			code.setValue(classCode.getValue());
                    			em.persist(code);
                			em.flush();
                			em.clear();
                            em.getTransaction().commit();
                			
        			    } catch (Exception exception) {
        			        ExceptionLogger.log(exception, getClass(), wcClassCodeList.toString());
        			        try {
        	                    if(em.getTransaction().isActive()) {
        	                        em.getTransaction().rollback();
        	                    }
        	                }catch (Exception exception2) {
        	                    ExceptionLogger.log(exception2, getClass(), "Transaction#rollback()");
        	                }
                        }
    			    }    
    			}
			}finally {
			    em.close();
			}

	}
	

}
