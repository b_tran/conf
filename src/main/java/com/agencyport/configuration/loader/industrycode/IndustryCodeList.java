/*
 * Created on Apr 19, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.loader.industrycode;

import java.util.ArrayList;
import java.util.List;

import com.agencyport.configuration.entity.industrycode.IndustryCode;

/**
 * The IndustryCodeOptionList class
 */
public class IndustryCodeList {
    
    /**
     * The <code>location</code>.
     */
    private String location;

    /**
     * Gets the location.
     *
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * Sets the location.
     *
     * @param location the new location
     */
    public void setLocation(String location) {
        this.location = location;
    }
    
    /** 
     * {@inheritDoc}
     */ 
    
    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" location: "+location);
        return buffer.toString();
    }
    
    /**
     * Gets the.
     *
     * @return the list
     */
    public static List<IndustryCode> get(){
        List<IndustryCode> list = new ArrayList<IndustryCode>();
        IndustryCode c = new IndustryCode();
        c.setValue("Wood Containers");
        c.setIndustryCdType("SIC");
        c.setAttrValue("2449");
        list.add(c);

        c = new IndustryCode();
        c.setValue("Mobile Homes");
        c.setIndustryCdType("SIC");
        c.setAttrValue("2451");
        list.add(c);
        
        c = new IndustryCode();
        c.setValue("Prefabricated Wood Buildings and Components");
        c.setIndustryCdType("SIC");
        c.setAttrValue("2452");
        list.add(c);
        
        c = new IndustryCode();
        c.setValue("Home Health Care Services");
        c.setIndustryCdType("NAICS");
        c.setAttrValue("621610");
        list.add(c);
        
        c = new IndustryCode();
        c.setValue("Ambulance Services");
        c.setIndustryCdType("NAICS");
        c.setAttrValue("621910");
        list.add(c);
        
        c = new IndustryCode();
        c.setValue("Blood and Organ Banks");
        c.setIndustryCdType("NAICS");
        c.setAttrValue("621991");
        list.add(c);
        
        return list;
        
    }

}
