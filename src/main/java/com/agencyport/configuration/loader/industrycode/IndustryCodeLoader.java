/*
 * Created on Mar 25, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.loader.industrycode;

import java.io.InputStream;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Service;

import com.agencyport.configuration.entity.Client;
import com.agencyport.configuration.entity.industrycode.IndustryCode;
import com.agencyport.logging.ExceptionLogger;
import com.agencyport.logging.LoggingManager;

/**
 * The IndustryCodeLoader class
 */
@Service
public class IndustryCodeLoader {
    /**
     * The <code>logger</code> logger for this instance
     */
	
    final  Logger logger = LoggingManager.getLogger(IndustryCodeLoader.class.getName());


	/**
	 * The <code>transaction manager</code>.
	 */
	@Autowired
	JpaTransactionManager transactionManager;

	/**
	 * Load.
	 *
	 * @param client the client
	 * @param optionList the option list
	 * @param input the input
	 * @throws Exception the exception
	 */
	@SuppressWarnings("unchecked")
	public void load(Client client,com.agencyport.configuration.loader.industrycode.IndustryCodeList optionList, InputStream input) throws Exception {
			logger.info("Loading :"+optionList);
			EntityManagerFactory entityManagerFactory = transactionManager.getEntityManagerFactory();
			EntityManager em = entityManagerFactory.createEntityManager();
			try {
    			for(IndustryCode industryCode:  IndustryCodeList.get()) {
    			    try {
            			em.getTransaction().begin();
            			
            			Object c = em.createQuery("select c from client c where c.name = :name")
            			.setParameter("name", client.getName())
            			.getResultList().stream().findFirst().orElse(null);
            			
            			if(c != null) {
            				client = (Client)c;
            			}
            			IndustryCode code = new IndustryCode();
            			code.setClient(client);
            			code.setAttrValue(industryCode.getAttrValue());
            			code.setIndustryCdType(industryCode.getIndustryCdType());
            			code.setValue(industryCode.getValue());
            			
            			em.persist(code);
            			em.flush();
            			em.getTransaction().commit();
    			    } catch (Exception exception) {
    			        try {
    	                    if(em.getTransaction().isActive()) {
    	                        em.getTransaction().rollback();
    	                    }
    	                }catch (Exception exception2) {
    	                    ExceptionLogger.log(exception2, getClass(), "Transaction#rollback()");
    	                }
    			        throw new Exception(optionList.toString(),exception);
                    }
    			}
			}finally {
			    em.close();
			}
			

	}
	

}
