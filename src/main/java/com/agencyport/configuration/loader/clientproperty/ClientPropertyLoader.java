package com.agencyport.configuration.loader.clientproperty;

import java.io.InputStream;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Service;

import com.agencyport.configuration.entity.Client;
import com.agencyport.configuration.entity.jaxb.clientproperty.ClientProperties;
import com.agencyport.logging.ExceptionLogger;
import com.agencyport.logging.LoggingManager;

@Service
public class ClientPropertyLoader {
	
	/**
	 * The <code>logger</code> logger for this instance
	 */

	final  Logger logger = LoggingManager.getLogger(ClientPropertyLoader.class.getName());


	/**
	 * The <code>transaction manager</code>.
	 */
	@Autowired
	JpaTransactionManager transactionManager;

	/**
	 * Load.
	 * @param input the input
	 * @throws Exception the exception
	 */
	public  void load(Client client, ClientProperty inputEndPoint, InputStream input) throws Exception {
		EntityManagerFactory entityManagerFactory = transactionManager.getEntityManagerFactory();
		EntityManager em = entityManagerFactory.createEntityManager();
		
		try{
			JAXBContext jc1 = JAXBContext.newInstance(ClientProperties.class);
			Unmarshaller u1 = jc1.createUnmarshaller();
			ClientProperties clientProperties = (ClientProperties) u1.unmarshal(input);
			
			em.getTransaction().begin();
			
			clientProperties.getProperties().forEach(clientProperty -> {
				
				String env = clientProperty.getEnvironment();
				
				clientProperty.getProperty().forEach(property ->{
					property.setEnv(env);
					property.setClient(client);
					em.persist(property);
					em.flush();
				});
			});
			
			em.getTransaction().commit();
		}catch (Exception exception){
			try {
				if(em.getTransaction().isActive()) {
					em.getTransaction().rollback();
				}
			}catch (Exception exception2) {
				ExceptionLogger.log(exception2, getClass(), "Transaction#rollback()");
			}
		}finally {
			em.close();
		}	
	}
}
