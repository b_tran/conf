/*
 * Created on Aug 24, 2016 by sodonnell AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.loader.markdown;

import java.util.Date;

/**
 * The Menu class
 */
public class Markdown {
    
    /**
     * The <code>effective_date</code>.
     */
    private Date effective_date;
    
    /**
     * The <code>is_current_version</code>.
     */
    private boolean is_current_version;
        
    
    /**
     * The <code>location</code>.
     */
    private String location;

    /**
     * Gets the location.
     *
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * Sets the location.
     *
     * @param location the new location
     */
    public void setLocation(String location) {
        this.location = location;
    }
    
    /**
     * Gets the effective_date.
     *
     * @return the effective_date
     */
    public Date getEffective_date() {
        return effective_date;
    }

    /**
     * Sets the effective_date.
     *
     * @param effective_date the new effective_date
     */
    public void setEffective_date(Date effective_date) {
        this.effective_date = effective_date;
    }

    /**
     * Sets the current version.
     *
     * @param is_current_version the new is_current_version value
     */
    public void setIs_current_version(boolean is_current_version) {
        this.is_current_version = is_current_version;
    }
    
    /**
     * Checks if is is_current_version.
     *
     * @return true, if is is_current_version
     */
    public boolean isIs_current_version() {
        return is_current_version;
    }
    
    /**
     * Get the filename of the markdown file
     * 
     * @return markdown filename
     */
    public String getName(){
    	String[] delimitered = location.split("/");
    	String[] fileNameSplit = delimitered[delimitered.length-1].split("\\.");
    	return fileNameSplit[0];
    }

    /** 
     * {@inheritDoc}
     */ 
    
    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("effective_date: "+effective_date);
        buffer.append(" is_current_version: "+is_current_version);
        buffer.append(" location: "+location);
        return buffer.toString();
    }
}
