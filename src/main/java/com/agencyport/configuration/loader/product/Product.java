/*
 * Created on Mar 25, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.loader.product;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.agencyport.configuration.loader.acordtoworkitem.Acordtoworkitemmap;
import com.agencyport.configuration.loader.behavior.Behavior;
import com.agencyport.configuration.loader.groovyfiles.Groovy;
import com.agencyport.configuration.loader.markdown.Markdown;
import com.agencyport.configuration.loader.optionlist.OptionList;
import com.agencyport.configuration.loader.pdf.Pdfdefinitions;
import com.agencyport.configuration.loader.transdef.Transaction;
import com.agencyport.configuration.loader.transformer.Transformer;
import com.agencyport.configuration.loader.view.View;
import com.agencyport.configuration.loader.xarcrules.Xarcrules;

/**
 * The Product class
 */
public class Product {
	
	/**
	 * The <code>type</code>.
	 */
	private String type;
	
	/**
	 * The <code>version</code>.
	 */
	private String version;
	
	/**
	 * The <code>effective_date</code>.
	 */
	private Date effective_date;
	
	/**
	 * The <code>description</code>.
	 */
	private String description;
	
	/**
	 * The <code>title</code>.
	 */
	private String title;
	
	/**
	 * The <code>is_current_version</code>.
	 */
	private boolean is_current_version;
	
	/**
	 * The <code>transaction</code>.
	 */
	private List<Transaction> transaction;

	/**
	 * The <code>behavior</code>.
	 */
	private List<Behavior> behavior;
	
	/**
	 * The <code>optionlist</code>.
	 */
	private List<OptionList> optionlist;
	
	/**
	 * The <code>xarcrules</code>.
	 */
	private List<Xarcrules> xarcrules;
	
	/**
	 * The <code>view</code>.
	 */
	private List<View> view;
	
	/**
     * The <code>acordtoworkitemmap</code>.
     */
    private List<Acordtoworkitemmap> acordtoworkitemmap;
    
    /**
     * The <code>transformer</code>.
     */
    private List<Transformer> transformer;
    
    /**
     * The <code>pdfdefintions</code>.
     */
    private List<Pdfdefinitions> pdfdefinitions;
    
    /**
     * The <code>groovy</code>.
     */
    private List<Groovy> groovy;
    
    /**
     * The <code>markdown</code>.
     */
    private List<Markdown> markdown;
    
	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Gets the version.
	 *
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * Sets the version.
	 *
	 * @param version the new version
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * Gets the effective_date.
	 *
	 * @return the effective_date
	 */
	public Date getEffective_date() {
		return effective_date;
	}

	/**
	 * Sets the effective_date.
	 *
	 * @param effective_date the new effective_date
	 */
	public void setEffective_date(Date effective_date) {
		this.effective_date = effective_date;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Checks if is is_current_version.
	 *
	 * @return true, if is is_current_version
	 */
	public boolean isIs_current_version() {
		return is_current_version;
	}

	/**
	 * Sets the is_current_version.
	 *
	 * @param is_current_version the new is_current_version
	 */
	public void setIs_current_version(boolean is_current_version) {
		this.is_current_version = is_current_version;
	}

	/**
	 * Gets the transaction.
	 *
	 * @return the transaction
	 */
	public List<Transaction> getTransaction() {
		if(transaction==null) {
			transaction = new ArrayList<Transaction>();
		}
		return transaction;
	}

	/**
	 * Sets the transaction.
	 *
	 * @param transaction the new transaction
	 */
	public void setTransaction(List<Transaction> transaction) {
		this.transaction = transaction;
	}

	/**
	 * Gets the behavior.
	 *
	 * @return the behavior
	 */
	public List<Behavior> getBehavior() {
		if(behavior==null) {
			behavior = new ArrayList<Behavior>();
		}
		return behavior;
	}

	/**
	 * Sets the behavior.
	 *
	 * @param behavior the new behavior
	 */
	public void setBehavior(List<Behavior> behavior) {
		this.behavior = behavior;
	}
	
	/**
	 * Gets the optionlist.
	 *
	 * @return the optionlist
	 */
	public List<OptionList> getOptionlist() {
        if(optionlist==null) {
            optionlist = new ArrayList<OptionList>();
        }
        return optionlist;
    }

    /**
     * Sets the optionlist.
     *
     * @param optionlist the new optionlist
     */
    public void setOptionlist(List<OptionList> optionlist) {
        this.optionlist = optionlist;
    }

	/** 
	 * {@inheritDoc}
	 */ 
	
	@Override
	public String toString() {
		
		StringBuffer buffer = new StringBuffer();
		buffer.append("type: "+type);
		buffer.append(" version: "+version);
		buffer.append(" effective_date: "+effective_date);
		buffer.append(" description: "+description);
		buffer.append(" title: "+title);
		buffer.append(" is_current_version: "+is_current_version);
		return buffer.toString();
	}

    /**
     * Gets the xarcrules.
     *
     * @return the xarcrules
     */
    public List<Xarcrules> getXarcrules() {
        if(xarcrules==null) {
            xarcrules = new ArrayList<Xarcrules>();
        }
        return xarcrules;
    }

    /**
     * Sets the xarcrules.
     *
     * @param xarcrules the new xarcrules
     */
    public void setXarcrules(List<Xarcrules> xarcrules) {
        this.xarcrules = xarcrules;
    }

    /**
     * Returns the view	
     * @return the view
     */
    public List<View> getView() {
        if(view==null) {
            view = new ArrayList<View>();
        }
        return view;
    }

    /**
     * Sets the view to view
     * @param view the view to set
     */
    public void setView(List<View> view) {
        this.view = view;
    }

    /**
     * Returns the acordtoworkitemmap	
     * @return the acordtoworkitemmap
     */
    public List<Acordtoworkitemmap> getAcordtoworkitemmap() {
        if(acordtoworkitemmap==null) {
            acordtoworkitemmap = new ArrayList<Acordtoworkitemmap>();
        }
        return acordtoworkitemmap;
    }

    /**
     * Sets the acordtoworkitemmap to acordtoworkitemmap
     * @param acordtoworkitemmap the acordtoworkitemmap to set
     */
    public void setAcordtoworkitemmap(List<Acordtoworkitemmap> acordtoworkitemmap) {
        this.acordtoworkitemmap = acordtoworkitemmap;
    }

    /**
     * Returns the transformer	
     * @return the transformer
     */
    public List<Transformer> getTransformer() {
        if(transformer==null) {
            transformer = new ArrayList<Transformer>();
        }
        return transformer;
    }

    /**
     * Sets the transformer to transformer
     * @param transformer the transformer to set
     */
    public void setTransformer(List<Transformer> transformer) {
        this.transformer = transformer;
    }
    
    /**
     * Gets the pdfdefinitions.
     *
     * @return the pdfdefinitions
     */
    public List<Pdfdefinitions> getPdfdefinitions() {
        if(pdfdefinitions==null) {
            pdfdefinitions = new ArrayList<Pdfdefinitions>();
        }
        return pdfdefinitions;
    }

    /**
     * Sets the pdfdefinitions.
     *
     * @param pdfdefinitions the new pdfdefinitions
     */
    public void setPdfdefinitions(List<Pdfdefinitions> pdfdefinitions) {
        this.pdfdefinitions = pdfdefinitions;
    }

	public List<Groovy> getGroovy() {
		if(this.groovy==null){
			groovy = new ArrayList<Groovy>();
		}
		return groovy;
	}

	public void setGroovy(List<Groovy> groovy) {
		this.groovy = groovy;
	}

	public List<Markdown> getMarkdown() {
		if(this.markdown==null){
			markdown = new ArrayList<Markdown>();
		}
		return markdown;
	}

	public void setMarkdown(List<Markdown> markdown) {
		this.markdown = markdown;
	}
}
