/*
 * Created on Apr 19, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.loader.optionlist;

/**
 * The OptionList class
 */
public class OptionList {
    
    /**
     * The <code>location</code>.
     */
    private String location;

    /**
     * Gets the location.
     *
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * Sets the location.
     *
     * @param location the new location
     */
    public void setLocation(String location) {
        this.location = location;
    }
    
    /** 
     * {@inheritDoc}
     */ 
    
    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" location: "+location);
        return buffer.toString();
    }

}
