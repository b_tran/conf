/*
 * Created on Mar 25, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.loader.optionlist;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Service;

import com.agencyport.configuration.entity.Client;
import com.agencyport.configuration.entity.Product;
import com.agencyport.configuration.entity.jaxb.codelist.TCodeListRef;
import com.agencyport.configuration.entity.jaxb.codelist.TOptionList;
import com.agencyport.logging.ExceptionLogger;
import com.agencyport.logging.LoggingManager;

/**
 * The OptionListLoader class
 */
@Service
public class OptionListLoader {
    /**
     * The <code>logger</code> logger for this instance
     */

    final  Logger logger = LoggingManager.getLogger(OptionListLoader.class.getName());

	/**
	 * The <code>transaction manager</code>.
	 */
	@Autowired
	JpaTransactionManager transactionManager;

	/**
	 * Load.
	 *
	 * @param client the client
	 * @param optionList the option list
	 * @param input the input
	 * @throws Exception the exception
	 */
	@SuppressWarnings("unchecked")
	public void load(Client client,com.agencyport.configuration.loader.optionlist.OptionList optionList, InputStream input) throws Exception {
			logger.info("Loading :"+optionList);
			EntityManagerFactory entityManagerFactory = transactionManager.getEntityManagerFactory();
			EntityManager em = entityManagerFactory.createEntityManager();
			JAXBContext jc1 = JAXBContext.newInstance(com.agencyport.configuration.entity.jaxb.codelist.ObjectFactory.class);
			Unmarshaller u1 = jc1.createUnmarshaller();
			JAXBElement<TCodeListRef> jaxbTCodeListRef = (JAXBElement<TCodeListRef>) u1.unmarshal(input);
			TCodeListRef eTCodeListRef = jaxbTCodeListRef.getValue();
			try {
    			for(TOptionList eTOptionList:  eTCodeListRef.getOptionList()) {
    			    try {
            			em.getTransaction().begin();
            			
            			Object c = em.createQuery("select c from client c where c.name = :name")
            			.setParameter("name", client.getName())
            			.getResultList().stream().findFirst().orElse(null);
            			
            			if(c != null) {
            				client = (Client)c;
            			}
            			
            			eTOptionList.setClient(client);
            			client.getOptionList().add(eTOptionList);
            			
            			eTOptionList.getOption().forEach(o->{
            				o.setOptionList(eTOptionList);
            				
            			});
            			
            			em.persist(client);
            			em.flush();
            			em.getTransaction().commit();
    			    } catch (Exception exception) {
    			        logger.severe(exception.getMessage()+ ":"+ optionList.toString());
    			        try {
    	                    if(em.getTransaction().isActive()) {
    	                        em.getTransaction().rollback();
    	                    }
    	                }catch (Exception exception2) {
    	                    ExceptionLogger.log(exception2, getClass(), "Transaction#rollback()");
    	                }
                    }
    			}
			}finally{
			    em.close();
			}

	}
	
	/**
	 * Load.
	 *
	 * @param client the client
	 * @param product the product
	 * @param optionList the option list
	 * @param input the input
	 * @throws Exception the exception
	 */
	@SuppressWarnings("unchecked")
    public void load(Client client, Product product,com.agencyport.configuration.loader.optionlist.OptionList optionList, InputStream input) throws Exception {
            logger.info("Loading :"+optionList);
            EntityManagerFactory entityManagerFactory = transactionManager.getEntityManagerFactory();
            EntityManager em = entityManagerFactory.createEntityManager();
            JAXBContext jc1 = JAXBContext.newInstance(com.agencyport.configuration.entity.jaxb.codelist.ObjectFactory.class);
            Unmarshaller u1 = jc1.createUnmarshaller();
            JAXBElement<TCodeListRef> jaxbTCodeListRef = (JAXBElement<TCodeListRef>) u1.unmarshal(input);
            TCodeListRef eTCodeListRef = jaxbTCodeListRef.getValue();
            for(TOptionList eTOptionList:  eTCodeListRef.getOptionList()) {
                try {
                    em.getTransaction().begin();
                    Object c = em.createQuery("select c from client c where c.name = :name")
                    .setParameter("name", client.getName())
                    .getResultList().stream().findFirst().orElse(null);
                    
                    if(c != null) {
                        client = (Client)c;
                    }
                    
                    Object  p =  em.createQuery("select p from product p where p.clientId = :clientId "
                            + "and p.type = :type and p.version = :version and p.effectiveDate = :effectiveDate")
                            .setParameter("clientId", client.getId())
                            .setParameter("type", product.getType())
                            .setParameter("version", product.getVersion())
                            .setParameter("effectiveDate", product.getEffectiveDate())
                            .getResultList().stream().findFirst().orElse(null);
                    
                    if(p!=null) {
                        product = (Product)(p);
                    }
                    final Product eP = product;
                    eTOptionList.setClient(client);
                    client.getOptionList().add(eTOptionList);
                    
                    eTOptionList.getOption().forEach(o->{
                        o.setOptionList(eTOptionList);
                        o.setEffectiveDate(eP.getEffectiveDate());
                        o.setProduct(eP);
                        
                    });
                    em.persist(product);
                    em.flush();
                    em.getTransaction().commit();
                } catch (Exception exception) {
                    logger.severe(exception.getMessage()+ ":"+ optionList.toString());
                    try {
                        if(em.getTransaction().isActive()) {
                            em.getTransaction().rollback();
                        }
                    }catch (Exception exception2) {
                        ExceptionLogger.log(exception2, getClass(), "Transaction#rollback()");
                    }
                
                }
            }
            em.close();

    }

	/**
	 * Read.
	 *
	 * @param optionListId the option list id
	 * @return the string
	 * @throws Exception the exception
	 */
	public String read(int optionListId) throws Exception {

		JAXBContext jc1 = JAXBContext.newInstance(com.agencyport.configuration.entity.jaxb.codelist.ObjectFactory.class);
		Marshaller m1 = jc1.createMarshaller();

		EntityManagerFactory entityManagerFactory = transactionManager.getEntityManagerFactory();
		EntityManager em = entityManagerFactory.createEntityManager();
		
		try {
			
		    TOptionList optionList = em.find(TOptionList.class, optionListId);
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			m1 = jc1.createMarshaller();
			m1.marshal(optionList, baos);
			return new String(baos.toByteArray());

	
	}finally {
		em.close();
	}

}


}
