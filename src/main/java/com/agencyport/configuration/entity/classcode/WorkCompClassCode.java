/*
 * Created on Apr 20, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.entity.classcode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

import com.agencyport.configuration.entity.Client;

/**
 * The WorkCompClassCode class
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WorkCompClassCode", propOrder = {
    "value"
})
@Entity(name = "ref_data_work_comp_class_code")
@Table(name = "ref_data_work_comp_class_code" , uniqueConstraints = @UniqueConstraint(columnNames = { "state_prov_cd","client_id","type_code","type_description" }))
@NamedQuery(name = "ref_data_work_comp_class_code.findAll", query = "SELECT a FROM ref_data_work_comp_class_code a")
@XmlRootElement(name = "option")
public class WorkCompClassCode {
    
    /**
     * The <code>auto id</code>.
     */
    @XmlTransient
    @Id
    @Column(name = "auto_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int autoId;
    
    /**
     * The <code>value</code>.
     */
    @Column(name = "type_description")
    @XmlValue
    protected String value;
    
    /**
     * The <code>attr value</code>.
     */
    @Column(name = "type_code")
    @XmlAttribute(name = "value", required = true)
    protected String attrValue;
    
    /**
     * The <code>effective date</code>.
     */
    @XmlTransient
    @Temporal(TemporalType.DATE)
    @Column(name = "effective_date")
    private java.util.Date effectiveDate;
    
    /**
     * The <code>expiration date</code>.
     */
    @XmlTransient
    @Temporal(TemporalType.DATE)
    @Column(name = "expiration_date")
    private java.util.Date expirationDate;
    
    /**
     * The <code>state prov cd</code>.
     */
    @XmlTransient
    @Column(name = "state_prov_cd")
    protected String stateProvCd;
    
    /**
     * The <code>class code type</code>.
     */
    @XmlTransient
    @Column(name = "class_code_type")
    protected String classCodeType;
    
    
    /**
     * The <code>client</code>.
     */
    @XmlTransient
    @ManyToOne
    @JoinColumn(name="client_id")
    private Client client;
    
    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the value of the attrValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttrValue() {
        return attrValue;
    }

    /**
     * Sets the value of the attrValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttrValue(String value) {
        this.attrValue = value;
    }

    /**
     * Gets the effective date.
     *
     * @return the effective date
     */
    public java.util.Date getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Sets the effective date.
     *
     * @param effectiveDate the new effective date
     */
    public void setEffectiveDate(java.util.Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    /**
     * Gets the auto id.
     *
     * @return the auto id
     */
    public int getAutoId() {
        return autoId;
    }

    /**
     * Gets the state prov cd.
     *
     * @return the state prov cd
     */
    public String getStateProvCd() {
        return stateProvCd;
    }

    /**
     * Sets the state prov cd.
     *
     * @param stateProvCd the new state prov cd
     */
    public void setStateProvCd(String stateProvCd) {
        this.stateProvCd = stateProvCd;
    }

    /**
     * Gets the client.
     *
     * @return the client
     */
    public Client getClient() {
        return client;
    }

    /**
     * Sets the client.
     *
     * @param client the new client
     */
    public void setClient(Client client) {
        this.client = client;
    }

    /**
     * Returns the expirationDate	
     * @return the expirationDate
     */
    public java.util.Date getExpirationDate() {
        return expirationDate;
    }

    /**
     * Sets the expirationDate to expirationDate
     * @param expirationDate the expirationDate to set
     */
    public void setExpirationDate(java.util.Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    /**
     * Returns the classCodeType	
     * @return the classCodeType
     */
    public String getClassCodeType() {
        return classCodeType;
    }

    /**
     * Sets the classCodeType to classCodeType
     * @param classCodeType the classCodeType to set
     */
    public void setClassCodeType(String classCodeType) {
        this.classCodeType = classCodeType;
    }
    
    

}
