/*
 * Created on Mar 22, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.agencyport.configuration.entity.groovy.GroovyModel;
import com.agencyport.configuration.entity.jaxb.acordtoworkitemmap.Acordtoworkitemmap;
import com.agencyport.configuration.entity.jaxb.behavior.TransactionDefinitionBehavior;
import com.agencyport.configuration.entity.jaxb.codelist.TOption;
import com.agencyport.configuration.entity.jaxb.transdef.TTransaction;
import com.agencyport.configuration.entity.jaxb.transformer.TTransformer;
import com.agencyport.configuration.entity.jaxb.view.TView;
import com.agencyport.configuration.entity.jaxb.xarcrules.RuleFile;
import com.agencyport.configuration.entity.markdown.MarkdownPojo;
import com.agencyport.rest.AtomLink;

/**
 * The Product class
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Entity(name = "product")
@Table(name = "product", uniqueConstraints = @UniqueConstraint(columnNames = { "client_id", "type", "version", "effective_date" }) )
@NamedQuery(name = "product.findAll", query = "SELECT a FROM product a")
public class Product {
    /**
     * The <code>id</code> is the id.
     */
    @XmlAttribute(name = "id")
    @Id
    @Column(name = "auto_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    /**
     * The <code>type</code> is the type/lob
     */
    @XmlAttribute(name = "type")
    @Column(name = "type")
    private String type;
    /**
     * The <code>version</code> is the version.
     */
    @XmlAttribute(name = "version")
    @Column(name = "version")
    private String version;
    /**
     * The <code>title</code> is the title.
     */
    @XmlAttribute(name = "title")
    @Column(name = "title")
    private String title;
    /**
     * The <code>description</code> is the description.
     */
    @XmlAttribute(name = "description")
    @Column(name = "description")
    private String description;
    /**
     * The <code>effectiveDate</code> is date on which the product is/was
     * available.
     */
    @XmlAttribute(name = "effectiveDate")
    @Temporal(TemporalType.DATE)
    @Column(name = "effective_date")
    private java.util.Date effectiveDate;
    /**
     * The <code>currrentVersion</code> is version of product
     */
    @XmlAttribute(name = "isCurrentVersion")
    @Column(name = "is_current_version")
    private boolean isCurrentVersion;

    /**
     * The <code>client</code>.
     */
    @XmlTransient
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn
    private Client client;

    /**
     * The <code>t transactions</code>.
     */
    @XmlTransient
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "product", cascade = CascadeType.ALL)
    @OrderBy("auto_id ASC")
    private List<TTransaction> tTransactions;

    /**
     * The <code>transaction definition behavior</code>.
     */
    @XmlTransient
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "product", cascade = CascadeType.ALL)
    @OrderBy("auto_id ASC")
    private List<TransactionDefinitionBehavior> transactionDefinitionBehavior;

    /**
     * The <code>client id</code>.
     */
    @XmlAttribute(name = "clientId")
    @Column(name = "client_id", insertable = false, updatable = false)
    private int clientId;

    /**
     * The <code>link</code>.
     */
    @Transient
    private List<AtomLink> link;
    
    /**
     * The <code>rule file</code>.
     */
    @XmlTransient
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "product", cascade = CascadeType.ALL)
    @OrderBy("auto_id ASC")
    private List<RuleFile> ruleFile;
    
    /**
     * The <code>acordtoworkitemmap</code>.
     */
    @XmlTransient
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "product", cascade = CascadeType.ALL)
    @OrderBy("auto_id ASC")
    private List<Acordtoworkitemmap> acordtoworkitemmap;
    
    /**
     * The <code>transformer</code>.
     */
    @XmlTransient
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "product", cascade = CascadeType.ALL)
    @OrderBy("auto_id ASC")
    private List<TTransformer> transformer;
    
    /**
     * The <code>transformer</code>.
     */
    @XmlTransient//prevents naming collision between javabean attribute name and xml field names
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "product", cascade = CascadeType.ALL)
    private List<GroovyModel> groovy;
    
    /**
     * The <code>transformer</code>.
     */
    @XmlTransient
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "product", cascade = CascadeType.ALL)
    private List<MarkdownPojo> markdown;
    
    /**
     * The <code>view</code>.
     */
    @XmlTransient
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "product", cascade = CascadeType.ALL)
    @OrderBy("auto_id ASC")
    private List<TView> view;
    
    @XmlTransient
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "product", cascade = CascadeType.ALL)
    @OrderBy("auto_id ASC")
    private List<TOption> option;

    /**
     * Get product type/LOB
     * 
     * @return
     */
    public String getType() {
        return type;
    }

    /**
     * Set Product type/LOB
     * 
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Get Version
     * 
     * @return version
     */
    public String getVersion() {
        return version;
    }

    /**
     * Set Version
     * 
     * @param version
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * Get Title
     * 
     * @return title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Set title
     * 
     * @param title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Get Description
     * 
     * @return Description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set description
     * 
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Get Effective date
     * 
     * @return
     */
    public java.util.Date getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Set effective date
     * 
     * @param effectiveDate
     */
    public void setEffectiveDate(java.util.Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    /**
     * Return True if this is the current version.
     * 
     * @return
     */
    public boolean isCurrrentVersion() {
        return isCurrentVersion;
    }

    /**
     * Set current version
     * 
     * @param isCurrrentVersion
     */
    public void setCurrrentVersion(boolean isCurrrentVersion) {
        this.isCurrentVersion = isCurrrentVersion;
    }

    /**
     * Get Client associated with this product
     * 
     * @return client
     */
    public Client getClient() {
        return client;
    }

    /**
     * Set client
     * 
     * @param client
     */

    public void setClient(Client client) {
        this.client = client;
    }

    /**
     * Get List of transaction.
     * 
     * @return
     */
    public List<TTransaction> gettTransactions() {
        if (tTransactions == null) {
            tTransactions = new ArrayList<TTransaction>();
        }
        return tTransactions;
    }

    

    /**
     * Get Product id.
     * 
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     * Get Client Id.
     * 
     * @return Client Id.
     */
    public int getClientId() {
        return clientId;
    }

    /**
     * Get AtomLink
     * 
     * @return
     */
    public List<AtomLink> getLink() {
        if (link == null) {
            link = new ArrayList<AtomLink>();
        }
        return link;
    }

    /**
     * Returns the ruleFile	
     * @return the ruleFile
     */
    public List<RuleFile> getRuleFile() {
        if (ruleFile == null) {
            ruleFile = new ArrayList<RuleFile>();
        }
        return ruleFile;
    }

    /**
     * Returns the acordtoworkitemmap	
     * @return the acordtoworkitemmap
     */
    public List<Acordtoworkitemmap> getAcordtoworkitemmap() {
        if (acordtoworkitemmap == null) {
            acordtoworkitemmap = new ArrayList<Acordtoworkitemmap>();
        }
        return acordtoworkitemmap;
    }

    /**
     * Returns the transactionDefinitionBehavior	
     * @return the transactionDefinitionBehavior
     */
    public List<TransactionDefinitionBehavior> getTransactionDefinitionBehavior() {
        if (transactionDefinitionBehavior == null) {
            transactionDefinitionBehavior = new ArrayList<TransactionDefinitionBehavior>();
        }
        return transactionDefinitionBehavior;
    }

    /**
     * Returns the transformer	
     * @return the transformer
     */
    public List<TTransformer> getTransformer() {
        if (transformer == null) {
            transformer = new ArrayList<TTransformer>();
        }
        return transformer;
    }

    /**
     * Returns the view	
     * @return the view
     */
    public List<TView> getView() {
        if (view == null) {
            view = new ArrayList<TView>();
        }
        return view;
    }

    /**
     * Returns the option	
     * @return the option
     */
    public List<TOption> getOption() {
        if (option == null) {
            option = new ArrayList<TOption>();
        }
        return option;
    }
    
    /**
     * Returns the groovy	
     * @return the groovy
     */
    public List<GroovyModel> getGroovy() {
        if (groovy == null) {
            groovy = new ArrayList<GroovyModel>();
        }
        return groovy;
    }
    
    /**
     * Returns the markdown	
     * @return the markdown
     */
    public List<MarkdownPojo> getMarkdown() {
        if (markdown == null) {
            markdown = new ArrayList<MarkdownPojo>();
        }
        return markdown;
    }
}
