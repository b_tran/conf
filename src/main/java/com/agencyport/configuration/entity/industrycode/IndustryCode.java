/*
 * Created on Apr 20, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.entity.industrycode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

import com.agencyport.configuration.entity.Client;

/**
 * The IndustryCode class
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IndustryCode", propOrder = {
    "value"
})
@Entity(name = "ref_data_industry_code")
@Table(name = "ref_data_industry_code" , uniqueConstraints = @UniqueConstraint(columnNames = { "industry_cd_type","client_id","type_code" }))
@NamedQuery(name = "ref_data_industry_code.findAll", query = "SELECT a FROM ref_data_industry_code a")
@XmlRootElement(name = "option")
public class IndustryCode {
    
    /**
     * The <code>auto id</code>.
     */
    @XmlTransient
    @Id
    @Column(name = "auto_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int autoId;
    
    /**
     * The <code>value</code>.
     */
    @Column(name = "type_description")
    @XmlValue
    protected String value;
    
    /**
     * The <code>attr value</code>.
     */
    @Column(name = "type_code")
    @XmlAttribute(name = "value", required = true)
    protected String attrValue;
    
    /**
     * The <code>effective date</code>.
     */
    @XmlTransient
    @Temporal(TemporalType.DATE)
    @Column(name = "effective_date")
    private java.util.Date effectiveDate;
    
    /**
     * The <code>industry cd type</code>.
     */
    @XmlTransient
    @Column(name = "industry_cd_type")
    protected String industryCdType;
    
    /**
     * The <code>client</code>.
     */
    @XmlTransient
    @ManyToOne
    @JoinColumn(name="client_id")
    private Client client;
    
    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the value of the attrValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttrValue() {
        return attrValue;
    }

    /**
     * Sets the value of the attrValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttrValue(String value) {
        this.attrValue = value;
    }

    /**
     * Gets the effective date.
     *
     * @return the effective date
     */
    public java.util.Date getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Sets the effective date.
     *
     * @param effectiveDate the new effective date
     */
    public void setEffectiveDate(java.util.Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    /**
     * Gets the auto id.
     *
     * @return the auto id
     */
    public int getAutoId() {
        return autoId;
    }


    /**
     * Gets the industry cd type.
     *
     * @return the industry cd type
     */
    public String getIndustryCdType() {
        return industryCdType;
    }

    /**
     * Sets the industry cd type.
     *
     * @param industryCdType the new industry cd type
     */
    public void setIndustryCdType(String industryCdType) {
        this.industryCdType = industryCdType;
    }
    
    /**
     * Gets the client.
     *
     * @return the client
     */
    public Client getClient() {
        return client;
    }

    /**
     * Sets the client.
     *
     * @param client the new client
     */
    public void setClient(Client client) {
        this.client = client;
    }
    
    

}
