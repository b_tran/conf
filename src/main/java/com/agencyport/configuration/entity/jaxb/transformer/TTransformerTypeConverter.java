/*
 * Created on Apr 12, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.entity.jaxb.transformer;

import java.io.Serializable;

import javax.persistence.AttributeConverter;

/**
 * The OpenModeConverter class
 */
public class TTransformerTypeConverter implements AttributeConverter<TTransformerType , String>,Serializable {
    
    /**
     * The <code>serialVersionUID</code>
     */
    private static final long serialVersionUID = 9046460518927341363L;

    /** 
     * {@inheritDoc}
     */ 
    
    @Override
    public String convertToDatabaseColumn(TTransformerType attribute) {
        switch (attribute) {
            case AGGREGATOR:
                return TTransformerType.AGGREGATOR.value();
            case JAVA:
                return TTransformerType.JAVA.value();
            case LINK:
                return TTransformerType.LINK.value();
            case XSLT:
                return TTransformerType.XSLT.value();    
            default:
                throw new IllegalArgumentException("Unknown" + attribute);
        }
    }
    
    /** 
     * {@inheritDoc}
     */ 
    
    @Override
    public TTransformerType convertToEntityAttribute(String dbData) {
        switch (dbData) {
            case "aggregator":
                return TTransformerType.AGGREGATOR;
            case "java":
                return TTransformerType.JAVA;
            case "link":
                return TTransformerType.LINK;
            case "XSLT":
                return TTransformerType.XSLT;
            default:
                throw new IllegalArgumentException("Unknown" + dbData);
        }
    }

}
