/*
 * Created on Apr 12, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.entity.jaxb.workflowdefinition;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.AttributeConverter;

/**
 * The OpenModeConverter class
 */
public class PageTypeConverter implements AttributeConverter<List<TPageType> , String> ,Serializable {
    
    /**
     * The <code>serialVersionUID</code>
     */
    private static final long serialVersionUID = 3072806694051863627L;

    /** 
     * {@inheritDoc}
     */ 
    
    @Override
    public String convertToDatabaseColumn(List<TPageType> attribute) {
        if(attribute==null)
            return null;
        
        StringBuilder buffer = new StringBuilder();
        attribute.forEach(op -> {
            buffer.append(";").append(op.value());
        });
        String str = buffer.toString().substring(";".length());
        return str;

    }
    
    /** 
     * {@inheritDoc}
     */ 
    
    @Override
    public List<TPageType> convertToEntityAttribute(String dbData) {
        if(dbData==null)
            return null;
        
        List<TPageType> returnVal = new ArrayList<TPageType>();
        if (dbData != null) {
            Arrays.asList(dbData.split(";")).forEach(o -> {
                returnVal.add(TPageType.fromValue(o));
            });
        }
        return returnVal;

    }

}
