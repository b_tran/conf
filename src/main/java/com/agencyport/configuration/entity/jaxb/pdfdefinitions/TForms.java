//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.06.15 at 07:59:51 AM EDT 
//


package com.agencyport.configuration.entity.jaxb.pdfdefinitions;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.agencyport.configuration.entity.jaxb.transdef.TTransaction;


/**
 * Type definition for the PDF forms aggregate
 * 
 * <p>Java class for T_forms complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="T_forms">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}formGroups" minOccurs="0"/>
 *         &lt;element ref="{}form" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *       &lt;attribute name="lob" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "T_forms", propOrder = {
    "formGroups",
    "form"
})
@Entity(name = "pdfdefinitions_forms")
@Table(name = "pdfdefinitions_forms", uniqueConstraints = @UniqueConstraint(columnNames = { "lob",  "transaction_auto_id" }) )
@NamedQuery(name = "pdfdefinitions_forms.findAll", query = "SELECT a FROM pdfdefinitions_forms a")
public class TForms {
    
    /**
     * The <code>auto id</code>.
     */
    @XmlTransient
    @Id
    @Column(name = "auto_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int autoId;
    
    @OneToOne(fetch = FetchType.LAZY, mappedBy = "forms", cascade = CascadeType.ALL)
    protected TFormGroups formGroups;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "forms", cascade = CascadeType.ALL)
    @XmlElement(required = true)
    protected List<TForm> form;
    
    @Column(name = "lob")
    @XmlAttribute(name = "lob", required = true)
    protected String lob;
    
    @XmlTransient
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn
    private TTransaction transaction;


    /**
     * Gets the value of the formGroups property.
     * 
     * @return
     *     possible object is
     *     {@link TFormGroups }
     *     
     */
    public TFormGroups getFormGroups() {
        return formGroups;
    }

    /**
     * Sets the value of the formGroups property.
     * 
     * @param value
     *     allowed object is
     *     {@link TFormGroups }
     *     
     */
    public void setFormGroups(TFormGroups value) {
        this.formGroups = value;
    }

    /**
     * Gets the value of the form property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the form property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getForm().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TForm }
     * 
     * 
     */
    public List<TForm> getForm() {
        if (form == null) {
            form = new ArrayList<TForm>();
        }
        return this.form;
    }

    /**
     * Gets the value of the lob property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLob() {
        return lob;
    }

    /**
     * Sets the value of the lob property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLob(String value) {
        this.lob = value;
    }

    /**
     * Returns the transaction	
     * @return the transaction
     */
    public TTransaction getTransaction() {
        return transaction;
    }

    /**
     * Sets the transaction to transaction
     * @param transaction the transaction to set
     */
    public void setTransaction(TTransaction transaction) {
        this.transaction = transaction;
    }
    
    /**
     * Transfer data from JAXB element to JPA entity. transferToEntity
     */
    
    @PrePersist
    public void transferToEntity() {
        
        getForm().forEach(form->{
            form.setForms(this);
        });
        if(formGroups!= null)
            formGroups.setForms(this);

    }

    /**
     * Transfer data from JPA entity to JAXB element. transferFromEntity
     */
    @PostLoad
    public void transferFromEntity() {
        
        getForm().forEach(form->{
            form.setForms(this);
        });
        if(formGroups!= null)
            formGroups.setForms(this);


    }

}
