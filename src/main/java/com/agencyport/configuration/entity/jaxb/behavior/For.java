/*
 * Created on Mar 24, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.entity.jaxb.behavior;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlTransient;

/**
 * The For class
 */

@Entity(name = "behavior_for")
@Table(name = "behavior_for")
@NamedQuery(name = "behavior_for.findAll", query = "SELECT a FROM behavior_for a")
public class For {

    /**
     * The <code>auto id</code>.
     */
    @Id
    @Column(name = "auto_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int autoId;

    /**
     * The <code>value</code>.
     */
    @Column(name = "value")
    private String value;

    /**
     * The <code>behavior</code>.
     */
    @XmlTransient
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn
    private Behavior behavior;

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value.
     *
     * @param value the new value
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the behavior.
     *
     * @return the behavior
     */
    public Behavior getBehavior() {
        return behavior;
    }

    /**
     * Sets the behavior.
     *
     * @param behavior the new behavior
     */
    public void setBehavior(Behavior behavior) {
        this.behavior = behavior;
    }

    /**
     * Get Auto generated ID
     * 
     * @return autoId
     */
    public int getAutoId() {
        return autoId;
    }

}
