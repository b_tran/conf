/**
 * @author scottsurette
 */

package com.agencyport.configuration.entity.jaxb.workflowdefinition;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for T_reasons complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * <xs:complexType name="T_reasons">
 *    <xs:sequence>
 *      <xs:element ref="reasonOptions" minOccurs="0" maxOccurs="1"/>
 *      <xs:element ref="allowReasonComment" minOccurs="0" maxOccurs="1"/>
 *    </xs:sequence>
 *  </xs:complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "T_reasons", propOrder = {
    "reasonOptions",
    "allowReasonComment"
})
@Entity(name = "work_flow_reasons")
@Table(name = "work_flow_reasons" )
@NamedQuery(name = "work_flow_reasons.findAll", query = "SELECT a FROM work_flow_reasons a")
public class TReasons {
    
    /**
     * The <code>auto id</code>.
     */
    @XmlTransient
    @Id
    @Column(name = "auto_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int autoId;

    /**
     * The <code>reason_options</code>.
     */
    @OneToOne(fetch = FetchType.LAZY, mappedBy = "reasons", cascade = CascadeType.ALL )
    @XmlElement
    protected TReasonOptions reasonOptions;
    
    /**
     * The <code>allow_reason_comment</code>
     */
    @OneToOne(fetch = FetchType.LAZY, mappedBy = "reasons", cascade = CascadeType.ALL )
    @XmlElement
    protected TAllowReasonComment allowReasonComment;
    
    /**
     * The <code>work item action</code>.
     */
    @XmlTransient
    @OneToOne(fetch = FetchType.LAZY)
    private TWorkItemAction workItemAction;

    /**
     * Gets the work item action.
     *
     * @return the work item action
     */
    public TWorkItemAction getWorkItemAction() {
        return workItemAction;
    }

    /**
     * Sets the work item action.
     *
     * @param workItemAction the new work item action
     */
    public void setWorkItemAction(TWorkItemAction workItemAction) {
        this.workItemAction = workItemAction;
    }

    /**
     * Gets the auto id.
     *
     * @return the auto id
     */
    public int getAutoId() {
        return autoId;
    }

	/**
	 * @return the reasonOptions
	 */
	public TReasonOptions getReasonOptions() {
		return reasonOptions;
	}

	/**
	 * @param reasonOptions the reasonOptions to set
	 */
	public void setReasonOptions(TReasonOptions reasonOptions) {
		this.reasonOptions = reasonOptions;
	}

	/**
	 * @return the allowReasonComment
	 */
	public TAllowReasonComment getAllowReasonComment() {
		return allowReasonComment;
	}

	/**
	 * @param allowReasonComment the allowReasonComment to set
	 */
	public void setAllowReasonComment(TAllowReasonComment allowReasonComment) {
		this.allowReasonComment = allowReasonComment;
	}
}
