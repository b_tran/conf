package com.agencyport.configuration.entity.jaxb.menudefinition;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "searchEntry_Type")
@Entity(name = "menu_search_entry_type")
@Table(name = "menu_search_entry_type", uniqueConstraints = @UniqueConstraint(columnNames = { "auto_id", "menuMemberType_auto_id" }))
@NamedQuery(name = "menu_search_entry_type.findAll", query = "SELECT a FROM menu_search_entry_type a")
public class SearchEntryType {
	
	/**
     * The <code>auto id</code>.
     */
    @XmlTransient
    @Id
    @Column(name = "auto_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int autoId;
    
    /**
     * The <code>searchToken</code>.
     */
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "searchEntryType", cascade = CascadeType.ALL)
    @XmlElement(required = true)
    protected List<SearchTokenType> searchToken;
    
    /**
     * The <code>description</code>
     */
    @Column(name = "description")
    @XmlAttribute(name = "description", required = true)
    protected String description;
    
    /**
     * The <code>menu member type</code>.
     */
    @XmlTransient
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn
    private MenuMemberType menuMemberType;
    
    public int getAutoId() {
    	return autoId;
    }
    
    public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public MenuMemberType getMenuMemberType() {
		return menuMemberType;
	}

	public void setMenuMemberType(MenuMemberType menuMemberType) {
		this.menuMemberType = menuMemberType;
	}
	
	public List<SearchTokenType> getSearchToken() {
		if (searchToken == null) {
            searchToken = new ArrayList<SearchTokenType>();
        }
        return this.searchToken;
    }
}
