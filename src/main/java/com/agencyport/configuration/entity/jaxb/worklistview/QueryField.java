//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.04.06 at 05:07:41 PM EDT 
//

package com.agencyport.configuration.entity.jaxb.worklistview;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element ref="{}opCode"/>
 *         &lt;element ref="{}operands"/>
 *       &lt;/sequence>
 *       &lt;attribute name="interactive" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
 *       &lt;attribute name="pageFieldRefId" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="fieldRefId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="isSaveable" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Entity(name = "worklist_query_field")
@Table(name = "worklist_query_field")
@NamedQuery(name = "worklist_query_field.findAll", query = "SELECT a FROM worklist_query_field a")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "opCode",
        "operands"
})
@XmlRootElement(name = "queryField")
public class QueryField {

    /**
     * The <code>auto id</code>.
     */
    @XmlTransient
    @Id
    @Column(name = "auto_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int autoId;

    /**
     * The <code>op code</code>.
     */
    @Column(name = "op_code", length = 100)
    @XmlSchemaType(name = "string")
    protected OpCodeType opCode;

    /**
     * The <code>operands</code>.
     */
    @OneToOne(fetch = FetchType.LAZY, mappedBy = "queryField", cascade = CascadeType.ALL)
    protected Operands operands;

    /**
     * The <code>interactive</code>.
     */
    @Column(name = "interactive")
    @XmlAttribute(name = "interactive")
    protected Boolean interactive;

    /**
     * The <code>page field ref id</code>.
     */
    @Column(name = "page_field_ref_id")
    @XmlAttribute(name = "pageFieldRefId")
    protected String pageFieldRefId;

    /**
     * The <code>field ref id</code>.
     */
    @Column(name = "field_ref_id")
    @XmlAttribute(name = "fieldRefId", required = true)
    protected String fieldRefId;

    /**
     * The <code>is saveable</code>.
     */
    @Column(name = "is_saveable")
    @XmlAttribute(name = "isSaveable")
    protected Boolean isSaveable;

    /**
     * The <code>query info</code>.
     */
    @XmlTransient
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn
    private QueryInfo queryInfo;

    /**
     * Defines the operator code to apply the field value with the operands.
     * 
     * @return possible object is {@link OpCodeType }
     * 
     */
    public OpCodeType getOpCode() {
        return opCode;
    }

    /**
     * Sets the value of the opCode property.
     * 
     * @param value
     *            allowed object is {@link OpCodeType }
     * 
     */
    public void setOpCode(OpCodeType value) {
        this.opCode = value;
    }

    /**
     * A container of operands. Typically there is only one operand element for
     * all operators except the opCodes of BETWEEN and ONE_OF.
     * 
     * @return possible object is {@link Operands }
     * 
     */
    public Operands getOperands() {
        return operands;
    }

    /**
     * Sets the value of the operands property.
     * 
     * @param value
     *            allowed object is {@link Operands }
     * 
     */
    public void setOperands(Operands value) {
        this.operands = value;
    }

    /**
     * Gets the value of the interactive property.
     * 
     * @return possible object is {@link Boolean }
     * 
     */
    public boolean isInteractive() {
        if (interactive == null) {
            return true;
        } else {
            return interactive;
        }
    }

    /**
     * Sets the value of the interactive property.
     * 
     * @param value
     *            allowed object is {@link Boolean }
     * 
     */
    public void setInteractive(Boolean value) {
        this.interactive = value;
    }

    /**
     * Gets the value of the pageFieldRefId property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getPageFieldRefId() {
        return pageFieldRefId;
    }

    /**
     * Sets the value of the pageFieldRefId property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setPageFieldRefId(String value) {
        this.pageFieldRefId = value;
    }

    /**
     * Gets the value of the fieldRefId property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getFieldRefId() {
        return fieldRefId;
    }

    /**
     * Sets the value of the fieldRefId property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setFieldRefId(String value) {
        this.fieldRefId = value;
    }

    /**
     * Gets the value of the isSaveable property.
     * 
     * @return possible object is {@link Boolean }
     * 
     */
    public boolean isIsSaveable() {
        if (isSaveable == null) {
            return false;
        } else {
            return isSaveable;
        }
    }

    /**
     * Sets the value of the isSaveable property.
     * 
     * @param value
     *            allowed object is {@link Boolean }
     * 
     */
    public void setIsSaveable(Boolean value) {
        this.isSaveable = value;
    }

    /**
     * Gets the query info.
     *
     * @return the query info
     */
    public QueryInfo getQueryInfo() {
        return queryInfo;
    }

    /**
     * Sets the query info.
     *
     * @param queryInfo the new query info
     */
    public void setQueryInfo(QueryInfo queryInfo) {
        this.queryInfo = queryInfo;
    }

    /**
     * Returns the autoId	
     * @return the autoId
     */
    public int getAutoId() {
        return autoId;
    }

}
