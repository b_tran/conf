/*
 * Created on Apr 12, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.entity.jaxb.workflowdefinition;

import java.io.Serializable;

import javax.persistence.AttributeConverter;

/**
 * The ChannelConverter class
 */
public class ChannelConverter implements AttributeConverter<TChannel , String> ,Serializable{
    
    /**
     * The <code>serialVersionUID</code>
     */
    private static final long serialVersionUID = 2349083372815365383L;

    /** 
     * {@inheritDoc}
     */ 
    
    @Override
    public String convertToDatabaseColumn(TChannel attribute) {
        switch (attribute) {
            case AGENT:
                return "AGENT";
            case CONSUMER:
                return "CONSUMER";
            default:
                throw new IllegalArgumentException("Unknown" + attribute);
        }
    }
    
    /** 
     * {@inheritDoc}
     */ 
    
    @Override
    public TChannel convertToEntityAttribute(String dbData) {
        switch (dbData) {
            case "AGENT":
                return TChannel.AGENT;
            case "CONSUMER":
                return TChannel.CONSUMER;
            default:
                throw new IllegalArgumentException("Unknown" + dbData);
        }
    }

}
