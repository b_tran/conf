//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.03.09 at 04:00:50 PM EST 
//


package com.agencyport.configuration.entity.jaxb.workflowdefinition;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for T_modal complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="T_modal">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}title" minOccurs="0"/>
 *         &lt;element ref="{}body" minOccurs="0"/>
 *         &lt;element ref="{}confirmButtonText" minOccurs="0"/>
 *         &lt;element ref="{}cancelButtonText" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "T_modal", propOrder = {
    "title",
    "body",
    "confirmButtonText",
    "cancelButtonText"
})
@Entity(name = "work_flow_modal")
@Table(name = "work_flow_modal" )
@NamedQuery(name = "work_flow_modal.findAll", query = "SELECT a FROM work_flow_modal a")
public class TModal {
	
	/**
     * The <code>auto id</code>.
     */
    @XmlTransient
    @Id
    @Column(name = "auto_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int autoId;

    /**
     * The <code>title</code>
     */
    @Column(name = "title")
    protected String title;
    
    /**
     * The <code>body</code>
     */
    @Column(name = "body")
    protected String body;
    
    /**
     * The <code>confirmButtonText</code>
     */
    @Column(name = "confirm_button_text")
    protected String confirmButtonText;
    
    /**
     * The <code>cancelButtonText</code>
     */
    @Column(name = "cancel_button_text")
    protected String cancelButtonText;
    
    /**
     * The <code>work item action</code>.
     */
    @XmlTransient
    @OneToOne(fetch = FetchType.LAZY)
    private TWorkItemAction workItemAction;

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Gets the value of the body property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBody() {
        return body;
    }

    /**
     * Sets the value of the body property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBody(String value) {
        this.body = value;
    }

    /**
     * Gets the value of the confirmButtonText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConfirmButtonText() {
        return confirmButtonText;
    }

    /**
     * Sets the value of the confirmButtonText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConfirmButtonText(String value) {
        this.confirmButtonText = value;
    }

    /**
     * Gets the value of the cancelButtonText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCancelButtonText() {
        return cancelButtonText;
    }

    /**
     * Sets the value of the cancelButtonText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCancelButtonText(String value) {
        this.cancelButtonText = value;
    }
    
    /**
     * Gets the work item action.
     *
     * @return the work item action
     */
    public TWorkItemAction getWorkItemAction() {
        return workItemAction;
    }

    /**
     * Sets the work item action.
     *
     * @param workItemAction the new work item action
     */
    public void setWorkItemAction(TWorkItemAction workItemAction) {
        this.workItemAction = workItemAction;
    }

    /**
     * Gets the auto id.
     *
     * @return the auto id
     */
    public int getAutoId() {
        return autoId;
    }

}
