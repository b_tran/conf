//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.04.06 at 05:07:41 PM EDT 
//

package com.agencyport.configuration.entity.jaxb.worklistview;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="fieldRefId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="acsending" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Entity(name = "worklist_default_sort_clause")
@Table(name = "worklist_default_sort_clause")
@NamedQuery(name = "worklist_default_sort_clause.findAll", query = "SELECT a FROM worklist_default_sort_clause a")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "defaultSortClause")
public class DefaultSortClause {

    /**
     * The <code>auto id</code>.
     */
    @XmlTransient
    @Id
    @Column(name = "auto_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int autoId;

    /**
     * The <code>field ref id</code>.
     */
    @Column(name = "field_ref_id")
    @XmlAttribute(name = "fieldRefId", required = true)
    protected String fieldRefId;

    /**
     * The <code>acsending</code>.
     */
    @Column(name = "acsending")
    @XmlAttribute(name = "acsending")
    protected Boolean acsending;

    /**
     * The <code>sort info</code>.
     */
    @XmlTransient
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn
    private SortInfo sortInfo;

    /**
     * Gets the value of the fieldRefId property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getFieldRefId() {
        return fieldRefId;
    }

    /**
     * Sets the value of the fieldRefId property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setFieldRefId(String value) {
        this.fieldRefId = value;
    }

    /**
     * Gets the value of the acsending property.
     * 
     * @return possible object is {@link Boolean }
     * 
     */
    public boolean isAcsending() {
        if (acsending == null) {
            return false;
        } else {
            return acsending;
        }
    }

    /**
     * Sets the value of the acsending property.
     * 
     * @param value
     *            allowed object is {@link Boolean }
     * 
     */
    public void setAcsending(Boolean value) {
        this.acsending = value;
    }

    /**
     * Gets the sort info.
     *
     * @return the sort info
     */
    public SortInfo getSortInfo() {
        return sortInfo;
    }

    /**
     * Sets the sort info.
     *
     * @param sortInfo the new sort info
     */
    public void setSortInfo(SortInfo sortInfo) {
        this.sortInfo = sortInfo;
    }

    /**
     * Gets the auto id.
     *
     * @return the auto id
     */
    public int getAutoId() {
        return autoId;
    }

}
