//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.02.05 at 05:54:50 PM IST 
//


package com.agencyport.configuration.entity.jaxb.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the generated package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _EndPoint_QNAME = new QName("", "endPoint");
    private final static QName _EndPointDefinition_QNAME = new QName("", "endPointDefinition");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link EndPointType }
     * 
     */
    public EndPointType createEndPointType() {
        return new EndPointType();
    }

    /**
     * Create an instance of {@link EndPointDefinitionType }
     * 
     */
    public EndPointDefinitionType createEndPointDefinitionType() {
        return new EndPointDefinitionType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EndPointType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "endPoint")
    public JAXBElement<EndPointType> createEndPoint(EndPointType value) {
        return new JAXBElement<EndPointType>(_EndPoint_QNAME, EndPointType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EndPointDefinitionType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "endPointDefinition")
    public JAXBElement<EndPointDefinitionType> createEndPointDefinition(EndPointDefinitionType value) {
        return new JAXBElement<EndPointDefinitionType>(_EndPointDefinition_QNAME, EndPointDefinitionType.class, null, value);
    }

}
