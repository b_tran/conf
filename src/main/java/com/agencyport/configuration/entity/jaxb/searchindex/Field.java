//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.04.06 at 05:11:17 PM EDT 
//

package com.agencyport.configuration.entity.jaxb.searchindex;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}content" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *       &lt;attribute name="type" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;enumeration value="DATE"/>
 *             &lt;enumeration value="INTEGER"/>
 *             &lt;enumeration value="DECIMAL"/>
 *             &lt;enumeration value="STATUS"/>
 *             &lt;enumeration value="STRING"/>
 *             &lt;enumeration value="LOB"/>
 *             &lt;enumeration value="TIME"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="indexFieldName" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="indexSearchFieldName" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *       &lt;attribute name="workItemPropertyName" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="isSearchable" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "content"
})
@Entity(name = "searchindex_field")
@Table(name = "searchindex_field")
@NamedQuery(name = "searchindex_field.findAll", query = "SELECT a FROM searchindex_field a")
@XmlRootElement(name = "field")
public class Field {

    /**
     * The <code>auto id</code>.
     */
    @XmlTransient
    @Id
    @Column(name = "auto_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int autoId;

    /**
     * The <code>content</code>.
     */
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "field", cascade = CascadeType.ALL)
    @XmlElement(required = true)
    protected List<Content> content;

    /**
     * The <code>type</code>.
     */
    @Column(name = "type")
    @XmlAttribute(name = "type", required = true)
    protected String type;

    /**
     * The <code>index field name</code>.
     */
    @Column(name = "index_field_name")
    @XmlAttribute(name = "indexFieldName")
    protected String indexFieldName;

    /**
     * The <code>index search field name</code>.
     */
    @Column(name = "index_search_field_name")
    @XmlAttribute(name = "indexSearchFieldName")
    protected String indexSearchFieldName;

    /**
     * The <code>name</code>.
     */
    @Column(name = "name")
    @XmlAttribute(name = "name")
    protected String name;

    /**
     * The <code>id</code>.
     */
    @Column(name = "id")
    @XmlAttribute(name = "id", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    /**
     * The <code>work item property name</code>.
     */
    @Column(name = "work_item_property_name")
    @XmlAttribute(name = "workItemPropertyName")
    @XmlSchemaType(name = "anySimpleType")
    protected String workItemPropertyName;

    /**
     * The <code>is searchable</code>.
     */
    @Column(name = "is_searchable")
    @XmlAttribute(name = "isSearchable")
    protected Boolean isSearchable;

    /**
     * The <code>index mapping</code>.
     */
    @XmlTransient
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn
    private IndexMapping indexMapping;

    /**
     * Localized content for this field Gets the value of the content property.
     * 
     * <p>
     * This accessor method returns a reference to the live list, not a
     * snapshot. Therefore any modification you make to the returned list will
     * be present inside the JAXB object. This is why there is not a
     * <CODE>set</CODE> method for the content property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getContent().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list {@link Content }
     * 
     * 
     */
    public List<Content> getContent() {
        if (content == null) {
            content = new ArrayList<Content>();
        }
        return this.content;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the indexFieldName property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getIndexFieldName() {
        return indexFieldName;
    }

    /**
     * Sets the value of the indexFieldName property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setIndexFieldName(String value) {
        this.indexFieldName = value;
    }

    /**
     * Gets the value of the indexSearchFieldName property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getIndexSearchFieldName() {
        return indexSearchFieldName;
    }

    /**
     * Sets the value of the indexSearchFieldName property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setIndexSearchFieldName(String value) {
        this.indexSearchFieldName = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the workItemPropertyName property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getWorkItemPropertyName() {
        return workItemPropertyName;
    }

    /**
     * Sets the value of the workItemPropertyName property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setWorkItemPropertyName(String value) {
        this.workItemPropertyName = value;
    }

    /**
     * Gets the value of the isSearchable property.
     * 
     * @return possible object is {@link Boolean }
     * 
     */
    public boolean isIsSearchable() {
        if (isSearchable == null) {
            return true;
        } else {
            return isSearchable;
        }
    }

    /**
     * Sets the value of the isSearchable property.
     * 
     * @param value
     *            allowed object is {@link Boolean }
     * 
     */
    public void setIsSearchable(Boolean value) {
        this.isSearchable = value;
    }

    /**
     * Get IndexMapping
     * 
     * @return indexMapping
     */
    public IndexMapping getIndexMapping() {
        return indexMapping;
    }

    /**
     * Set IndexMapping
     * 
     * @param indexMapping
     */
    public void setIndexMapping(IndexMapping indexMapping) {
        this.indexMapping = indexMapping;
    }

    /**
     * Get Auto generated ID
     * 
     * @return autoId
     */
    public int getAutoId() {
        return autoId;
    }

}
