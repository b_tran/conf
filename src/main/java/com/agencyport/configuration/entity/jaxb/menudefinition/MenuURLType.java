package com.agencyport.configuration.entity.jaxb.menudefinition;

import javax.persistence.Column;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * <p>Java class for menuURL_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="menuURL_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="isExternal" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="target" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "url")
@XmlType(name = "", propOrder = {
		"url"
	})
public class MenuURLType {
	
	@Column(name = "url")
    @XmlValue
    protected String url;
	  
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
   
    @Column(name = "isExternal")
    @XmlAttribute(name = "isExternal")
    protected Boolean isExternal;
    
    /**
     * Gets the value of the isExternal property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsExternal() {
        return isExternal;
    }

    /**
     * Sets the value of the isExternal property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsExternal(Boolean value) {
        this.isExternal = value;
    }

	@Column(name = "target")
    @XmlAttribute(name = "target")
    protected String target;
 
	 /**
     * Gets the value of the target property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
	public String getTarget() {
        return target;
    }

    /**
     * Sets the value of the target property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }
    
    @Column(name = "contentSource")
    @XmlAttribute(name = "contentSource")
    protected String contentSource;
    
    
   
    /**
     * Gets the value of the markdownSource property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMarkdownSource() {
		return contentSource;
	}

    /**
     * Sets the value of the markdownSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
	public void setMarkdownSource(String contentSource) {
		this.contentSource = contentSource;
	}

	

}
