package com.agencyport.configuration.entity.jaxb.menudefinition;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "xs:string")
@Entity(name = "menu_search_token_type")
@Table(name = "menu_search_token_type", uniqueConstraints = @UniqueConstraint(columnNames = { "auto_id", "searchEntryType_auto_id" }))
@NamedQuery(name = "menu_search_token_type.findAll", query = "SELECT a FROM menu_search_token_type a")
public class SearchTokenType {
	
	/**
     * The <code>auto id</code>.
     */
    @XmlTransient
    @Id
    @Column(name = "auto_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int autoId;
    
    /**
     * The <code>searchToken</code>.
     */
    @Column(name = "searchToken")
    @XmlValue
    protected String searchToken;
    
    /**
     * The <code>search entry type</code>.
     */
    @XmlTransient
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn
    private SearchEntryType searchEntryType;

	public String getSearchToken() {
		return searchToken;
	}

	public void setSearchToken(String searchToken) {
		this.searchToken = searchToken;
	}

	public SearchEntryType getSearchEntryType() {
		return searchEntryType;
	}

	public void setSearchEntryType(SearchEntryType searchEntryType) {
		this.searchEntryType = searchEntryType;
	}

	public int getAutoId() {
		return autoId;
	}
}
