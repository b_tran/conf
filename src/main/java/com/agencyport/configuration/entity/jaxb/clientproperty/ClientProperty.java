package com.agencyport.configuration.entity.jaxb.clientproperty;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class ClientProperty {
	
	@XmlElement(name="property")
	private List<Property> property;
	
	@XmlAttribute(name="env")
	private String environment;
	
	public String getEnvironment() {
		return environment;
	}

	public void setEnvironment(String environment) {
		this.environment = environment;
	}
	
	public List<Property> getProperty() {
		return property;
	}

	public void setProperty(List<Property> properties) {
		this.property = properties;
	}
}
