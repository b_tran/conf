/**
 * @author scottsurette
 */

package com.agencyport.configuration.entity.jaxb.workflowdefinition;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.agencyport.configuration.entity.helper.ListToStringConverter;


/**
 * <p>Java class for T_statusConstraints complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * <xs:complexType name="T_statusConstraints">
 *    <xs:sequence>
 *      <xs:element ref="mnemonic" minOccurs="0"/>
 *    </xs:sequence>
 *  </xs:complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "T_statusConstraints", propOrder = {
    "mnemonic"
})
@Entity(name = "work_flow_status_constraints")
@Table(name = "work_flow_status_constraints" )
@NamedQuery(name = "work_flow_status_constraints.findAll", query = "SELECT a FROM work_flow_status_constraints a")
public class TStatusConstraints {
    
    /**
     * The <code>auto id</code>.
     */
    @XmlTransient
    @Id
    @Column(name = "auto_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int autoId;

    /**
     * The <code>mnemonic</code>.
     */
    @Column(name = "mnemonic")
    @Convert(converter=ListToStringConverter.class)
    protected List<String> mnemonic;
    
    /**
     * The <code>work item action</code>.
     */
    @XmlTransient
    @OneToOne(fetch = FetchType.LAZY)
    private TWorkItemAction workItemAction;

    /**
     * Gets the work item action.
     *
     * @return the work item action
     */
    public TWorkItemAction getWorkItemAction() {
        return workItemAction;
    }

    /**
     * Sets the work item action.
     *
     * @param workItemAction the new work item action
     */
    public void setWorkItemAction(TWorkItemAction workItemAction) {
        this.workItemAction = workItemAction;
    }

    /**
     * Gets the auto id.
     *
     * @return the auto id
     */
    public int getAutoId() {
        return autoId;
    }

    /**
     * Gets the mnemonic.
     *
     * @return the mnemonic
     */
    public List<String> getMnemonic() {
    	if (mnemonic == null) {
    		mnemonic = new ArrayList<String>();
    	}
        return this.mnemonic;
    }

    /**
     * Sets the mnemonic.
     *
     * @param mnemonic the new mnemonic
     */
    public void setMnemonic(List<String> mnemonic) {
        this.mnemonic = mnemonic;
    }

}
