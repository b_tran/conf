/*
 * Created on Apr 12, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.entity.jaxb.workflowdefinition;

import java.io.Serializable;

import javax.persistence.AttributeConverter;

/**
 * The WorkItemTypeConverter class
 */
public class WorkItemTypeConverter implements AttributeConverter<TWorkItemType , String> ,Serializable {
    
    /**
     * The <code>serialVersionUID</code>
     */
    private static final long serialVersionUID = 5996307886923995019L;

    /** 
     * {@inheritDoc}
     */ 
    
    @Override
    public String convertToDatabaseColumn(TWorkItemType attribute) {
        if(attribute==null){
            return null;
        }
        
        switch (attribute) {
            case REGULAR:
                return "regular";
            case ACCOUNT:
                return "account";
            case NEW_BUSINESS:
                return "new_business";
            case ENDORSEMENT:
                return "endorsement";
            case QUICK_QUOTE:
                return "quick_quote";
            case CLAIM:
                return "claim";
            case RENEWAL:
                return "renewal";
            case REISSUE:
                return "reissue";
            default:
                throw new IllegalArgumentException("Unknown" + attribute);
        }
    }
    
    /** 
     * {@inheritDoc}
     */ 
    
    @Override
    public TWorkItemType convertToEntityAttribute(String dbData) {
        
        if(dbData==null){
            return null;
        }
        switch (dbData) {
            case "regular":
                return TWorkItemType.REGULAR;
            case "account":
                return TWorkItemType.ACCOUNT;
            case "new_business":
                return TWorkItemType.NEW_BUSINESS;
            case "endorsement":
                return TWorkItemType.ENDORSEMENT;
            case "quick_quote":
                return TWorkItemType.QUICK_QUOTE;
            case "renewal":
                return TWorkItemType.RENEWAL;
            case "reissue":
                return TWorkItemType.REISSUE;
            default:
                throw new IllegalArgumentException("Unknown" + dbData);
        }
    }

}
