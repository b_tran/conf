/**
 * @author scottsurette
 */

package com.agencyport.configuration.entity.jaxb.workflowdefinition;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for T_reasonOptions complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 *  <xs:complexType name="T_reasonOptions">
 *    <xs:sequence>
 *      <xs:element ref="reason" minOccurs="0" maxOccurs="unbounded"/>
 *    </xs:sequence>
 *    <xs:attribute name="required" type="xs:boolean"/>
 *    <xs:attribute name="title" type="xs:string"/>
 *    <xs:attribute name="placeholder" type="xs:string"/>
 *  </xs:complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "T_reasonOptions", propOrder = {
    "reason"
})
@Entity(name = "work_flow_reason_options")
@Table(name = "work_flow_reason_options" )
@NamedQuery(name = "work_flow_reason_options.findAll", query = "SELECT a FROM work_flow_reason_options a")
public class TReasonOptions {
	
	/**
     * The <code>auto id</code>.
     */
    @XmlTransient
    @Id
    @Column(name = "auto_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int autoId;

    /**
     * The <code>reason</code>.
     */
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "reasonOptions", cascade = CascadeType.ALL )
    protected List<TReason> reason;
    
    /**
     * The <code>required</code>.
     */
    @Column(name = "required")
    @XmlAttribute(name = "required")
    protected Boolean required = new Boolean(false);
    
    
    /**
     * The <code>title</code>.
     */
    @Column(name = "title")
    @XmlAttribute(name = "title")
    protected String title;
    
    /**
     * The <code>placeholder</code>.
     */
    @Column(name = "placeholder")
    @XmlAttribute(name = "placeholder")
    protected String placeholder;
    
    /**
     * The <code>work item action</code>.
     */
    @XmlTransient
    @OneToOne(fetch = FetchType.LAZY)
    private TReasons reasons;

	/**
	 * @return the reason
	 */
	public List<TReason> getReason() {
		return reason;
	}
	
	/**
	 * @param reason the reason to set
	 */
	public void setReason(List<TReason> reason) {
		this.reason = reason;
	}

	/**
	 * @return the required
	 */
	public Boolean getRequired() {
		return required;
	}

	/**
	 * @param required the required to set
	 */
	public void setRequired(Boolean required) {
		this.required = required;
	}

	/**
	 * @return the reasons
	 */
	public TReasons getReasons() {
		return reasons;
	}

	/**
	 * @param reasons the reasons to set
	 */
	public void setReasons(TReasons reasons) {
		this.reasons = reasons;
	}
	
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	
	/**
	 * @return the placeholder
	 */
	public String getPlaceholder() {
		return placeholder;
	}

	/**
	 * @param placeholder the placeholder to set
	 */
	public void setPlaceholder(String placeholder) {
		this.placeholder = placeholder;
	}
}
