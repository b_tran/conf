//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.04.06 at 05:07:41 PM EDT 
//

package com.agencyport.configuration.entity.jaxb.worklistview;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="sortInfoRefId" use="required" type="{http://www.w3.org/2001/XMLSchema}IDREF" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Entity(name = "worklist_sort_info_ref")
@Table(name = "worklist_sort_info_ref")
@NamedQuery(name = "worklist_sort_info_ref.findAll", query = "SELECT a FROM worklist_sort_info_ref a")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "sortInfoRef")
public class SortInfoRef {

    /**
     * The <code>auto id</code>.
     */
    @XmlTransient
    @Id
    @Column(name = "auto_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int autoId;

    /**
     * The <code>sort info ref id</code>.
     */
    @Transient
    @XmlAttribute(name = "sortInfoRefId", required = true)
    @XmlIDREF
    @XmlSchemaType(name = "IDREF")
    protected Object sortInfoRefId;

    /**
     * The <code>view</code>.
     */
    @XmlTransient
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn
    private View view;

    /**
     * The <code>sort info</code>.
     */
    @XmlTransient
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "sort_auto_id", referencedColumnName = "auto_id")
    })
    private SortInfo sortInfo;

    /**
     * Gets the value of the sortInfoRefId property.
     * 
     * @return possible object is {@link Object }
     * 
     */
    public Object getSortInfoRefId() {
        return sortInfoRefId;
    }

    /**
     * Sets the value of the sortInfoRefId property.
     * 
     * @param value
     *            allowed object is {@link Object }
     * 
     */
    public void setSortInfoRefId(Object value) {
        this.sortInfoRefId = value;
    }

    /**
     * Gets the view.
     *
     * @return the view
     */
    public View getView() {
        return view;
    }

    /**
     * Sets the view.
     *
     * @param view the new view
     */
    public void setView(View view) {
        this.view = view;
    }

    /**
     * Gets the auto id.
     *
     * @return the auto id
     */
    public int getAutoId() {
        return autoId;
    }

    /**
     * Gets the sort info.
     *
     * @return the sort info
     */
    public SortInfo getSortInfo() {
        return sortInfo;
    }

    /**
     * Sets the sort info.
     *
     * @param sortInfo the new sort info
     */
    public void setSortInfo(SortInfo sortInfo) {
        this.sortInfo = sortInfo;
    }

    /**
     * Transfer to entity.
     */
    public void transferToEntity() {
        if (sortInfoRefId instanceof SortInfo) {
            sortInfo = ((SortInfo) sortInfoRefId);
        }
    }

    /**
     * Transfer from entity.
     */
    public void transferFromEntity() {
        sortInfoRefId = getSortInfo();

    }

}
