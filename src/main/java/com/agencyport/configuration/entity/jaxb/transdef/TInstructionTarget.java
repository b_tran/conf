package com.agencyport.configuration.entity.jaxb.transdef;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlTransient;

/**
 * The Class TInstructionTarget.
 */
@Entity(name = "tdf_instruction_traget")
@Table(name = "tdf_instruction_traget" )
@NamedQuery(name = "tdf_instruction_traget.findAll", query = "SELECT a FROM tdf_instruction_traget a")
public class TInstructionTarget {

    /**
     * The <code>auto id</code>.
     */
    @XmlTransient
    @Id
    @Column(name = "auto_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int autoId;

    /**
     * The <code>instruction</code>.
     */
    @XmlTransient
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn
    private TInstruction instruction;

    /**
     * The <code>principal</code>.
     */
    @XmlTransient
    @Column(name = "principal", nullable = false)
    protected String principal;

    /**
     * The <code>type</code>.
     */
    @XmlTransient
    @Column(name = "type", nullable = false)
    protected String type;

    /**
     * Gets the value of the principal property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getPrincipal() {
        return principal;
    }

    /**
     * Sets the value of the principal property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setPrincipal(String value) {
        this.principal = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the instruction.
     *
     * @return the instruction
     */
    public TInstruction getInstruction() {
        return instruction;
    }

    /**
     * Sets the instruction.
     *
     * @param instruction the new instruction
     */
    public void setInstruction(TInstruction instruction) {
        this.instruction = instruction;
    }

    /**
     * Get Auto generated ID
     * 
     * @return autoId
     */
    public int getAutoId() {
        return autoId;
    }

}
