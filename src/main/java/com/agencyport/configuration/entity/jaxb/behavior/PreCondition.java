//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.18 at 09:49:42 PM EDT 
//

package com.agencyport.configuration.entity.jaxb.behavior;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;simpleContent>
 *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *       &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="isSetRequested" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="isCountRequested" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/extension>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "value"
})
@Entity(name = "behavior_pre_condition")
@Table(name = "behavior_pre_condition")
@NamedQuery(name = "behavior_pre_condition.findAll", query = "SELECT a FROM behavior_pre_condition a")
@XmlRootElement(name = "preCondition")
public class PreCondition {

    /**
     * The <code>auto id</code>.
     */
    @XmlTransient
    @Id
    @Column(name = "auto_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int autoId;

    /**
     * The <code>value</code>.
     */
    @Column(name = "value")
    @XmlValue
    protected String value;

    /**
     * The <code>name</code>.
     */
    @Column(name = "name")
    @XmlAttribute(name = "name", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String name;

    /**
     * The <code>is set requested</code>.
     */
    @Column(name = "is_set_requested")
    @XmlAttribute(name = "isSetRequested")
    protected Boolean isSetRequested;

    /**
     * The <code>is count requested</code>.
     */
    @Column(name = "is_count_requested")
    @XmlAttribute(name = "isCountRequested")
    protected Boolean isCountRequested;

    /**
     * The <code>where</code>.
     */
    @XmlTransient
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn
    private Where where;

    /**
     * Gets the value of the value property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the isSetRequested property.
     * 
     * @return possible object is {@link Boolean }
     * 
     */
    public boolean isIsSetRequested() {
        if (isSetRequested == null) {
            return false;
        } else {
            return isSetRequested;
        }
    }

    /**
     * Sets the value of the isSetRequested property.
     * 
     * @param value
     *            allowed object is {@link Boolean }
     * 
     */
    public void setIsSetRequested(Boolean value) {
        this.isSetRequested = value;
    }

    /**
     * Gets the value of the isCountRequested property.
     * 
     * @return possible object is {@link Boolean }
     * 
     */
    public boolean isIsCountRequested() {
        if (isCountRequested == null) {
            return false;
        } else {
            return isCountRequested;
        }
    }

    /**
     * Sets the value of the isCountRequested property.
     * 
     * @param value
     *            allowed object is {@link Boolean }
     * 
     */
    public void setIsCountRequested(Boolean value) {
        this.isCountRequested = value;
    }

    /**
     * Gets the where.
     *
     * @return the where
     */
    public Where getWhere() {
        return where;
    }

    /**
     * Sets the where.
     *
     * @param where the new where
     */
    public void setWhere(Where where) {
        this.where = where;
    }

    /**
     * Get Auto generated ID
     * 
     * @return autoId
     */
    public int getAutoId() {
        return autoId;
    }

}
