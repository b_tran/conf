/*
 * Created on Mar 22, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.agencyport.configuration.entity.classcode.WorkCompClassCode;
import com.agencyport.configuration.entity.groovy.GroovyModel;
import com.agencyport.configuration.entity.industrycode.IndustryCode;
import com.agencyport.configuration.entity.jaxb.clientproperty.Property;
import com.agencyport.configuration.entity.jaxb.codelist.TOptionList;
import com.agencyport.configuration.entity.jaxb.dynamiclisttemplate.TDynamicListTemplate;
import com.agencyport.configuration.entity.jaxb.endpoint.EndPointType;
import com.agencyport.configuration.entity.jaxb.hosts.Host;
import com.agencyport.configuration.entity.jaxb.menudefinition.MenuDefinitionType;
import com.agencyport.configuration.entity.jaxb.searchindex.IndexMapping;
import com.agencyport.configuration.entity.jaxb.transformer.TTransformer;
import com.agencyport.configuration.entity.jaxb.view.TView;
import com.agencyport.configuration.entity.jaxb.workflowdefinition.TWorkFlow;
import com.agencyport.configuration.entity.jaxb.workitemassistant.Workitemassistant;
import com.agencyport.configuration.entity.jaxb.worklistview.WorkListViewDefinition;
import com.agencyport.configuration.entity.markdown.MarkdownPojo;

/**
 * The Client class
 */

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Entity(name = "client")
@Table(name = "client", uniqueConstraints = @UniqueConstraint(columnNames = { "name" }) )
@NamedQuery(name = "client.findAll", query = "SELECT a FROM client a")
public class Client {

    /**
     * The <code>id</code>.
     */
    @XmlAttribute(name = "id")
    @Id
    @Column(name = "id")
    private int id;

    /**
     * The <code>name</code> is the name client.
     */
    @XmlAttribute(name = "name")
    @Column(name = "name")
    private String name;

    /**
     * The <code>description</code> is the description client.
     */
    @XmlAttribute(name = "description")
    @Column(name = "description")
    private String description;

    /**
     * The <code>products</code>.
     */
    @XmlTransient
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "client", cascade = CascadeType.ALL)
    private List<Product> products;

    /**
     * The <code>groovy files</code>.
     */
    @XmlTransient
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "client", cascade = CascadeType.ALL)
    private List<GroovyModel> groovy;
    
    /**
     * The <code>groovy files</code>.
     */
    @XmlTransient
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "client", cascade = CascadeType.ALL)
    private List<Property> clientproperty;
    
    /**
     * The <code>groovy files</code>.
     */
    @XmlTransient
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "client", cascade = CascadeType.ALL)
    private List<Host> hosts;
    
    /**
     * The <code>groovy files</code>.
     */
    @XmlTransient
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "client", cascade = CascadeType.ALL)
    private List<MarkdownPojo> markdown;
    
    /**
     * The <code>EndPointType</code>.
     */
    @XmlTransient
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "client", cascade = CascadeType.ALL)
    private List<EndPointType> endpoint;
    
    
    /**
     * The <code>index mapping</code>.
     */
    @XmlTransient
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "client", cascade = CascadeType.ALL)
    private List<IndexMapping> indexMapping;

    /**
     * The <code>work list view definition</code>.
     */
    @XmlTransient
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "client", cascade = CascadeType.ALL)
    private List<WorkListViewDefinition> workListViewDefinition;
    
    /**
     * The <code>work flow</code>.
     */
    @XmlTransient
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "client", cascade = CascadeType.ALL)
    private List<TWorkFlow> workFlow;
    
    /**
     * The <code>option list</code>.
     */
    @XmlTransient
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "client", cascade = CascadeType.ALL)
    private List<TOptionList> optionList;
    
    /**
     * The <code>dynamic list template</code>.
     */
    @XmlTransient
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "client", cascade = CascadeType.ALL)
    private List<TDynamicListTemplate> dynamicListTemplate;
    
    /**
     * The <code>view</code>.
     */
    @XmlTransient
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "client", cascade = CascadeType.ALL)
    private List<TView> view;
    
    /**
     * The <code>menuDefinitionType</code>.
     */
    @XmlTransient
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "client", cascade = CascadeType.ALL)
    private List<MenuDefinitionType> menuDefinitionType;
    
    /**
     * The <code>Workitemassistant</code>.
     */
    @XmlTransient
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "client", cascade = CascadeType.ALL)
    private List<Workitemassistant> workitemassistant;
    
    /**
     * The <code>transformer</code>.
     */
    @XmlTransient
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "client", cascade = CascadeType.ALL)
    private List<TTransformer> transformer;

    /**
     * The <code>work comp class code</code>.
     */
    @XmlTransient
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "client", cascade = CascadeType.ALL)
    private List<WorkCompClassCode> workCompClassCode;

    /**
     * The <code>industry code</code>.
     */
    @XmlTransient
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "client", cascade = CascadeType.ALL)
    private List<IndustryCode> industryCode;

    
    /**
     * Get Client Name
     * 
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Set Client Name
     * 
     * @param name
     *            client name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get Client description
     * 
     * @return description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set Client description
     * 
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Get List of products
     * 
     * @return products
     */
    public List<Product> getProducts() {
        if (products == null) {
            products = new ArrayList<Product>();
        }
        return products;
    }
    
    /**
     * Get List of groovy files
     * 
     * @return groovy files
     */
    public List<GroovyModel> getGroovy() {
        if (groovy == null) {
            groovy = new ArrayList<GroovyModel>();
        }
        return groovy;
    }
    
    /**
     * Returns the endpoint
     * @return
     */
    public List<EndPointType> getEndPoint(){
    	if(endpoint == null) {
    		endpoint = new ArrayList<EndPointType>();
    	}
    	return endpoint;
    }
    /**
     * Get List of groovy files
     * 
     * @return groovy files
     */
    public List<MarkdownPojo> getMarkdown() {
        if (markdown == null) {
            markdown = new ArrayList<MarkdownPojo>();
        }
        return markdown;
    }

    /**
     * Sets the id to id
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Get Client id.
     * 
     * @return id
     */
    public int getId() {
        return id;
    }

    /**
     * Get IndexMapping
     * 
     * @return IndexMapping
     */
    public List<IndexMapping> getIndexMapping() {
        if (indexMapping == null) {
            indexMapping = new ArrayList<IndexMapping>();
        }
        return indexMapping;
    }

    /**
     * Gets the work list view definition.
     *
     * @return the work list view definition
     */
    public List<WorkListViewDefinition> getWorkListViewDefinition() {
        if(workListViewDefinition == null) {
            workListViewDefinition = new ArrayList<WorkListViewDefinition>();
        }
        return workListViewDefinition;
    }
    
    /**
     * Gets the work flow.
     *
     * @return the work flow
     */
    public List<TWorkFlow> getWorkFlow() {
        if(workFlow == null) {
            workFlow = new ArrayList<TWorkFlow>();
        }
        return workFlow;
    }

    /**
     * Gets the option list.
     *
     * @return the option list
     */
    public List<TOptionList> getOptionList() {
        if(optionList == null) {
            optionList = new ArrayList<TOptionList>();
        }
        return optionList;
    }

    /**
     * Gets the dynamic list template.
     *
     * @return the dynamic list template
     */
    public List<TDynamicListTemplate> getDynamicListTemplate() {
        if(dynamicListTemplate == null) {
            dynamicListTemplate = new ArrayList<TDynamicListTemplate>();
        }
        return dynamicListTemplate;
    }
    

    /**
     * Gets the view.
     *
     * @return the view
     */
    public List<TView> getView() {
        if(view == null) {
            view = new ArrayList<TView>();
        }
        return view;
    }

    /**
     * Returns the transformer	
     * @return the transformer
     */
    public List<TTransformer> getTransformer() {
        if(transformer == null) {
            transformer = new ArrayList<TTransformer>();
        }
        return transformer;
    }

    /**
     * Returns the menuDefinitionType	
     * @return the menuDefinitionType
     */
    public List<MenuDefinitionType> getMenuDefinitionType() {
        if(menuDefinitionType == null) {
            menuDefinitionType = new ArrayList<MenuDefinitionType>();
        }
        return menuDefinitionType;
    }

    /**
     * Returns the workitemassistant	
     * @return the workitemassistant
     */
    public List<Workitemassistant> getWorkitemassistant() {
        if(workitemassistant == null) {
            workitemassistant = new ArrayList<Workitemassistant>();
        }
        return workitemassistant;
    }

    /**
     * Returns the workCompClassCode	
     * @return the workCompClassCode
     */
    public List<WorkCompClassCode> getWorkCompClassCode() {
        if(workCompClassCode == null) {
            workCompClassCode = new ArrayList<WorkCompClassCode>();
        }
        return workCompClassCode;
    }

    /**
     * Returns the industryCode	
     * @return the industryCode
     */
    public List<IndustryCode> getIndustryCode() {
        if(industryCode == null) {
            industryCode = new ArrayList<IndustryCode>();
        }
        return industryCode;
    }

	public List<Property> getClientProperty() {
		return clientproperty;
	}

	public void setClientProperty(List<Property> clientProperty) {
		this.clientproperty = clientProperty;
	}

	public List<Host> getHosts() {
		return hosts;
	}

	public void setHosts(List<Host> hosts) {
		this.hosts = hosts;
	}

}
