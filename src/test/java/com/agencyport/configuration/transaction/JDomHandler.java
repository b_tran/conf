/*
 * Created on May 5, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.transaction;

import javax.xml.bind.ValidationEventHandler;
import javax.xml.bind.annotation.DomHandler;
import javax.xml.transform.Source;

import org.jdom2.Element;
import org.jdom2.transform.JDOMResult;
import org.jdom2.transform.JDOMSource;

/**
 * The JDomHandler class
 */
public class JDomHandler implements DomHandler<Element, JDOMResult> {
    /**
     * Default
     * Constructs an instance.
     */
    public JDomHandler() {
    }
    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public JDOMResult createUnmarshaller(ValidationEventHandler errorHandler) {
        return new JDOMResult();
    }
    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public Element getElement(JDOMResult rt) {
        return rt.getDocument().getRootElement();
    }
    /**
     * 
     * {@inheritDoc}
     */
    
    @Override
    public Source marshal(Element n, ValidationEventHandler errorHandler) {
        return new JDOMSource(n);
    }
    
    

}
