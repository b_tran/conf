/*
 * Created on Apr 27, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.transaction;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * The Response class
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="response")
@XmlType(name = "", propOrder = {
        "status",
        "message",
        "results"
})
public class Response {
    
    /**
     * The <code>status</code>.
     */
    private String status;
    
    /**
     * The <code>message</code>.
     */
    private String message;
    
    /**
     * The <code>results</code>.
     */
    private Results results;

    /**
     * Returns the status	
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Returns the message	
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Returns the results	
     * @return the results
     */
    public Results getResults() {
        return results;
    }

    /**
     * Sets the status to status
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Sets the message to message
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Sets the results to results
     * @param results the results to set
     */
    public void setResults(Results results) {
        this.results = results;
    }

}
