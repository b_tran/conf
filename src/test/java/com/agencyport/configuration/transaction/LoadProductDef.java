/*
 * Created on Mar 31, 2016 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.configuration.transaction;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.custommonkey.xmlunit.DetailedDiff;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.yaml.snakeyaml.Yaml;

import com.agencyport.configuration.entity.Client;
import com.agencyport.configuration.entity.Product;
import com.agencyport.configuration.entity.jaxb.behavior.TransactionDefinitionBehavior;
import com.agencyport.configuration.entity.jaxb.searchindex.IndexMapping;
import com.agencyport.configuration.entity.jaxb.transdef.TPage;
import com.agencyport.configuration.entity.jaxb.transdef.TTransaction;
import com.agencyport.configuration.entity.jaxb.workflowdefinition.TWorkFlow;
import com.agencyport.configuration.entity.jaxb.worklistview.WorkListViewDefinition;
import com.agencyport.configuration.loader.BatchLoader;
import com.agencyport.configuration.loader.client.ClientConfig;
import com.agencyport.configuration.security.mock.TestProfileCreator;

/**
 * The TestTransaction class
 */
public class LoadProductDef {
	
	/**
	 * The <code>context</code>.
	 */
	@Autowired
	ApplicationContext context;
	
	/**
	 * The <code>transaction manager</code>.
	 */
	@Autowired
	JpaTransactionManager transactionManager;
	
	/**
	 * The <code>init</code>.
	 */
	private static boolean init = false;
	
	/**
	 * Load.
	 */
	@Before
	public  void  load(){
		
		if(init)
			return;
		
		TestProfileCreator.createSecurityProfile("agent");
        
		 InputStream stream = null;
			try {
			    
			    BatchLoader loader  = context.getBean(BatchLoader.class);
	            loader.loadAll();
			    
			}catch (Exception exception) {
				exception.printStackTrace();
				
			}finally {
				try {
				if(stream != null)
					stream.close();
				}catch (Exception exception) {
					;
				}
				
			}
			
			init =true;

	}
	
	/**
	 * Test a transaction.
	 *
	 * @throws Exception the exception
	 */
	public void testATransaction() throws Exception{
		

		EntityManagerFactory entityManagerFactory = transactionManager.getEntityManagerFactory();
		EntityManager em = entityManagerFactory.createEntityManager();
		
		try {
		    

            Resource res =  new ClassPathResource("config/batchloader/clientconfig.yaml");
            InputStream stream =  res.getInputStream();
            
            ClientConfig config = new Yaml().loadAs( stream, ClientConfig.class );
            stream.close();
            config.getClient().forEach(_client -> {
                _client.getProduct().forEach(pro->{
                    pro.getTransaction().forEach(t->{
                        
                      try {
                              JAXBContext jc1 = JAXBContext.newInstance(com.agencyport.configuration.entity.jaxb.transdef.ObjectFactory.class);
                              Unmarshaller u1 = jc1.createUnmarshaller();
                              JAXBElement<TTransaction>  j = (JAXBElement<TTransaction>) u1.unmarshal(new ClassPathResource(t.getLocation()).getInputStream());
                              TTransaction expectedTransaction = j.getValue();
                              Client client =   em.createQuery(
                                      "SELECT c FROM client c WHERE c.name = :clientName",Client.class)
                                      .setParameter("clientName", _client.getName())
                                      .getResultList().stream().findFirst().get();
                              
                              
                              CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
                              CriteriaQuery<Product> criteriaQuery = criteriaBuilder.createQuery(Product.class);
                              Root<Product> pRoot = criteriaQuery.from(Product.class);
                              List<Predicate> predicates = new ArrayList<Predicate>();
                              predicates.add(criteriaBuilder.equal(pRoot.get("type"), pro.getType()));
                              predicates.add(criteriaBuilder.equal(pRoot.get("version"), pro.getVersion()));
                              predicates.add(criteriaBuilder.equal(pRoot.get("client"), client));
                              predicates.add(criteriaBuilder.lessThanOrEqualTo(pRoot.get("effectiveDate"), pro.getEffective_date()));
                              predicates.add(criteriaBuilder.equal(pRoot.get("isCurrentVersion"), pro.isIs_current_version()));
                              criteriaQuery.select(pRoot).where(predicates.toArray(new Predicate[] {}));
                              
                              com.agencyport.configuration.entity.Product eProduct = em.createQuery(criteriaQuery).getResultList().stream().findFirst().get();
                              
                              CriteriaBuilder tranCriteriaBuilder = em.getCriteriaBuilder();
                              CriteriaQuery<TTransaction> tranCriteriaQuery = tranCriteriaBuilder.createQuery(TTransaction.class);
                              Root<TTransaction> tranRoot = tranCriteriaQuery.from(TTransaction.class);
                              List<Predicate> tranPredicates = new ArrayList<Predicate>();
                              tranPredicates.add(criteriaBuilder.equal(tranRoot.get("product"), eProduct));
                              tranPredicates.add(criteriaBuilder.equal(tranRoot.get("id"), expectedTransaction.getId()));
                              tranCriteriaQuery.select(tranRoot).where(tranPredicates.toArray(new Predicate[] {}));
                              
                              TTransaction actualTTransaction = em.createQuery(tranCriteriaQuery).getResultList().stream().findFirst().get();
                              actualTTransaction.transferFromEntity();
                              
                              for (TPage page : actualTTransaction.getTPage()) {
                                  page.getPageElement().forEach(pageElem -> {
                                      pageElem.getFieldElement().forEach(fieldElem -> {
                                          fieldElem.transferFromEntity();
                                      });
                                  });
                                  page.getConnectors().forEach(connectors -> {
                                      connectors.getConnector().forEach(connector -> {
                                          connector.getCustomParameters().forEach(param -> {
      
                                          });
                                      });
                                      connectors.getInstruction().forEach(instruction -> {
                                          instruction.transferFromEntity();
                                      });
      
                                  });
                              }
                              
                              jc1 = JAXBContext.newInstance(com.agencyport.configuration.entity.jaxb.transdef.TTransaction.class);
                              Marshaller m1 = jc1.createMarshaller();
                              ByteArrayOutputStream baos = new ByteArrayOutputStream();
                              m1 = jc1.createMarshaller();
                              m1.marshal(new com.agencyport.configuration.entity.jaxb.transdef.ObjectFactory().createTransaction(actualTTransaction), baos);
                              String actualXML =new String(baos.toByteArray());
                              
                              baos = new ByteArrayOutputStream();
                              m1 = jc1.createMarshaller();
                              m1.marshal(new com.agencyport.configuration.entity.jaxb.transdef.ObjectFactory().createTransaction(expectedTransaction), baos);
                              String expectedXML =new String(baos.toByteArray());
                              
                              List<?> allDifferences = getDiff(expectedXML, actualXML);
                              allDifferences.forEach(d->{
                                  System.out.println( d.toString());
                              });
                         
                          //Assert.assertEquals("Differences found: "+ diff.toString(), 0, allDifferences.size());
                              
                          }catch(Exception exception) {
                              exception.printStackTrace();
                          }
                        
                    });
                });
                

                
            });
	
	}finally {
		em.close();
	}

	}
	
	/**
	 * Test b transaction behavior.
	 *
	 * @throws Exception the exception
	 */
	public void testBTransactionBehavior()  throws Exception{
		

		EntityManagerFactory entityManagerFactory = transactionManager.getEntityManagerFactory();
		EntityManager em = entityManagerFactory.createEntityManager();
		
		try {
            Resource res =  new ClassPathResource("config/batchloader/clientconfig.yaml");
            InputStream stream =  res.getInputStream();
            
            ClientConfig config = new Yaml().loadAs( stream, ClientConfig.class );
            stream.close();
            config.getClient().forEach(_client -> {
                _client.getProduct().forEach(pro->{
                    pro.getBehavior().forEach(b->{
                        
                      try {
                              JAXBContext jc1 = JAXBContext.newInstance(com.agencyport.configuration.entity.jaxb.behavior.ObjectFactory.class);
                              Unmarshaller u1 = jc1.createUnmarshaller();
                              TransactionDefinitionBehavior expectedTransactionDefinitionBehavior = (TransactionDefinitionBehavior) u1.unmarshal(new ClassPathResource(b.getLocation()).getInputStream());
                              
                              Client client =   em.createQuery(
                                      "SELECT c FROM client c WHERE c.name = :clientName",Client.class)
                                      .setParameter("clientName", _client.getName())
                                      .getResultList().stream().findFirst().get();
                              
                              
                              CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
                              CriteriaQuery<Product> criteriaQuery = criteriaBuilder.createQuery(Product.class);
                              Root<Product> pRoot = criteriaQuery.from(Product.class);
                              List<Predicate> predicates = new ArrayList<Predicate>();
                              predicates.add(criteriaBuilder.equal(pRoot.get("type"), pro.getType()));
                              predicates.add(criteriaBuilder.equal(pRoot.get("version"), pro.getVersion()));
                              predicates.add(criteriaBuilder.equal(pRoot.get("client"), client));
                              predicates.add(criteriaBuilder.lessThanOrEqualTo(pRoot.get("effectiveDate"), pro.getEffective_date()));
                              predicates.add(criteriaBuilder.equal(pRoot.get("isCurrentVersion"), pro.isIs_current_version()));
                              criteriaQuery.select(pRoot).where(predicates.toArray(new Predicate[] {}));
                              
                              com.agencyport.configuration.entity.Product eProduct = em.createQuery(criteriaQuery).getResultList().stream().findFirst().get();
                              
                              CriteriaBuilder tranCriteriaBuilder = em.getCriteriaBuilder();
                              CriteriaQuery<TTransaction> tranCriteriaQuery = tranCriteriaBuilder.createQuery(TTransaction.class);
                              Root<TTransaction> tranRoot = tranCriteriaQuery.from(TTransaction.class);
                              List<Predicate> tranPredicates = new ArrayList<Predicate>();
                              tranPredicates.add(criteriaBuilder.equal(tranRoot.get("product"), eProduct));
                              tranPredicates.add(criteriaBuilder.equal(tranRoot.get("id"), b.getTransaction()));
                              tranCriteriaQuery.select(tranRoot).where(tranPredicates.toArray(new Predicate[] {}));
                              
                              List<TTransaction> s = em.createQuery(tranCriteriaQuery).getResultList();
                              TTransaction eTTransaction = em.createQuery(tranCriteriaQuery).getResultList().stream().findFirst().get();
                              Set<TransactionDefinitionBehavior> bs = eTTransaction.getTransactionDefinitionBehavior();
                              TransactionDefinitionBehavior actualTransactionDefinitionBehavior = eTTransaction.getTransactionDefinitionBehavior().stream().findFirst().get();
                              if(actualTransactionDefinitionBehavior.getWhere() != null) {
                                  actualTransactionDefinitionBehavior.getWhere().getPreCondition().forEach(_x ->{});
                              }
                              actualTransactionDefinitionBehavior.getAdditionalFieldToShred().forEach(x->{});
                              actualTransactionDefinitionBehavior.getHotField().forEach(_x->{
                                  _x.getRefreshListFieldEvent();
                              });
                              actualTransactionDefinitionBehavior.getBehavior().forEach(_b ->{
                                  _b.transferFromEntity();
                              });
                              jc1 = JAXBContext.newInstance(com.agencyport.configuration.entity.jaxb.behavior.TransactionDefinitionBehavior.class);
                              Marshaller m1 = jc1.createMarshaller();
                              ByteArrayOutputStream baos = new ByteArrayOutputStream();
                              m1 = jc1.createMarshaller();
                              m1.marshal(actualTransactionDefinitionBehavior, baos);
                              String actualXML =new String(baos.toByteArray());
                              
                              baos = new ByteArrayOutputStream();
                              m1 = jc1.createMarshaller();
                              m1.marshal(expectedTransactionDefinitionBehavior, baos);
                              String expectedXML =new String(baos.toByteArray());
                              
                              
                              List<?> allDifferences = getDiff(expectedXML, actualXML);
                              allDifferences.forEach(d->{
                                  System.out.println( d.toString());
                              });
                         
                          //Assert.assertEquals("Differences found: "+ diff.toString(), 0, allDifferences.size());
                              
                          }catch(Exception exception) {
                              exception.printStackTrace();
                          }
                        
                    });
                });
                

                
            });

	
	}finally {
		em.close();
	}

	}
	
	/**
	 * Test c index mapping.
	 *
	 * @throws Exception the exception
	 */
	public void testCIndexMapping() throws Exception{
		

		EntityManagerFactory entityManagerFactory = transactionManager.getEntityManagerFactory();
		EntityManager em = entityManagerFactory.createEntityManager();
		
		try {
			
            Resource res =  new ClassPathResource("config/batchloader/clientconfig.yaml");
            InputStream stream =  res.getInputStream();
            
            ClientConfig config = new Yaml().loadAs( stream, ClientConfig.class );
            stream.close();
            config.getClient().forEach(_client -> {
                
                _client.getSearchindex().forEach(searchIndex->{
                    
                    try {
                        JAXBContext jc1 = JAXBContext.newInstance(com.agencyport.configuration.entity.jaxb.searchindex.ObjectFactory.class);
                        Unmarshaller u1 = jc1.createUnmarshaller();
                        IndexMapping expectedIndexMapping = (IndexMapping) u1.unmarshal(new ClassPathResource(searchIndex.getLocation()).getInputStream());
                        Client client =   em.createQuery(
                                "SELECT c FROM client c WHERE c.name = :clientName",Client.class)
                                .setParameter("clientName", _client.getName())
                                .getResultList().stream().findFirst().get();
                        
                        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
                        CriteriaQuery<IndexMapping> criteriaQuery = criteriaBuilder.createQuery(IndexMapping.class);
                        Root<IndexMapping> pRoot = criteriaQuery.from(IndexMapping.class);
                        List<Predicate> predicates = new ArrayList<Predicate>();
                        predicates.add(criteriaBuilder.equal(pRoot.get("client"), client));
                        predicates.add(criteriaBuilder.equal(pRoot.get("index"), expectedIndexMapping.getIndex()));
                        criteriaQuery.select(pRoot)
                        .where(predicates.toArray(new Predicate[] {}));
                        IndexMapping acttualIndexMapping = em.createQuery(criteriaQuery).getResultList().stream().findFirst().get();
    
                        acttualIndexMapping.getField().forEach(field-> {
                            
                            field.getContent().forEach(c->{
                                ;
                            });
                        });
                        
                        
                        jc1 = JAXBContext.newInstance(com.agencyport.configuration.entity.jaxb.searchindex.IndexMapping.class);
                        Marshaller m1 = jc1.createMarshaller();
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        m1 = jc1.createMarshaller();
                        m1.marshal(acttualIndexMapping, baos);
                        String actualXML =new String(baos.toByteArray());
                        
                        baos = new ByteArrayOutputStream();
                        m1 = jc1.createMarshaller();
                        m1.marshal(expectedIndexMapping, baos);
                        String expectedXML =new String(baos.toByteArray());
                        
                        
                        List<?> allDifferences = getDiff(expectedXML, actualXML);
                        allDifferences.forEach(d->{
                            System.out.println( d.toString());
                        });
                   
                    //Assert.assertEquals("Differences found: "+ diff.toString(), 0, allDifferences.size());
                        
                    }catch(Exception exception) {
                        exception.printStackTrace();
                    }
                    
                    
                
                });
                
            });

	
	}finally {
		em.close();
	}

	}
	
	/**
	 * Test d work list view definition.
	 *
	 * @throws Exception the exception
	 */
	public void testDWorkListViewDefinition() throws Exception{
		

		EntityManagerFactory entityManagerFactory = transactionManager.getEntityManagerFactory();
		EntityManager em = entityManagerFactory.createEntityManager();
		
		try {
		    Resource res =  new ClassPathResource("config/batchloader/clientconfig.yaml");
            InputStream stream =  res.getInputStream();
            
            ClientConfig config = new Yaml().loadAs( stream, ClientConfig.class );
            stream.close();
            config.getClient().forEach(_client -> {
                
                _client.getWorklistview().forEach(lv->{
                    
                    try {
                        JAXBContext jc1 = JAXBContext.newInstance(com.agencyport.configuration.entity.jaxb.worklistview.ObjectFactory.class);
                        Unmarshaller u1 = jc1.createUnmarshaller();
                        WorkListViewDefinition expectedWorkListViewDefinition = (WorkListViewDefinition) u1.unmarshal(new ClassPathResource(lv.getLocation()).getInputStream());
                        
                        
                        Client client =   em.createQuery(
                                "SELECT c FROM client c WHERE c.name = :clientName",Client.class)
                                .setParameter("clientName", _client.getName())
                                .getResultList().stream().findFirst().get();
                        
                        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
                        CriteriaQuery<WorkListViewDefinition> criteriaQuery = criteriaBuilder.createQuery(WorkListViewDefinition.class);
                        Root<WorkListViewDefinition> pRoot = criteriaQuery.from(WorkListViewDefinition.class);
                        List<Predicate> predicates = new ArrayList<Predicate>();
                        predicates.add(criteriaBuilder.equal(pRoot.get("client"), client));
                        predicates.add(criteriaBuilder.equal(pRoot.get("viewName"), expectedWorkListViewDefinition.getViewName()));
                        criteriaQuery.select(pRoot)
                        .where(predicates.toArray(new Predicate[] {}));
                        WorkListViewDefinition actualWorkListViewDefinition = em.createQuery(criteriaQuery).getResultList().stream().findFirst().get();
    
                        
                        if(actualWorkListViewDefinition.getQueryInfos() != null) {
                            actualWorkListViewDefinition.getQueryInfos().setWorkListViewDefinition(actualWorkListViewDefinition);
                            actualWorkListViewDefinition.getQueryInfos().getQueryInfo().forEach(qInfo->{
                                qInfo.setQueryInfos(actualWorkListViewDefinition.getQueryInfos());
                                qInfo.getQueryField().forEach(queryField->{
                                    queryField.setQueryInfo(qInfo);
                                    
                                });
                            });
                        }
                        
                        if(actualWorkListViewDefinition.getSortInfos() != null) {
                            actualWorkListViewDefinition.getSortInfos().setWorkListViewDefinition(actualWorkListViewDefinition);
                            actualWorkListViewDefinition.getSortInfos().getSortInfo().forEach(sInfo->{
                                sInfo.setSortInfos(actualWorkListViewDefinition.getSortInfos());
                            });
                        }
                        actualWorkListViewDefinition.getFilters().forEach(filters->{
                            filters.setWorkListViewDefinition(actualWorkListViewDefinition);
                            filters.getFilter().forEach(filter->{
                                filter.setFilters(filters);
                            });
                        });
                        
                        actualWorkListViewDefinition.getViews().setWorkListViewDefinition(actualWorkListViewDefinition);
                        actualWorkListViewDefinition.getViews().getView().forEach(view->{
                            view.setViews(actualWorkListViewDefinition.getViews());
                            
                            if(view.getFilterRefs() != null) {
                                view.getFilterRefs().setView(view);
                                view.getFilterRefs().transferToEntity();
                                view.getFilterRefs().getFilterRef().forEach(ref->{
                                    ref.setFilterRefs(view.getFilterRefs());
                                    ref.transferFromEntity();
                                });
                            }
                            if(view.getSortInfoRef() != null) {
                                view.getSortInfoRef().setView(view);
                                view.getSortInfoRef().transferFromEntity();
                            }
                            
                            if(view.getQueryInfoRef() != null) {
                                view.getQueryInfoRef().setView(view);
                                view.getQueryInfoRef().transferFromEntity();
                            }
                        });
                        
                        jc1 = JAXBContext.newInstance(com.agencyport.configuration.entity.jaxb.worklistview.WorkListViewDefinition.class);
                        Marshaller m1 = jc1.createMarshaller();
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        m1 = jc1.createMarshaller();
                        m1.marshal(actualWorkListViewDefinition, baos);
                        String actualXML =new String(baos.toByteArray());
                        
                        baos = new ByteArrayOutputStream();
                        m1 = jc1.createMarshaller();
                        m1.marshal(expectedWorkListViewDefinition, baos);
                        String expectedXML =new String(baos.toByteArray());
                        
                        
                        List<?> allDifferences = getDiff(expectedXML, actualXML);
                        allDifferences.forEach(d->{
                            System.out.println( d.toString());
                        });
                   
                    //Assert.assertEquals("Differences found: "+ diff.toString(), 0, allDifferences.size());
                        
                    }catch(Exception exception) {
                        exception.printStackTrace();
                    }
                    
                    
                
                });
				
			});

			

	
	}finally {
		em.close();
	}

	}
	
	/**
	 * Test e work flow.
	 *
	 * @throws Exception the exception
	 */
    public void testEWorkFlow() throws Exception{
        

        EntityManagerFactory entityManagerFactory = transactionManager.getEntityManagerFactory();
        EntityManager em = entityManagerFactory.createEntityManager();
        
        try {
            Resource res =  new ClassPathResource("config/batchloader/clientconfig.yaml");
            InputStream stream =  res.getInputStream();
            
            ClientConfig config = new Yaml().loadAs( stream, ClientConfig.class );
            stream.close();
            config.getClient().forEach(_client -> {
                
                _client.getWorkflow().forEach(wf->{
                    try {
                        
                        JAXBContext jc1 = JAXBContext.newInstance(com.agencyport.configuration.entity.jaxb.workflowdefinition.ObjectFactory.class);
                        Unmarshaller u1 = jc1.createUnmarshaller();
                        JAXBElement<TWorkFlow> expectedWorkFlow = (JAXBElement<TWorkFlow>) u1.unmarshal(new ClassPathResource(wf.getLocation()).getInputStream());
                        
                        
                        Client client =   em.createQuery(
                                "SELECT c FROM client c WHERE c.name = :clientName",Client.class)
                                .setParameter("clientName", _client.getName())
                                .getResultList().stream().findFirst().get();
                        
                        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
                        CriteriaQuery<TWorkFlow> criteriaQuery = criteriaBuilder.createQuery(TWorkFlow.class);
                        Root<TWorkFlow> pRoot = criteriaQuery.from(TWorkFlow.class);
                        List<Predicate> predicates = new ArrayList<Predicate>();
                        predicates.add(criteriaBuilder.equal(pRoot.get("client"), client));
                        predicates.add(criteriaBuilder.equal(pRoot.get("id"), expectedWorkFlow.getValue().getId()));
                        criteriaQuery.select(pRoot)
                        .where(predicates.toArray(new Predicate[] {}));
                        TWorkFlow actualTWorkFlow = em.createQuery(criteriaQuery).getResultList().stream().findFirst().get();
                        

                        jc1 = JAXBContext.newInstance(com.agencyport.configuration.entity.jaxb.workflowdefinition.TWorkFlow.class);
                        Marshaller m1 = jc1.createMarshaller();
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        m1 = jc1.createMarshaller();
                        m1.marshal(new com.agencyport.configuration.entity.jaxb.workflowdefinition.ObjectFactory().createWorkFlow(actualTWorkFlow), baos);
                        String actualXML =new String(baos.toByteArray());
                        
                        baos = new ByteArrayOutputStream();
                        m1 = jc1.createMarshaller();
                        m1.marshal(expectedWorkFlow, baos);
                        String expectedXML =new String(baos.toByteArray());
                        
                        
                        List<?> allDifferences = getDiff(expectedXML, actualXML);
                        allDifferences.forEach(d->{
                            System.out.println( d.toString());
                        });
                       
                        //Assert.assertEquals("Differences found: "+ diff.toString(), 0, allDifferences.size());
                        
                    }catch(Exception exception) {
                        exception.printStackTrace();
                    }
                    
                });
                
            });

            

    
    }finally {
        em.close();
    }

    }
	
	/**
	 * Gets the diff.
	 *
	 * @param expectedXML the expected xml
	 * @param actualXML the actual xml
	 * @return the diff
	 * @throws Exception the exception
	 */
	public List<?> getDiff(String expectedXML, String actualXML) throws Exception{
	    
	    expectedXML = sort(expectedXML);
	    actualXML = sort(actualXML);
	    
	    
	    XMLUnit.setIgnoreWhitespace(true);
        XMLUnit.setIgnoreAttributeOrder(true);
        DetailedDiff diff = new DetailedDiff(XMLUnit.compareXML(expectedXML, actualXML));
        return diff.getAllDifferences();
	    
	}
	
	/**
	 * Sort.
	 *
	 * @param xml the xml
	 * @return the string
	 * @throws Exception the exception
	 */
	public String sort(String xml) throws Exception{
	    
	    
	    Resource xsltResource =  new ClassPathResource("config/XSLT/sort.xslt");
	    InputStream xsltImputStream = xsltResource.getInputStream();
	    Source xmlInput = new StreamSource(new ByteArrayInputStream(xml.getBytes()));
	    ByteArrayOutputStream outStream =new ByteArrayOutputStream();
	    Source xsl = new StreamSource(xsltImputStream);
	    Result xmlOutput = new StreamResult(outStream);
	    
	    Transformer transformer = TransformerFactory.newInstance().newTransformer(xsl);
        transformer.transform(xmlInput, xmlOutput);
        
        xsltImputStream.close();
        return new String(outStream.toByteArray());
	    
	}
	
	
	
	

}
